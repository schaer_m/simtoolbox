%%%%% FUNCTION optimization_diagnostic.m %%%%%
% Mattia Schaer
% April 2014
%
% Generate plots to control optimization progress.
%
% CALL optimization_diagnostic()
%
%%%%%



function optimization_diagnostic(varargin)

global runParam hFigOptDiagnostic;


% % Put everything in one figure
% figureName = ['optimization_diagnostic: ' runParam.list(runParam.active.simInd).simFilebase];
% hFig = findobj('Type','figure', 'Name',figureName);
% if isempty(hFig)
%     hFig = figure('Name',figureName, ...
%                   'NumberTitle','off', ...
%                   'Position', [0 0 1240 768] ...
%                  );
% end
% figure(hFig);
% Put everything in one figure
    if isempty(hFigOptDiagnostic)
        hFigOptDiagnostic = figure(1);
    else
        set(0, 'CurrentFigure', hFigOptDiagnostic);
    end

FontSize = 14;

% number of subplot rows and columns
rNum = 1 + runParam.optimize.no;
cNum = 2;

% Indices to show
switch nargin
    case 0
        plotInd = 1:runParam.optimize.optInd;
        bestInd = runParam.optimize.bestInd(runParam.active.simInd);
    case 1
        plotInd = varargin{1};
        [~, bestInd] = min(runParam.optimize.fom(runParam.active.simInd,plotInd));
    otherwise
        error('Unknown input sequence');
end
plotIndRanges = [0 max(plotInd)];
% variables to change the bar color of the best iteration
%CDataIndexing = ones(1, runParam.optimize.optInd);
%CDataIndexing(bestInd) = 2;
%CDataColormap = [0 0 1 ; 0 1 0];

subplot(rNum, cNum, 1);
hBar = bar(squeeze(runParam.optimize.fom(runParam.active.simInd, plotInd)));
xlim(plotIndRanges);
fomWidth = max(runParam.optimize.fom(runParam.active.simInd, plotInd)) - min(runParam.optimize.fom(runParam.active.simInd, plotInd));
if fomWidth == 0
    fomWidth = 1;
end
fomRanges(1) = min(runParam.optimize.fom(runParam.active.simInd, plotInd)) - 0.1*fomWidth;
fomRanges(2) = max(runParam.optimize.fom(runParam.active.simInd, plotInd)) + 0.1*fomWidth;
ylim(fomRanges);
%hBarChildren = get(hBar, 'children');
%set(hBarChildren, 'CDataMapping','direct');
%set(hBarChildren, 'CData',CDataIndexing);
%colormap(CDataColormap);
set(gca, 'FontSize',FontSize);
xlabel('Iteration');
ylabel('Penalty');
title(['Penalty: ' runParam.fomDef{runParam.optimize.fomToOpt}.name ...
       ' , Best value: ' sprintf('%.3E',runParam.optimize.bestFom(runParam.active.simInd)) ' (opt' num2str(bestInd) ')'], ...
       'Interpreter','none');

if runParam.postprocess.start
    % Plot the evolution of the other figures of merit
    subplot(rNum, cNum, 2);
    legendEntry = cell(1, numel(runParam.postprocess.fomIndices));
    for ii = 1:numel(runParam.postprocess.fomIndices)
        % Normalize from 1 to 0
        tmpFom = squeeze(runParam.fom(ii, runParam.active.simInd, plotInd));
        tmpFomRescaled = tmpFom-min(tmpFom);
        tmpFomRescaled = tmpFomRescaled / max(tmpFomRescaled);
        plot(tmpFomRescaled);
        xlim(plotIndRanges);
        hold all;
        set(gca, 'FontSize',FontSize);
        xlabel('Iteration');
        ylabel('Other FOMs');
%         legendEntry{ii} = [runParam.fomDef{runParam.postprocess.fomIndices(ii)}.name ...
%                            ' (' num2str(min(tmpFom)) ',' num2str(max(tmpFom)) ')'];
        legendEntry{ii} = [runParam.fomDef{runParam.postprocess.fomIndices(ii)}.name];
    end
    legend(legendEntry, 'Interpreter','none');
    hold off;
end

tmpPar = NaN(1, runParam.optimize.optInd-1);
for ii = 1:runParam.optimize.no
    % prepare parameter array
    for jj = plotInd
        tmpPar(jj) = runParam.optimize.list(runParam.active.simInd, jj).(runParam.optimize.name{ii});
    end
    
    subplot(rNum, cNum, 1+cNum*ii);
    hBar = bar(tmpPar);
    %hBarChildren = get(hBar, 'children');
    %set(hBarChildren, 'CDataMapping','direct');
    %set(hBarChildren, 'CData',CDataIndexing);
    %colormap(CDataColormap);
    xlim(plotIndRanges);
    ylim(runParam.(runParam.optimize.name{ii}).opt(2:3));
    hold on;
    plot(get(gca, 'XLim'), repmat(tmpPar(bestInd),1,2), 'g', 'LineWidth',2);
    if tmpPar(bestInd)>runParam.(runParam.optimize.name{ii}).opt(2) && tmpPar(bestInd)<runParam.(runParam.optimize.name{ii}).opt(3)
        parTick = [runParam.(runParam.optimize.name{ii}).opt(2) ...
                 tmpPar(bestInd) ...
                 runParam.(runParam.optimize.name{ii}).opt(3) ...
        ];
    else
        parTick = [runParam.(runParam.optimize.name{ii}).opt(2) ...
                 runParam.(runParam.optimize.name{ii}).opt(3) ...
        ];
    end
    set(gca, 'YTick', parTick);
    hold off;
    set(gca, 'FontSize',FontSize);
    xlabel('Iteration');
    %ylabel(['Par. ' num2str(ii) ': ' runParam.optimize.name{ii} ' [' runParam.(runParam.optimize.name{ii}).unit ']']);
    ylabel([runParam.optimize.name{ii}(1:6) ' [' runParam.(runParam.optimize.name{ii}).unit ']']);
    
    subplot(rNum, cNum, 2+cNum*ii);
    plot(tmpPar(plotInd), squeeze(runParam.optimize.fom(runParam.active.simInd, plotInd)), '.');
    hold on;
    plot(tmpPar(bestInd), runParam.optimize.fom(runParam.active.simInd, bestInd), 'xg', 'MarkerSize',10, 'LineWidth',3);
    hold off;
    xlim(runParam.(runParam.optimize.name{ii}).opt(2:3));
    ylim(fomRanges);
    set(gca, 'FontSize',FontSize);
    %xlabel(['Par. ' num2str(ii) ': ' runParam.optimize.name{ii} ' [' runParam.(runParam.optimize.name{ii}).unit ']']);
    xlabel([runParam.optimize.name{ii} ' [' runParam.(runParam.optimize.name{ii}).unit ']']);
    ylabel('Penalty');
    set(gca, 'XTick', parTick);
end


end
