%%%%% FUNCTION Ekin_to_p.m %%%%%
% Mattia Schaer
% March 2013
%
% Convert kinetic energy to momentum (and corresponding relativistic
% factors)
%
% CALL [beta, gamma, p] = Ekin_to_p(Ekin, m)
%           Ekin:                   float(N)        [eV]
%           m:                      float(N)        [eV/c^2]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [beta, gamma, p] = Ekin_to_p(Ekin, m)
    gamma   = Ekin/m + 1;
    beta    = sqrt(1 - 1./gamma.^2);
    p       = m * gamma .* beta;
end
