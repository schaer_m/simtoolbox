%%%%% FUNCTION run_optimize.m %%%%%
% Mattia Schaer
% March 2014
%
% Optimize parameters according to the input sets
%
% CALL run_optimize()
%
%%%%%



function run_optimize()

global runParam hFigOptDiagnostic;


%% Prepare variable to store figure-of-merits
if runParam.newRun && runParam.postprocess.start
    runParam.fom = NaN(numel(runParam.postprocess.fomIndices), ...
                             runParam.active.combinationsNo, ...
                             runParam.optimize.MaxFunEval );
end


%% Loop over active parameters
for simIndex = runParam.active.restartInd:runParam.active.combinationsNo
    fprintf('\nRunning optimization %d/%d, ', simIndex, runParam.active.combinationsNo);
    runParam.active.simInd = simIndex;
    
    % Define vectors of starting parameters and upper and lower bounds
    for ii = 1:runParam.optimize.no
        startPars(ii)   = runParam.(runParam.optimize.name{ii}).opt(1); %#ok<AGROW>
        lowerPars(ii)   = runParam.(runParam.optimize.name{ii}).opt(2); %#ok<AGROW>
        upperPars(ii)   = runParam.(runParam.optimize.name{ii}).opt(3); %#ok<AGROW>
        tolPars(ii)     = runParam.(runParam.optimize.name{ii}).opt(4); %#ok<AGROW>
    end
    
    % transform values from real dimensions to normalized dimensions in [0,1]
    %         normRange = [0.0 1.0];
    %         for ii = 1:runParam.optimize.no
    %             realRange(ii,1:2) = [runParam.(runParam.optimize.name{ii}).opt(2) runParam.(runParam.optimize.name{ii}).opt(3)]; %#ok<AGROW>
    %             startPars(ii) = interp1(realRange(ii,:), normRange, runParam.(runParam.optimize.name{ii}).opt(1)); %#ok<AGROW>
    %             lowerPars(ii) = 0.0; %#ok<AGROW>
    %             upperPars(ii) = 1.0; %#ok<AGROW>
    %         end
    
    % Setup optimizer
    opt = setup_optimizer(lowerPars, upperPars, tolPars);
    
    % Optimize
    runParam.optimize.optInd = 1;
    
    % Open figure where to follow the optimization process
    figureName = ['optimization_diagnostic: ' runParam.list(runParam.active.simInd).simFilebase];
    hFigOptDiagnostic = figure('Name',figureName, ...
                  'NumberTitle','off', ...
                  'Position', [0 0 1240 768] ...
                 );
    figure(hFigOptDiagnostic);
    drawnow();
    
    switch runParam.optimize.algorithm
        
        % Standard MATLAB optimizers
        case 'patternsearch'
            [runParam.optimize.bestPars(simIndex,:), runParam.optimize.bestFom(simIndex), runParam.optimize.exitFlag(simIndex), runParam.optimize.output(simIndex)] = ...
                patternsearch(run_single_opt, startPars, [], [], [], [], lowerPars, upperPars, [], runParam.optimize.algorithmOptions);
        case 'fmincon'
            [runParam.optimize.bestPars(simIndex,:), runParam.optimize.bestFom(simIndex), runParam.optimize.exitFlag(simIndex), runParam.optimize.output(simIndex)] = ...
                fmincon(run_single_opt, startPars, [], [], [], [], lowerPars, upperPars, [], runParam.optimize.algorithmOptions);
        case 'fminsearch'
            [runParam.optimize.bestPars(simIndex,:), runParam.optimize.bestFom(simIndex), runParam.optimize.exitFlag(simIndex), runParam.optimize.output(simIndex)] = ...
                fminsearch(run_single_opt, startPars, runParam.optimize.algorithmOptions);
        case 'simulannealbnd'
            [runParam.optimize.bestPars(simIndex,:), runParam.optimize.bestFom(simIndex), runParam.optimize.exitFlag(simIndex), runParam.optimize.output(simIndex)] = ...
                simulannealbnd(run_single_opt, startPars, lowerPars, upperPars, runParam.optimize.algorithmOptions);
        case 'gamultiobj'
            fprintf('Not tested yet!\n');
            
        % NLopt optimizers
        case {'NLOPT_LN_SBPLX' 'NLOPT_LN_NELDERMEAD' 'NLOPT_GN_DIRECT_L' 'NLOPT_GN_DIRECT' 'NLOPT_GN_ISRES'}
            [runParam.optimize.bestPars(simIndex,:), runParam.optimize.bestFom(simIndex), runParam.optimize.exitFlag(simIndex)] = ...
                nlopt_optimize(opt, startPars);
            
    end
    
    % Transform back optimal parameters from normalized space [0,1] to real space and store them
    %         for ii = 1:runParam.optimize.no
    %             runParam.optimize.bestPars(ii) = interp1(normRange, realRange(ii,:), runParam.optimize.bestPars(ii));
    %         end
    
    runParam.optimize.optInd = runParam.optimize.optInd - 1;
    
    
    % Report results
    email_optimizationResults();
    
end


end
