%%%%% FUNCTION p_to_Ekin.m %%%%%
% Mattia Schaer
% April 2013
%
% Convert momentum to kinetic energy to momentum (and corresponding relativistic
% factors)
%
% CALL [beta, gamma, E_kin] = p_to_Ekin(p, m)
%           p:                      float(N)        [eV/c]
%           m:                      float(N)        [eV/c^2]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [beta, gamma, Ekin] = p_to_Ekin(p, m)
    beta    = sqrt( p.^2 ./ (p.^2+m.^2));
    gamma   = sqrt( 1 ./ (1-beta.^2) );
    Ekin    = (gamma-1) .* m;
end
