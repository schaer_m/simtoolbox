%%%%% FUNCTION email_optimizationResults.m %%%%%
% Mattia Schaer
% May 2014
%
% Email the results of the optimization
%
% CALL email_optimizationResults()
%
%%%%%



function email_optimizationResults()

global runParam;


%% Prepare contents
subject = ['[simToolbox] Results of optimization ' runParam.list(runParam.active.simInd).simFilebase];

% Best fom and corresponding parameters
body = ['bestInd: ' num2str(runParam.optimize.bestInd(runParam.active.simInd)) 10 ...
        'bestFom: ' sprintf('%.3E',runParam.optimize.bestFom(runParam.active.simInd)) ' (' runParam.fomDef{runParam.optimize.fomToOpt}.name ')' 10 ...
        'bestPars:' 10 ...
       ];
for ii = 1:runParam.optimize.no
    parStr = sprintf('%30s = %10f %4s', ...
            runParam.optimize.name{ii}, ...
            runParam.optimize.list(runParam.active.simInd, runParam.optimize.bestInd(runParam.active.simInd)).(runParam.optimize.name{ii}), ...
            runParam.(runParam.optimize.name{ii}).unit ...
    );
    body = [body parStr 10];
end
% Other foms
body = [body 'Other foms:' 10];
for jj = 1:numel(runParam.postprocess.fomIndices)
    [minVal minInd] = min(runParam.fom(jj, runParam.active.simInd, :));
    fomStr = sprintf('%30s = %10.3e   (best = %10.3e at iteration %d)', ...
            runParam.fomDef{runParam.postprocess.fomIndices(jj)}.name, ...
            runParam.fom(jj, runParam.active.simInd, runParam.optimize.bestInd(runParam.active.simInd)), ...
            minVal, minInd ...
    );
    body = [body fomStr 10];
end

figureName = ['optimization_diagnostic: ' runParam.list(runParam.active.simInd).simFilebase];
hFig = findobj('Type','figure', 'Name',figureName);
attachment1 = 'optimization_diagnostic.png';
print(hFig, '-dpng', attachment1);


%% Send email
setpref('Internet', 'SMTP_Server', runParam.smtpServer);
sendmail(runParam.emailAddress, subject, body, attachment1);


end
