%%%%% FUNCTION restart_run.m %%%%%
% Mattia Schaer
% March 2014
%
% Restart an interrupted run
%
% CALL restartRun = restart_run()
%           restartRun                  bool
%%%%%



function restartRun = restart_run(runFilepath, varargin)

global runParam;


load(runFilepath, '-mat');
runParam.newRun = false;

% Check if the restart index was specified
if nargin == 2
    runParam.active.restartInd = varargin{1};
end

if ~isempty(runParam.active.restartInd)
    answer = input(['\n********************\nRESTART\n' runFilepath ...
        '\nfrom iteration ' num2str(runParam.active.restartInd) '/' num2str(runParam.active.combinationsNo) '? N/Y [N]: '], 's');
    if strcmp(answer,'Y')
        restartRun = true;
    else
        restartRun = false;
    end
else
    fprintf('This run reached the end, nothing to restart.\n');
    restartRun = false;
end
if ~restartRun
    fprintf('\nEXITING\n********************\n\n');
end

end
