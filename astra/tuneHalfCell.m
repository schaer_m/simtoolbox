%%%%% FUNCTION tuneHalfCell.m %%%%%
% Mattia Schaer
% September 2013
%
% Take existing fieldmap and squeeze or stretch the first half cell. If
% required, plot some fieldmap diagnostic plots.
%
% CALL tunedFieldmapPath = tuneHalfCell(inFieldmapPath, frequency, outFieldmapPath, halfCellNewFracLength, diagnosticPlots)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function tunedFieldmapPath = tuneHalfCell(inFieldmapPath, frequency, outFieldmapPath, halfCellNewFracLength)

    activatePlot = false;

    % Load input fieldmap
    inFm    = load(inFieldmapPath);
    z       = inFm(:,1);
    Ez      = inFm(:,2);
    dz      = diff(z);
    dEz     = diff(Ez);
    
    % RF wavelength
    c = 2.99792458E8;     % [m/s]
    lambda = c / frequency;   % [m]
    % Find first Ez minimum (center of first full cell)
    searchMinInd = z < 3/4 * lambda;
    firstMinInd = find(dEz(searchMinInd)<0, 1, 'last') - 1;

    % Find first half cell length (electric)
    endHalfCellInd = find(Ez<0, 1, 'first') - 1;
    halfCellInd = 1:endHalfCellInd;
    fullCellsInd = endHalfCellInd+1:numel(z);
    halfCellLength = interp1(Ez(endHalfCellInd:endHalfCellInd+1), z(endHalfCellInd:endHalfCellInd+1), 0.0);
    halfCellFracLength = halfCellLength / (lambda/2);
    fprintf('Original half cell length = %f m = %f cells\n', halfCellLength, halfCellFracLength);
    stretchFactor = halfCellNewFracLength / halfCellFracLength;
    % If the correction is small, do not modify the fieldmap
    stretchThreshold = 0.01;
    if abs(stretchFactor-1) < stretchThreshold
        fprintf('Small stretch factor: %f --> no new fieldmap has been generated.\n', stretchFactor);
        tunedFieldmapPath = inFieldmapPath;
        return;
    end
    halfCellNewLength = halfCellLength * stretchFactor;
    fprintf('Tuned half cell length = %f m = %f cells\n', halfCellNewLength, halfCellNewFracLength);
    tunedFieldmapPath = outFieldmapPath;
        
    % Stretch fieldmap (method 1)
    % WARNING: zStretched isneeded by the other methods too!
    zStretched = z;
    % squeeze/stretch half cell according to input
    zStretched(halfCellInd) = z(halfCellInd) * stretchFactor;
    % adjust z values of full cells
    %lengthDifference = zStep * (fix(halfCellLength*(halfCellNewFracLength/halfCellFracLength-1)/zStep) + 1);
    lengthDifference = halfCellLength*(stretchFactor-1);
    zStretched(fullCellsInd) = z(fullCellsInd) + lengthDifference;
    dzStretched = diff(zStretched);
        
   if activatePlot

        % Spline interpolation (method 2)
        % take only few stretched points and make spline interpolation through them
        skipFactor = 100;
        zForInterp = zStretched(1:skipFactor:firstMinInd);
        dzForInterp = diff(zForInterp);
        EzForInterp = Ez(1:skipFactor:firstMinInd);
        dEzForInterp = diff(EzForInterp);
        zTuned = 0.0:dz(1):zStretched(firstMinInd);
        dzTuned = diff(zTuned);
        EzTuned = spline(zForInterp, EzForInterp, zTuned);
        dEzTuned = diff(EzTuned);

        % Cosine "interpolation" (method 3)
        % reproduce first half cell with a simple cosine
        EzCosine = Ez;
        EzCosine(halfCellInd) = cos(pi/(2*halfCellLength)/stretchFactor*zStretched(halfCellInd));
        dEzCosine = diff(EzCosine);

        % Cosine-weighted stretch (method 4)
        % this is a trial... could probably be improved...
        zWeighted = z;
        zWeighted(1:firstMinInd) = zWeighted(1:firstMinInd) - lengthDifference/2 * (1+cos(pi/2/(z(firstMinInd)-halfCellLength)*zWeighted(1:firstMinInd)));
        zWeighted = zWeighted + lengthDifference;
        dzWeighted = diff(zWeighted);
        
    end
    
    % Cubic interpolation (method 5)
    % 3. order polynomial matching values and derivatives at z=0 and z=halfCellNewLength
    slope0 = dEz(1) / dzStretched(1);
    slopeL = interp1(z(endHalfCellInd:endHalfCellInd+1), dEz(endHalfCellInd:endHalfCellInd+1)./dz(endHalfCellInd:endHalfCellInd+1), halfCellLength);
    a = (2+slope0*halfCellNewLength+slopeL*halfCellNewLength) / halfCellNewLength^3;
    b = (-3-2*slope0*halfCellNewLength-slopeL*halfCellNewLength) / halfCellNewLength^2;
    c = slope0;
    d = 1;
    EzCubic = Ez;
    EzCubic(halfCellInd) = a*zStretched(halfCellInd).^3 + b*zStretched(halfCellInd).^2 + c*zStretched(halfCellInd) + d;
    dEzCubic = diff(EzCubic);
    
    
    % Write new file with new fieldmap
    fout = fopen(outFieldmapPath, 'w');
    fprintf(fout, '%20.10f %20.10f\n', [zStretched'; EzCubic']);
    fclose(fout);


    % If required, plot diagnostic plots
    if activatePlot
        hFig = findobj('Type','figure', 'Name','tuneHalfCell');
        if isempty(hFig)
            hFig = figure('Name','tuneHalfCell', 'NumberTitle','off');
        end
        figure(hFig);
        % Field
        subplot(2,1,1);
        plot(z, Ez, 'b');
        xlabel('z [m]');
        ylabel('Ez [V/m]');
        ylim([-1.1 1.1]);
        hold on;
        plot([0.0 max(z(end),zStretched(end))], [0.0 0.0], 'k--');
        plot(halfCellLength, 0.0, 'bo');
        plot(z(firstMinInd), Ez(firstMinInd), 'bo');
        plot(zStretched, Ez, 'r');
        plot(zForInterp, EzForInterp, 'xg');
        plot(zTuned, EzTuned, 'g');
        plot(zStretched, EzCosine, 'c');
        plot(zWeighted, Ez, 'm');
        plot(zStretched, EzCubic, 'k');
        newLengthStr = num2str(halfCellNewFracLength);
        legend({[num2str(halfCellFracLength) ' half cell (original)'], ...
                '', ...
                'End of half cell', ...
                'Center of first full cell', ...
                [newLengthStr ' half cell, stretched'], ...
                'Points considered for spline', ...
                [newLengthStr ' half cell, spline'], ...
                [newLengthStr ' half cell, simple cosine'], ...
                [newLengthStr ' half cell, cosine-weighted'], ...
                [newLengthStr ' half cell, cubic'], ...
        });
        hold off;
        % Derivative
        subplot(2,1,2)
        plot(z(1:end-1), dEz./dz, 'b');
        xlabel('z [m]');
        ylabel('dEz/dz [V/m^2]');
        hold on;
        plot([0.0 max(z(end),zStretched(end))], [0.0 0.0], 'k--');
        plot(halfCellLength, dEz(endHalfCellInd)/dz(endHalfCellInd), 'bo');
        plot(z(firstMinInd), dEz(firstMinInd)/dz(firstMinInd), 'bo');
        plot(zStretched(1:end-1), dEz./dzStretched, 'r');
        plot(zForInterp(1:end-1), dEzForInterp./dzForInterp, 'xg');
        plot(zTuned(1:end-1), dEzTuned./dzTuned, 'g');
        plot(zStretched(1:end-1), dEzCosine./dzStretched, 'c');
        plot(zWeighted(1:end-1), dEz./dzWeighted, 'm');
        plot(zStretched(1:end-1), dEzCubic./dzStretched, 'k');
        legend({[num2str(halfCellFracLength) ' half cell (original)'], ...
                '', ...
                'End of half cell', ...
                'Center of first full cell', ...
                [newLengthStr ' half cell, stretched'], ...
                'Points considered for spline', ...
                [newLengthStr ' half cell, spline'], ...
                [newLengthStr ' half cell, simple cosine'], ...
                [newLengthStr ' half cell, cosine-weighted'], ...
                [newLengthStr ' half cell, cubic'], ...
        });
        hold off;
    end

end
