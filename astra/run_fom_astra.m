%%%%% FUNCTION run_fom_astra.m %%%%%
% Mattia Schaer
% March 2014
%
% Compute figure-of-merits.
%
% CALL fom = run_fom_astra(fomDefinition, actualSimulationParameters)
%           fomDefinition                       struct
%           actualSimulationParameters          struct
%
%           fom                                 float
%
%%%%%



function fom = run_fom_astra(fomDef, actualPars)


switch fomDef.name
    
    case {'sliceEmit' 'mismatchPar' 'peakCurrent' 'sliceBrightness' ...
          'slice+mismatch' 'slice+mismatch+flat' 'brightness+mismatch' ...
          'bunchCharge'}
        % Read the results
        finalBeam = get_beamParameters(actualPars.finalDistrFilename, fomDef);
        % Calculate FIGURE OF MERIT
        fom = figureOfMerit_slice(fomDef, finalBeam);
        
    case {'brightness' 'brightness_ferrarioMatching' 'projectedEmit_min' ...
          'projectedEmit_min-(max-min)' 'projectedEmit_flatness' ...
          'averageEnergy_at' 'projectedEmit_at' 'dEdz_at' 'current' ...
          'energySpread_at' 'emittedCharge'}
        % Read the results
        XemitData   = get_lineParameters(actualPars.xLineFilename);
        YemitData   = get_lineParameters(actualPars.yLineFilename);
        ZemitData   = get_lineParameters(actualPars.zLineFilename);
        refData     = get_lineParameters(actualPars.refLineFilename);
        % TODO emitted charge and cathodeLineFilename
        %cathodeData = get_lineParameters(actualPars.cathodeLineFilename);
        % Other stuff
        if isfield(fomDef, 'zLocation')
            if ~isfield(fomDef, 'zAt')
                if isfield(actualPars, fomDef.zLocation)
                    fomDef.zAt = actualPars.(fomDef.zLocation);
                else
                    error('Unknown zLocation');
                end
            else
                error('Conflicting zLocation and zAt');
            end        
        end
        if strcmp(fomDef.name, 'projectedEmit_flatness')
            if ~isfield(fomDef, 'zEmitStart') && ~isfield(fomDef, 'zEmitEnd')
                fomDef.zEmitStart = actualPars.z_firstRegularCellEnd;
                fomDef.zEmitEnd = actualPars.z_lastRegularCellEnd;
            end
        end

        % Calculate FIGURE OF MERIT
        fom = figureOfMerit_projected(fomDef, {XemitData, YemitData, ZemitData, refData});
        
    otherwise
        error('Unknown fomDef.name');
        
end

end
