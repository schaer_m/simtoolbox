%%%%% FUNCTION generate_idealTw.m %%%%%
% Mattia Schaer
% May 2015
%
% Generate a fieldmap for a TW gun with ideal sine shape of the field. In
% ASTRA this is obtained by summing a cos(kz) and a sin(kz) together.
%
% CALL generate_idealTw(cellNum, phaseAdvance, frequency, gunFmFilepath)
%           cellNum                                 int
%           phaseAdvance                            float
%           frequency                               float
%           gunFmFilepath                           string
%
%%%%%



function generate_idealTw(cellNum, phaseAdvance, frequency, gunFmFilepath)

global constVars;


lambda = constVars.c.si / frequency;   % [m]
k = 2*pi / lambda;

zStep = 0.025E-3;   % [m]
zEnd = cellNum * phaseAdvance * lambda;
z = 0.0:zStep:zEnd;

Ez_re = cos(k*z);
Ez_im = sin(k*z);

% plot(z,Ez);


%% Save fieldmap

fOut = fopen([gunFmFilepath '_re.astra'], 'w');
fprintf(fOut, '%20.8e %20.8e\n', [z; Ez_re]);
fclose(fOut);

fOut = fopen([gunFmFilepath '_im.astra'], 'w');
fprintf(fOut, '%20.8e %20.8e\n', [z; Ez_im]);
fclose(fOut);


end
