%%%%% FUNCTION generate_idealSw.m %%%%%
% Mattia Schaer
% March 2015
%
% Generate a fieldmap for a SW gun with ideal cosine shape of the field
%
% CALL generate_idealSw(fullCellNum, frequency, gunFmFilepath, [halfCellLength])
%           fullCellNum                             int
%           frequency                               float
%           gunFmFilepath                           string
%           [halfCellLength]                        float
%
%%%%%



function generate_idealSw(fullCellNum, frequency, gunFmFilepath, varargin)

global constVars;


switch nargin
    case 3
        halfCellLength = 0.5;
    case 4
        halfCellLength = varargin{1};
    otherwise
        error('Unknown input sequence');
end


lambda = constVars.c.si / frequency;   % [m]
k = 2*pi / lambda;

zStep = 0.025E-3;   % [m]
zEnd = (halfCellLength + fullCellNum) * lambda/2;
z = 0.0:zStep:zEnd;

Ez = cos(k*z);

% plot(z,Ez);


%% Save fieldmap
fOut = fopen(gunFmFilepath, 'w');
fprintf(fOut, '%20.8e %20.8e\n', [z; Ez]);
fclose(fOut);


end
