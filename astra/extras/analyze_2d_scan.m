%%%%% FUNCTION analyze_2d_scan.m %%%%%
% Mattia Schaer
% March 2014
%
% Make picture to visualize the 2D phase space of a parameter sweep
%
% CALL analyze_2d_scan(runParametersFilepath, quantityDefinition, generateImageFlag)
%           runParametersFilepath                   string
%           quantityDefinition                      struct
%                                                   string
%           generatImageFlag                        bool
%
%%%%%



function analyze_2d_scan(runParamFilepath, quantityDef, generateImage)

global runParam;


load(runParamFilepath, '-mat');
[path, ~, ~] = fileparts(runParamFilepath);

% Use shorter variable names
paramNames      = runParam.active.name;
paramLengths    = runParam.active.length;
paramValues     = cell(2, 1);
for ii = 1:2
    for jj = 1:paramLengths(ii)
        paramValues{ii}{jj}   = [sprintf('%.5f',runParam.(paramNames{ii}).nom+runParam.(paramNames{ii}).var(jj)) ' ' runParam.(paramNames{ii}).unit];
    end
end
% Prepare data
if isstruct(quantityDef)
    quantityName = quantityDef.name;
elseif ischar(quantityDef)
    quantityName = quantityDef;
end
quantity = zeros(paramLengths);
for ii=1:runParam.active.combinationsNo
    tmpQuantity = run_fom(quantityDef, runParam.list(ii));
    quantity(ind2sub(paramLengths, ii)) = tmpQuantity;
end

% Find minimum and corresponding indices
bestSetsOutNo = 10;
for nn = 1:2
    if nn == 1
        [quantitySorted, linIndSorted] = sort(quantity(:), 'descend');
        fprintf('\n%d parameter sets with LARGEST quantity:\n', bestSetsOutNo);
    elseif nn == 2
        [quantitySorted, linIndSorted] = sort(quantity(:), 'ascend');
        fprintf('\n%d parameter sets with SMALLEST quantity:\n', bestSetsOutNo);
    end
    for kk = 1:bestSetsOutNo
        fprintf('\n\tSim%d: FOM = %f:\n', linIndSorted(kk), quantitySorted(kk));
        [ind{1:3}] = ind2sub(paramLengths, linIndSorted(kk));
        for ii = 1:2
            fprintf('\t\t%10s = %10s\t(ind %d/%d)\n', paramNames{ii}, paramValues{ii}{ind{ii}}, ind{ii}, paramLengths(ii));
        end
    end
end

if generateImage
    % Make image
    figure(1);
    % Draw image
    imagesc(quantity, [min(quantity(:)) max(quantity(:))]);
    hColorbar = colorbar();
    title(hColorbar, quantityName);
    % Mark NaN with a white cross
    hold on;
    for ii=1:runParam.active.combinationsNo
        if isnan(quantity(ii))
            [subind1, subind2] = ind2sub(paramLengths, ii);
            plot(subind2, subind1, 'wx', 'MarkerSize',20);
        end
    end
    hold off;
    % Axis and labels
    xlabel(paramNames{2});
    ylabel(paramNames{1});
    set(gca, 'XTick', 1:paramLengths(2));
    xTickLabels         = cell(1, paramLengths(2));
    xTickLabels{1}      = paramValues{2}{1};
    xTickLabels{end}    = paramValues{2}{end};
    set(gca, 'XTickLabel', xTickLabels);
    set(gca, 'YTick', 1:paramLengths(1));
    yTickLabels         = cell(1, paramLengths(1));
    yTickLabels{1}      = paramValues{1}{1};
    yTickLabels{end}    = paramValues{1}{end};
    set(gca, 'YTickLabel', yTickLabels);
end

end
