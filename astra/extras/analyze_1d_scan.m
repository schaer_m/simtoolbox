%%%%% FUNCTION analyze_1d_scan.m %%%%%
% Mattia Schaer
% March 2015
%
% Make picture to visualize the 1D phase space of a parameter sweep
%
% CALL [paramValues, quantity] = analyze_1d_scan(runParametersFilepath, quantityDefinition, generateImageFlag, [requiredSimInd])
%           runParametersFilepath                   string
%           quantityDefinition                      struct
%                                                   string
%           generatImageFlag                        bool
%           requiredSimInd                          int(N)
%
%           paramValues                             float(N)
%           quantity                                float(N)
%
%%%%%



function [paramValues, quantity] = analyze_1d_scan(runParamFilepath, quantityDef, generateImage, varargin)

global runParam;


load(runParamFilepath, '-mat');
switch nargin
    case 3
        % All simulations are considered
        requiredSimInd = 1:runParam.active.length;
    case 4
        requiredSimInd = varargin{1};
end

% Use shorter variable names
paramName       = runParam.active.name{1};
paramLength     = runParam.active.length;
for ii = 1:numel(requiredSimInd)
    paramValues(ii)             = runParam.list(requiredSimInd(ii)).(paramName);
    paramValuesString{ii}   = [sprintf('%.5f',runParam.(paramName).nom+runParam.(paramName).var(requiredSimInd(ii))) ' ' runParam.(paramName).unit];
end
% Prepare data
if isstruct(quantityDef)
    quantityName = quantityDef.name;
elseif ischar(quantityDef)
    quantityName = quantityDef;
end
quantity = zeros(1, numel(requiredSimInd));
for ii = 1:numel(requiredSimInd)
    quantity(ii) = run_fom(quantityDef, runParam.list(requiredSimInd(ii)));
end

% Find minimum and corresponding indices
bestSetsOutNo = 10;
if bestSetsOutNo > numel(requiredSimInd)
    bestSetsOutNo = numel(requiredSimInd);
end
for nn = 1:2
    if nn == 1
        [quantitySorted, linIndSorted] = sort(quantity(:), 'descend');
        fprintf('\n%d parameter sets with LARGEST quantity:\n', bestSetsOutNo);
    elseif nn == 2
        [quantitySorted, linIndSorted] = sort(quantity(:), 'ascend');
        fprintf('\n%d parameter sets with SMALLEST quantity:\n', bestSetsOutNo);
    end
    for kk = 1:bestSetsOutNo
        fprintf('\n\tSim%d: FOM = %f:\n', requiredSimInd(linIndSorted(kk)), quantitySorted(kk));
        paramValuesSorted = paramValues(linIndSorted);
        paramValuesStringSorted = paramValuesString{linIndSorted};
        fprintf('\t\t%10s = %10s\t(ind %d/%d)\n', paramName, paramValuesStringSorted, requiredSimInd(linIndSorted(kk)), paramLength);
    end
end

if generateImage
    % Make image
    figure(1);
    % Draw plot
    plot(paramValues, quantity, '.');
    % Mark NaN with a cross
    hold on;
    for ii=1:numel(requiredSimInd)
        if isnan(quantity(ii))
            plot(paramValues(ii), quantity(ii), 'rx', 'MarkerSize',20);
        end
    end
    hold off;
    % Axis and labels
    xlabel(paramName, 'Interpreter', 'none');
    ylabel(quantityName, 'Interpreter', 'none');
end


end
