%%%%% FUNCTION analyze_3d_scan_combine.m %%%%%
% Mattia Schaer
% January 2014
%
% Make movies to surf through the 3D phase space of a parameter sweep
%
% CALL analyze_3d_scan(runParamFilepath, generateMovie)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function analyze_3d_scan_combine(runParamFilepath, quantityDef, generateMovie)

global runParam;


load(runParamFilepath, '-mat');
[path, ~, ~] = fileparts(runParamFilepath);


%% Use row vectors for later use of circshift()
paramNames      = runParam.active.name';
paramLengths    = runParam.active.length';
paramValues     = cell(3, 1);
for ii = 1:3
    for jj = 1:paramLengths(ii)
        paramValues{ii}{jj}   = [sprintf('%.5f',runParam.(paramNames{ii}).nom+runParam.(paramNames{ii}).var(jj)) ' ' runParam.(paramNames{ii}).unit];
    end
end





% %% Find minimum and corresponding indices
% bestSetsOutNo = 10;
% for nn = 1:2
%     if nn == 1
%         [quantitySorted, linIndSorted] = sort(quantity(:), 'descend');
%         fprintf('\n%d parameter sets with LARGEST quantity:\n', bestSetsOutNo);
%     elseif nn == 2
%         [quantitySorted, linIndSorted] = sort(quantity(:), 'ascend');
%         minAbsoluteLinInd = linIndSorted(1);
%         fprintf('\n%d parameter sets with SMALLEST quantity:\n', bestSetsOutNo);
%     end
%     for kk = 1:bestSetsOutNo
%         fprintf('\n\tSim%5d) %s = %f:\n', linIndSorted(kk), quantityName, quantitySorted(kk));
%         [ind{1:3}] = ind2sub(paramLengths', linIndSorted(kk));
%         for ii = 1:3
%             fprintf('\t\t%10s = %10s\t(ind %d/%d)\n', paramNames{ii}, paramValues{ii}{ind{ii}}, ind{ii}, paramLengths(ii));
%         end
%     end
% end


%% Make movie
if generateMovie
    for direction = 1:3
        
        if direction > 1
            paramNames      = circshift(paramNames, -1);
            paramLengths    = circshift(paramLengths, -1);
            paramValues     = circshift(paramValues, -1);
        end
        
        
        h1 = figure('units','normalized', 'outerposition',[0 0 1 1]);
        writerObj = VideoWriter(fullfile(path, ['quantities_along-' paramNames{1} '.avi']));
        writerObj.FrameRate = 1;
        open(writerObj);
        for sliceInd = 1:paramLengths(1)
            
            clf(h1);
            for jj = 1:numel(quantityDef)
                % Prepare data
                alreadyPostprocessed = false;
                if isstruct(quantityDef{jj})
                    quantityName = quantityDef{jj}.name;
                    % Check if that was already postprocessed
                    if runParam.postprocess.start
                        fomReadoutIndex = 0;
                        for fomInd = runParam.postprocess.fomIndices
                            fomReadoutIndex = fomReadoutIndex + 1;
                            if strcmp(quantityName, runParam.fomDef{fomInd}.name)
                                alreadyPostprocessed = true;
                                %fprintf('%s was already postprocessed as fomDef{%d} and stored in fom(%d,:)\n', quantityName, fomInd, fomReadoutIndex);
                                break;
                            end
                        end
                    end
                elseif ischar(quantityDef{jj})
                    quantityName = quantityDef{jj};
                end
                % Read out / Compute requested quantity
                quantity = zeros(runParam.active.length);
                for ii=1:runParam.active.combinationsNo
                    if alreadyPostprocessed
                        quantity(ind2sub(runParam.active.length', ii)) = runParam.fom(fomReadoutIndex,ii);
                    else
                        tmpQuantity = run_fom(quantityDef{jj}, runParam.list(ii));
                        quantity(ind2sub(runParam.active.length', ii)) = tmpQuantity;
                    end
                end
                % Shift dimensions for next movie
                % WARNING: always shift all used variables together in order to
                % avoid confusions!
                for ii= 1:(direction-1)
                    quantity = shiftdim(quantity, 1);
                end
                
                % Draw slice
                subplot(2,4,jj);
                sliceQuantity = squeeze(quantity(sliceInd,:,:));
                imagesc(sliceQuantity, [min(quantity(:)) max(quantity(:))]);
                hColorbar = colorbar();
                title(hColorbar, quantityName);
                hold on;
                % Mark NaN with a white cross
                for ii = 1:paramLengths(2)*paramLengths(3)
                    if isnan(sliceQuantity(ii))
                        [subind1, subind2] = ind2sub(paramLengths([2 3]), ii);
                        plot(subind2, subind1, 'wx', 'MarkerSize',20, 'LineWidth',3);
                    end
                end
                % Mark smaller sliceQuantity with a green circle
                [~, minSliceInd] = min(sliceQuantity(:));
                [subind1, subind2] = ind2sub(paramLengths([2 3]), minSliceInd);
                plot(subind2, subind1, 'og', 'MarkerSize',20, 'LineWidth',3);
                %             % Mark absolute smaller quantity with a gree cross
                %             tmpCell = num2cell(circshift([sliceInd; subind1; subind2], (direction-1))');
                %             tmpCell = {runParam.active.length, tmpCell{:}};
                %             minSliceLinInd = sub2ind(tmpCell{:});
                %             if minSliceLinInd == minAbsoluteLinInd
                %                 plot(subind2, subind1, 'xg', 'MarkerSize',20, 'LineWidth',3);
                %             end
                hold off;
                title([paramNames{1} ' = ' paramValues{1}{sliceInd}]);
                xlabel(paramNames{3});
                ylabel(paramNames{2});
                set(gca, 'XTick', 1:paramLengths(3));
                xTickLabels         = cell(1, paramLengths(3));
                xTickLabels{1}      = paramValues{3}{1};
                xTickLabels{end}    = paramValues{3}{end};
                set(gca, 'XTickLabel', xTickLabels);
                set(gca, 'YTick', 1:paramLengths(2));
                yTickLabels         = cell(1, paramLengths(2));
                yTickLabels{1}      = paramValues{2}{1};
                yTickLabels{end}    = paramValues{2}{end};
                set(gca, 'YTickLabel', yTickLabels);
            end
            
            % Record slice
            writeVideo(writerObj, getframe(h1));
            
        end
        
        close(writerObj);
        close(h1);
        
    end
end


end
