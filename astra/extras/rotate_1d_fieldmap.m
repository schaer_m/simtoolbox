%%%%% FUNCTION rotate_1d_fieldMap.m %%%%%
% Mattia Schaer
% July 2015
%
% Take the on-axis Ez complex field from HFSS and rotate to a chosen phase.
% The phase phi = 0 is defined to be when Im[Ez(z=0)] = 0. This is our
% typical choice for the input fieldmaps for ASTRA, because only one
% element matters for the extraction. Probably not that important but...
%
% CALL [] = rotate_1d_fieldMap(fieldFilepath, [newPhase], [outFilebase=fieldFilepath(without extension)])
%               fieldFilepath           string
%               newPhase                float
%
%%%%%



function [] = rotate_1d_fieldmap(fieldFilepath, varargin)


switch nargin
    case 1
        newPhase = [];   % leave original phase
        outFilebase = fieldFilepath(1:end-4);
    case 2
        newPhase = varargin{1};   % [deg]
        outFilebase = [fieldFilepath(1:end-4) '_rotated'];
    case 3
        newPhase = varargin{1};   % [deg]
        outFilebase = varargin{2};
    otherwise
        error('Wrong number of input parameters');
end


%% Import data

rawData = importdata(fieldFilepath, '\t', 1);

z           = rawData.data(:,1);
Ez_complex  = complex(rawData.data(:,2), rawData.data(:,3));


%% Rotate field

if ~isempty(newPhase)
    originalPhase = angle(Ez_complex(1));
    Ez_complex = Ez_complex * exp(1i*(-originalPhase+deg2rad(newPhase)));
end

% Convert z from mm to m
%z = z / 1E3;
Ez_re = real(Ez_complex);
Ez_im = imag(Ez_complex);

% Plot for checking
figure(1);
plot(z, Ez_re, z, Ez_im);
xlabel('z [m]');
ylabel('Ez [arb. units]');
legend('Re[Ez]', 'Im[Ez]');


%% Save to input file for ASTRA

zEzForSaving = [z Ez_re];
save([outFilebase '_re.astra'], 'zEzForSaving', '-ascii');
zEzForSaving = [z Ez_im];
save([outFilebase '_im.astra'], 'zEzForSaving', '-ascii');

% Parameters needed in the run_ file for the real and imaginary part
% normalization
fprintf('Input values for ASTRA Toolbox:\n');
fprintf('gunFmReNorm =  %f\n', max(abs(Ez_re)) / max(abs(Ez_complex)));
fprintf('gunFmImNorm =  %f\n', max(abs(Ez_im)) / max(abs(Ez_complex)));


end
