%%%%% FUNCTION compare_1d_fieldMap.m %%%%%
% Mattia Schaer
% August 2015
%
% Compare complex fieldmaps of tw guns by plotting them
%
% CALL [] = compare_1d_fieldMap(baseFieldmapFilepath1, baseFieldmapFilepath2, [..., baseFieldmapFilepathN])
%               baseFieldmapFilepath1       string
%               baseFieldmapFilepath2       string
%
%%%%%



function [] = compare_1d_fieldmap(varargin)


if nargin < 2
    error('Too few input fieldmaps.');
end

figure(1);

for ii = 1:nargin
    
    rawData_re = importdata([varargin{ii} '_re.astra']);
    rawData_im = importdata([varargin{ii} '_im.astra']);
    tmpZRe = rawData_re(:,1);
    tmpEzRe = rawData_re(:,2);
    tmpZIm = rawData_im(:,1);
    tmpEzIm = rawData_im(:,2);
    
    % Check if imaginary part is missing the first point (Lukas'
    % implementation), and in case forget about the first point
    if size(tmpZIm,1) == size(tmpZRe,1)-1
        tmpZRe = tmpZRe(2:end);
        tmpEzRe = tmpEzRe(2:end);
    % If it is not the case, the dimensions must be the same
    elseif size(tmpZIm,1) ~= size(tmpZRe,1)
        error('The sampling in z is not the same for real and imaginary part.');
    end
    
    
    complexE = tmpEzRe + 1i*tmpEzIm;
    subplot(2,1,1);
    plot(tmpZRe, abs(complexE)/max(abs(complexE)));
    hold all;
    subplot(2,1,2);
    plot(tmpZRe, angle(complexE)/pi*180);
    hold all;
    
end
    
subplot(2,1,1);
hold off;
xlabel('z [m]');
ylabel('abs(complexEz)');
legend(varargin, 'Interpreter','none');
subplot(2,1,2);
hold off;
xlabel('z [m]');
ylabel('phase(complexEz) [deg]');


end
