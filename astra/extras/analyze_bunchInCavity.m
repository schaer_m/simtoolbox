%%%%% FUNCTION analyze_bunchInCavity.m %%%%%
% Mattia Schaer
% July 2014
%
% Make movie of the bunch riding the crest of the accelerating field
%
% CALL analyze_bunchInCavity(runParamFilepath, generateMovie)
%%%%%



function analyze_bunchInCavity(runParamFilepath, simIndex)



load(runParamFilepath, '-mat');


%% Read in fieldmap
fm = importdata(fullfile(runParam.workingPath, runParam.runFolderName, 'fieldmapsLocal', runParam.list(simIndex).gunFmRe));
z            = fm(:, 1);
EzRe         = fm(:, 2);
fm = importdata(fullfile(runParam.workingPath, runParam.runFolderName, 'fieldmapsLocal', runParam.list(simIndex).gunFmIm));
% Same grid for real and imaginary part
EzIm         = interp1(fm(:,1), fm(:, 2), z);
EzIm(isnan(EzIm)) = 0.0;


%% Read in ASTRA solution
% Kinematic of reference particle
refParticle = get_lineParameters(fullfile(runParam.workingPath, runParam.runFolderName, [runParam.list(simIndex).simFilebase '.ref.' num2str(runParam.list(simIndex).astraId,'%03d')]));
% Longitudinal bunch properties
bunch = get_lineParameters(fullfile(runParam.workingPath, runParam.runFolderName, [runParam.list(simIndex).simFilebase '.Zemit.' num2str(runParam.list(simIndex).astraId,'%03d')]));


%% Generate movie
hf1 = figure(1);
lastMovieInd = find(refParticle.z < z(end), 1, 'last');
movieInd = 1:fix(numel(refParticle.t)/5000):lastMovieInd;
phase = 2*pi*runParam.list(simIndex).gunNue*refParticle.t(movieInd) + runParam.list(simIndex).gunPhi*pi/180;
% Record video
writerObj = VideoWriter(fullfile(runParam.workingPath, runParam.runFolderName, 'bunchInCavity.avi'));
writerObj.FrameRate = 10;
open(writerObj);
% F(numel(phase)) = struct('cdata',[], 'colormap',[]);
for ii = 1:numel(phase)
    %Ez = -( EzRe * sin(phase(ii)) + EzIm * cos(phase(ii)) );
    Ez = -( EzRe * sin(phase(ii)) + EzIm * sin(pi/2-phase(ii)) );
    plot(z, Ez/1E6);
    hold on;
    zRef = refParticle.z(movieInd(ii));
    plot(zRef, interp1(z,Ez,zRef)/1E6, 'ro');
    zTail = zRef - interp1(bunch.z,bunch.zRms,zRef)*3*1E-3;
    plot(zTail, interp1(z,Ez,zTail)/1E6, 'bo');
    zHead = zRef + interp1(bunch.z,bunch.zRms,zRef)*3*1E-3;
    plot(zHead, interp1(z,Ez,zHead)/1E6, 'go');
    hold off;
    ylim([-150 150]);
    title(['phase = ' num2str(round(phase(ii)*180/pi), '%3d')]);
    xlabel('z [m]');
    ylabel('E_z [MV/m]');
    % record slice
    writeVideo(writerObj, getframe(hf1));
%     F(ii) = getframe(gcf);
end
close(writerObj);
% movie(gcf, F, 3);


end
