%%%%% FUNCTION analyze_particleTrajectories.m %%%%%
% Mattia Schaer
% December 2014
%
% Make plots of tracked particles for which the trajectory was stored.
%
% CALL analyze_particleTrajectories(trackFilepaths, [simulationLabels])
%           trackFilepaths:             cell(N)
%           simulationLabels:           cell(N)
%
%%%%%



function [] = analyze_particleTrajectories(trackFilepaths, varargin)


switch nargin
    case 1
        % do nothing
    case 2
        zFaradayCup = varargin{1};
    otherwise
        error('Unknown input sequence');
end


for ii = 1:numel(trackFilepaths)
    
    [trackedParticles, ~] = get_trackedParticles(trackFilepaths{ii});
    maxZ = zeros(1, numel(trackedParticles));
    
    %% Plot trajectory
    figure(1);
    subplot(numel(trackFilepaths), 1, ii);
    for jj = 1:numel(trackedParticles)
        plot(trackedParticles(jj).z, sqrt(trackedParticles(jj).x.^2 + trackedParticles(jj).y.^2));
        hold all;
        jj;
        sum(trackedParticles(jj).ind==jj);
        max(trackedParticles(jj).z);
        startZ(jj) = trackedParticles(jj).z(1);
        endZ(jj) = trackedParticles(jj).z(end);
        minZ(jj) = min(trackedParticles(jj).z);
        maxZ(jj) = max(trackedParticles(jj).z);
        startEz(jj) = trackedParticles(jj).Ez(2);
        startEx(jj) = trackedParticles(jj).Ex(2);
        startEy(jj) = trackedParticles(jj).Ey(2);
        fprintf('Final r = %f , Initial Ez = %f\n', ...
                sqrt(trackedParticles(jj).x(end)^2+trackedParticles(jj).y(end)^2), ...
                trackedParticles(jj).Ez(1));
    end
    hold off;
    xlabel('z [m]');
    ylabel('r [mm]');

    %% Select particles reaching a certain position
    if nargin == 2
        reachingInd = maxZ > zFaradayCup;
        sum(reachingInd)
        sum(~reachingInd)
        % Starting positions
        startZ(reachingInd)
        startEz(reachingInd)
        startEx(reachingInd)
        startEy(reachingInd)
    end


end
