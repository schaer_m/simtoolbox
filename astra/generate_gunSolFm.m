%%%%% FUNCTION generate_gunSolFm.m %%%%%
% Mattia Schaer
% February 2015
%
% Generate a fieldmap for the gun solenoid with a given slope at the
% cathode and a given length.
%
% CALL generate_gunSolFm(gunSolFmFWHM, gunSolFmSlope, gunSolFmFilepath)
%           gunSolFmFWHM                            float
%           gunSolFmSlope                           float
%           gunSolFmFilepath                        string
%
%%%%%



function generate_gunSolFm(gunSolFmFWHM, gunSolFmSlope, gunSolFmFilepath)


zStep = 0.025E-3;   % [m]
slopeRef = 10E-3;   % [m]
zRoundingRange = 0.2*slopeRef/gunSolFmSlope;   % [m]

if gunSolFmFWHM < slopeRef/gunSolFmSlope+zRoundingRange
    warning('The FWHM of the solenoid is too short for the chosen slope');
end

z = 0.0:zStep:gunSolFmFWHM+slopeRef/gunSolFmSlope+2*zRoundingRange;
Bz = zeros(1, numel(z));

zLin1 = slopeRef/gunSolFmSlope;
zLin1Ind = round(zLin1/zStep)+1;
z1 = zLin1 - zRoundingRange/2;
z1Ind = round(z1/zStep)+1;
z2 = zLin1 + zRoundingRange/2;
z2Ind = round(z2/zStep)+1;
a = gunSolFmSlope/slopeRef/(2*(z1-z2));

% Raising field
Bz(1:zLin1Ind) = linspace(0, 1, zLin1Ind);
% Round edge
Bz(z1Ind:z2Ind) = 1 + a*(z(z1Ind:z2Ind) - z2) .^2;

zLin2Ind = round(gunSolFmFWHM/zStep)+1;
zLin3 = zLin1 + gunSolFmFWHM;
zLin3Ind = round(zLin3/zStep)+1;
z3 = gunSolFmFWHM - zRoundingRange/2;
z3Ind = round(z3/zStep)+1;
z4 = gunSolFmFWHM + zRoundingRange/2;
z4Ind = round(z4/zStep)+1;

% Flat region
Bz(z2Ind+1:z3Ind-1) = 1;
% Falling field
Bz(zLin2Ind:zLin3Ind) = linspace(1, 0, zLin3Ind-zLin2Ind+1);
% Round edge
Bz(z3Ind:z4Ind) = 1 + a*(z(z3Ind:z4Ind) - z3) .^2;


z5 = zLin3 - zRoundingRange/2;
z5Ind = round(z5/zStep)+1;
z6 = zLin3 + zRoundingRange/2;
z6Ind = round(z6/zStep)+1;

% Round edge
Bz(z5Ind:z6Ind) = - a*(z(z5Ind:z6Ind) - z6) .^2;

%plot(z, Bz);


%% Save fieldmap
fOut = fopen(gunSolFmFilepath, 'w');
fprintf(fOut, '%20.8e %20.8e\n', [z; Bz]);


end
