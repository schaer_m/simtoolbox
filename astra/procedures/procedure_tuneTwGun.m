%%%%% FUNCTION procedure_tuneTwGun.m %%%%%
% Mattia Schaer
% April 2014
%
% Tune SW cell length and TW cell number and run ASTRA
% simulation with the corresponding fieldmaps.
%
% CALL [modifiedSimulationParameters] = procedure_tuneTwGun([actualSimulationParameters])
%           actualSimulationParameters              struct
%
%           modifiedSimulationParameters            struct
%
%%%%%



function varargout = procedure_tuneTwGun(varargin)

global runParam;


%% Manage input
switch nargin
    case 0
        actualPars      = runParam.list(runParam.active.simInd);
        restartingSim   = runParam.simProgress.started(runParam.active.simInd); %#ok<NASGU>
    case 1
        actualPars      = varargin{1};
        restartingSim   = false; %#ok<NASGU>
    otherwise
        error('Unknown sequence of input parameters');
end


%% Tune Hybrid Gun

% Load MAFIA fieldmaps
HYBicp_re   = load(fullfile('fieldmaps', [actualPars.inputCouplerFm '_re.txt']));
HYBicp_im   = load(fullfile('fieldmaps', [actualPars.inputCouplerFm '_im.txt']));
HYBocp_re   = load(fullfile('fieldmaps', [actualPars.outputCouplerFm '_re.txt']));
HYBocp_im   = load(fullfile('fieldmaps', [actualPars.outputCouplerFm '_im.txt']));
HYBtwre     = load(fullfile('fieldmaps', [actualPars.normalCellsFm '_re.txt']));
HBtwim      = load(fullfile('fieldmaps', [actualPars.normalCellsFm '_im.txt']));

HYBphase_adv = 360.0 * actualPars.HYBn / actualPars.HYBm;
HYBphasor0 = phase(HYBtwre(1,2) + 1i*HBtwim(1,2));

% Scale z-axis of input SW-part
HYBsax = size(HYBicp_re(:,1));
HYBizSWscaleEnd = find(min(abs(HYBicp_re(:,1)-actualPars.HYBzSWscaleEnd)) == abs(HYBicp_re(:,1)-actualPars.HYBzSWscaleEnd));
HYBizSWscaleEndZ = HYBicp_re(HYBizSWscaleEnd,1);
HYBtune(1:HYBizSWscaleEnd) = actualPars.HYBdL1/2*(1+cos(pi*(HYBicp_re(1:HYBizSWscaleEnd,1)-HYBicp_re(1,1))/(HYBizSWscaleEndZ-HYBicp_re(1,1))));
HYBtune(HYBizSWscaleEnd+1:HYBsax(1)) = 0.0;
HYBicp_re(:,1) = HYBicp_re(:,1)-HYBtune'+actualPars.HYBdL1;
HYBicp_im(:,1) = HYBicp_im(:,1)-HYBtune'+actualPars.HYBdL1;

% Scale z-axis of output part
HYBsax = size(HYBocp_re(:,1));
HYBocp_re(:,1) = HYBocp_re(:,1)-HYBocp_re(1,1);
HYBocp_im(:,1) = HYBocp_im(:,1)-HYBocp_im(1,1);
HYBizOCPscaleEnd = find(min(abs(HYBocp_re(:,1)-actualPars.HYBLo))==abs(HYBocp_re(:,1)-actualPars.HYBLo));
HYBizOCPscaleEndZ = HYBocp_re(HYBizOCPscaleEnd,1);
HYBotune(1:HYBizOCPscaleEnd) = actualPars.HYBdLo/2*(1+cos(pi*(HYBocp_re(1:HYBizOCPscaleEnd,1) - HYBocp_re(1,1))/(HYBizOCPscaleEndZ-HYBocp_re(1,1))));
HYBotune(HYBizOCPscaleEnd+1:HYBsax(1)) = 0.0;
HYBocp_re(:,1) = HYBocp_re(:,1)-HYBotune'+actualPars.HYBdLo;
HYBocp_im(:,1) = HYBocp_im(:,1)-HYBotune'+actualPars.HYBdLo;

% Scale z-axis of TW part
% FIXME: the 1 + ...
HYBtwre(:,1)    = (actualPars.HYBTWscale+1.0)*HYBtwre(:,1);
HBtwim(:,1)     = (actualPars.HYBTWscale+1.0)*HBtwim(:,1);

HYBstw  = size(HYBtwre);
HYBdz   = HYBtwre(2,1) - HYBtwre(1,1);
HYBz    = HYBtwre(HYBstw(1),1) - HYBtwre(1,1) + HYBdz;

for ii = 1:actualPars.HYBntw
    HYBdphasor = (ii-1)*HYBphase_adv*2*pi/360;
    HYBEzre((1+(ii-1)*HYBstw(1)):(ii*HYBstw(1)),1) = HYBtwre(:,1)+(ii-1)*HYBz;
    HYBEzim((1+(ii-1)*HYBstw(1)):(ii*HYBstw(1)),1) = HBtwim(:,1)+(ii-1)*HYBz;
    HYBEzre((1+(ii-1)*HYBstw(1)):(ii*HYBstw(1)),2) = real(exp(1i*(-HYBphasor0-HYBdphasor))*(HYBtwre(:,2)+1i*HBtwim(:,2)));
    HYBEzim((1+(ii-1)*HYBstw(1)):(ii*HYBstw(1)),2) = imag(exp(1i*(-HYBphasor0-HYBdphasor))*(HYBtwre(:,2)+1i*HBtwim(:,2)));
end

HYBsicp         = size(HYBicp_re);
HYBsEzre        = size(HYBEzre);
HYBsocp         = size(HYBocp_re);

HYBmax_z_ipc    = max(HYBicp_re(:,1));
HYBmin_z_Ezre   = min(HYBEzre(:,1));
HYBmax_z_Ezre   = max(HYBEzre(:,1));
HYBmin_z_ocp    = min(HYBocp_re(:,1));

HYBicp_end_am   = abs(HYBicp_re(HYBsicp(1),2)+1i*HYBicp_im(HYBsicp(1),2));
HYBicp_end_ph   = phase(HYBicp_re(HYBsicp(1),2)+1i*HYBicp_im(HYBsicp(1),2));
HYBtwre_start   = HYBEzre(1,2);
HYBtwre_end     = HYBEzre(HYBsEzre(1),2);
HYBocp_start_am = abs(HYBocp_re(1,2)+1i*HYBocp_im(1,2));
HYBocp_start_ph = phase(HYBocp_re(1,2)+1i*HYBocp_im(1,2));

HYBcombined = zeros(HYBsicp(1)+HYBsEzre(1)+HYBsocp(1)-2,3);

HYBcombined(1:HYBsicp(1),1) = HYBicp_re(1:HYBsicp(1),1)+HYBmin_z_Ezre-HYBmax_z_ipc;
HYBcombined(HYBsicp(1):HYBsicp(1)+HYBsEzre(1)-1,1) = HYBEzre(1:HYBsEzre(1),1);
HYBcombined(HYBsicp(1)+HYBsEzre(1)-1:HYBsicp(1)+HYBsEzre(1)+HYBsocp(1)-2,1) = HYBmax_z_Ezre+HYBocp_re(1:HYBsocp(1),1)-HYBmin_z_ocp;

HYBcombined(1:HYBsicp(1),2) = ...
    real(exp(-1i*HYBicp_end_ph)*(HYBicp_re(1:HYBsicp(1),2)+1i*HYBicp_im(1:HYBsicp(1),2)))*HYBtwre_start/HYBicp_end_am;
HYBcombined(HYBsicp(1):HYBsicp(1)+HYBsEzre(1)-1,2) = HYBEzre(1:HYBsEzre(1),2);
HYBcombined(HYBsicp(1)+HYBsEzre(1)-1:HYBsicp(1)+HYBsEzre(1)+HYBsocp(1)-2,2) = ...
    real(exp(-1i*HYBocp_start_ph)*(HYBocp_re(1:HYBsocp(1),2)+1i*HYBocp_im(1:HYBsocp(1),2)))*HYBtwre_end/HYBocp_start_am;

HYBcombined(1:HYBsicp(1),3) = ...
    imag(exp(-1i*HYBicp_end_ph)*(HYBicp_re(1:HYBsicp(1),2)+1i*HYBicp_im(1:HYBsicp(1),2)))*HYBtwre_start/HYBicp_end_am;
HYBcombined(HYBsicp(1):HYBsicp(1)+HYBsEzre(1)-1,3) = HYBEzim(1:HYBsEzre(1),2);
HYBcombined(HYBsicp(1)+HYBsEzre(1)-1:HYBsicp(1)+HYBsEzre(1)+HYBsocp(1)-2,3) = ...
    imag(exp(-1i*HYBocp_start_ph)*(HYBocp_re(1:HYBsocp(1),2)+1i*HYBocp_im(1:HYBsocp(1),2)))*HYBtwre_end/HYBocp_start_am;

HYBcombined(:,1) = HYBcombined(:,1) - min(HYBcombined(:,1));

% Rotate phase to pure real at cathode:
HYBcomb_st_ph = phase(HYBcombined(1,2)+1i*HYBcombined(1,3));
HYBcombre = real(exp(-1i*HYBcomb_st_ph)*(HYBcombined(:,2)+1i*HYBcombined(:,3)));
HYBcombim = imag(exp(-1i*HYBcomb_st_ph)*(HYBcombined(:,2)+1i*HYBcombined(:,3)));

HYBcombined(:,2) = HYBcombre;
HYBcombined(:,3) = HYBcombim;

HYBcombinedRe(:,1) = HYBcombined(:,1);
HYBcombinedRe(:,2) = HYBcombined(:,2); %#ok<NASGU>
HYBcombinedIm(:,1) = HYBcombined(2:HYBsicp(1)+HYBsEzre(1)+HYBsocp(1)-2,1);
HYBcombinedIm(:,2) = HYBcombined(2:HYBsicp(1)+HYBsEzre(1)+HYBsocp(1)-2,3); %#ok<NASGU>

% Save fieldmap for Astra simulation
actualPars.gunFmRe = ['hybrid_' actualPars.simFilebase '_re.astra'];
actualPars.gunFmIm = ['hybrid_' actualPars.simFilebase '_im.astra'];
save(fullfile('fieldmaps', actualPars.gunFmRe),'HYBcombinedRe','-ascii');
save(fullfile('fieldmaps', actualPars.gunFmIm),'HYBcombinedIm','-ascii');

% Normalization factors for the fieldmaps
HYBmaxAmplHybrid = max(abs(HYBcombined(:,2)+1i*HYBcombined(:,3)));
actualPars.gunFmReNorm  = max(HYBcombined(:,2))/HYBmaxAmplHybrid;
actualPars.gunFmImNorm  = max(HYBcombined(:,3))/HYBmaxAmplHybrid;


%% Run ASTRA simulation
actualPars = procedure_astra(actualPars);


%% Manage output
switch nargout
    case 0
        runParam.list(runParam.active.simInd) = actualPars;
    case 1
        varargout{1} = actualPars;
    otherwise
        error('Unknown sequence of output parameters');
end


end
