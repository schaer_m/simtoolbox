%%%%% FUNCTION procedure_tuneTwGun_3.m %%%%%
% Mattia Schaer
% August 2015
%
% Tune first half-cell length, TW cell number, output cell length and and damping
% before running ASTRA simulation with the corresponding fieldmaps.
%
% CALL [modifiedSimulationParameters] = procedure_tuneTwGun_3([actualSimulationParameters])
%           actualSimulationParameters              struct
%
%           modifiedSimulationParameters            struct
%
%%%%%



function varargout = procedure_tuneTwGun_3(varargin)

global runParam;


%% Manage input
switch nargin
    case 0
        actualPars      = runParam.list(runParam.active.simInd);
        restartingSim   = runParam.simProgress.started(runParam.active.simInd); %#ok<NASGU>
    case 1
        actualPars      = varargin{1};
        restartingSim   = false; %#ok<NASGU>
    otherwise
        error('Unknown sequence of input parameters');
end


%% Load HFSS fieldmaps

% Input coupler
icp_re = load(fullfile(runParam.globalFieldmapFolder, [actualPars.inputCouplerFm '_re.txt']), '-ascii');
icp_im = load(fullfile(runParam.globalFieldmapFolder, [actualPars.inputCouplerFm '_im.txt']), '-ascii');
icp_zStep = icp_re(2,1) - icp_re(1,1);
% resample based on the given input for the end of the input coupling cell
if actualPars.HYBLi > icp_re(end,1)
    error('Fieldmap of input coupling cell ends before the declared end position (HYBLi).');
end
icp_z_res = linspace(0, actualPars.HYBLi, round(actualPars.HYBLi/icp_zStep))';
icp_complex = complex( interp1(icp_re(:,1), icp_re(:,2), icp_z_res), ...
                       interp1(icp_im(:,1), icp_im(:,2), icp_z_res) );

% Regular cell
tw_re = load(fullfile(runParam.globalFieldmapFolder, [actualPars.normalCellsFm '_re.txt']), '-ascii');
tw_im = load(fullfile(runParam.globalFieldmapFolder, [actualPars.normalCellsFm '_im.txt']), '-ascii');
% no resampling for the regular cell, the field map must already have the
% correct extension. Just make sure that it starts from z=0
tw_z = tw_re(:,1);
if tw_z(1) ~= 0
    warning('Fieldmap of regular cell does not start from z=0.');
    tw_z = tw_z - tw_z(1);
end
tw_complex = complex( tw_re(:,2), tw_im(:,2) );
tw_phasorStart = angle(tw_complex(1));
tw_phasorEnd = angle(tw_complex(end));
%
% check phase advance and direction of the wave
phaseAdvance = 360 * actualPars.HYBphaseAdvance;   % [deg]
phaseAdvance_calc = tw_phasorStart - tw_phasorEnd;
if phaseAdvance_calc < 0
    phaseAdvance_calc = phaseAdvance_calc + 2*pi;
end
phaseAdvance_calc = phaseAdvance_calc /pi*180;   % [deg]
if abs(phaseAdvance - phaseAdvance_calc) > 0.1
    warning('Difference between phase advance given as input (%f deg) and that calculated from the fieldmap (%f deg) is large: %f deg\n', ...
            phaseAdvance, phaseAdvance_calc, abs(phaseAdvance - phaseAdvance_calc));
end
if abs(phaseAdvance_calc + phaseAdvance - 360) < 0.1
    warning('Given and calculated phase advance seems to be complementary --> mirroring field map');
    % The wave from HFSS is travelling in the negative z-direction
    %(with the definition of time direction as below, exp(-1i*omega*t) )
    % Therefore invert it to get a correct fieldmap
    tw_complex = tw_complex(end:-1:1);
    phasorTmp = tw_phasorStart;
    tw_phasorStart = tw_phasorEnd;
    tw_phasorEnd = phasorTmp;
end
% Debug
% figure(1);
% plot(phase(tw_re(:,2)+1i*tw_im(:,2))/pi*180);
% phasor0/pi*180
% phasorEnd/pi*180
%
% damping due to the cell
voltageAttenuation = 10 ^ (-actualPars.HYBregularCellDamping/20.0);
tw_damping = linspace(1, voltageAttenuation, numel(tw_z))';
% normalize field of regular cell to 1 at z=0 and already apply damping
%max(abs(tw_complex))/abs(tw_complex(1))   % Debug
tw_complex = tw_complex/abs(tw_complex(1)) .* tw_damping;
%max(abs(tw_complex))/abs(tw_complex(1))   % Debug

% Output coupler
ocp_re = load(fullfile(runParam.globalFieldmapFolder, [actualPars.outputCouplerFm '_re.txt']), '-ascii');
ocp_im = load(fullfile(runParam.globalFieldmapFolder, [actualPars.outputCouplerFm '_im.txt']), '-ascii');
ocp_zStep = ocp_re(2,1) - ocp_re(1,1);
% resample based on the given input for the starting position of the output
% coupling cell
if actualPars.HYBLo < ocp_re(1,1)
    error('Fieldmap of output coupling cell starts after the declared starting position (HYBLo).');
end
ocp_z_res = linspace(actualPars.HYBLo, ocp_re(end,1), round((ocp_re(end,1)-actualPars.HYBLo)/ocp_zStep))';
ocp_complex = complex( interp1(ocp_re(:,1), ocp_re(:,2), ocp_z_res), ...
                       interp1(ocp_im(:,1), ocp_im(:,2), ocp_z_res) );
ocp_phasorStart = angle(ocp_complex(1));
% normalize field of output coupling cell to 1 at z=0
ocp_complex = ocp_complex/abs(ocp_complex(1));
% shift back points to start from z=0
ocp_z_res = ocp_z_res - actualPars.HYBLo;
% Debug
% figure(1);
% plot(ocp_z_res, abs(ocp_complex));
% xlabel('ocp_z_res [m]', 'Interpreter','none');
% ylabel('abs(ocp_complex)', 'Interpreter','none');


%% Stretch fieldmaps of individual parts

% Input coupling cell
if actualPars.HYBdLi ~= 0
    fprintf('\tStretching the input coupling cell by %f%%\n', actualPars.HYBdLi/actualPars.HYBLi*100);
    % use a cosine in order to not modify the derivative at the junction point
    icp_z_str = icp_z_res + cos(pi/2*(actualPars.HYBLi-icp_z_res)/actualPars.HYBLi) * actualPars.HYBdLi;
else
    % do not modify
    icp_z_str = icp_z_res;
end
% Debug
% figure(1);
% plot(icp_re(1:find(icp_re(:,1)>actualPars.HYBLi,1,'first'),1));
% hold all;
% plot(icp_z_res);
% plot(icp_z_str);
% hold off;
% legend('original', 'resampled', 'stretched');

% Output coupling cell
if actualPars.HYBdLo ~= 0
    fprintf('Stretching the output coupling cell by %f%%\n', actualPars.HYBdLi/actualPars.HYBLi*100);
    % use a cosine in order to not modify the derivative at the junction point
    ocp_z_str = ocp_z_res + (1-cos(pi/2*(ocp_z_res)/ocp_z_res(end))) * actualPars.HYBdLo;
else
    % do not modify
    ocp_z_str = ocp_z_res;
end
% Debug
% figure(1);
% plot(ocp_re(find(ocp_re(:,1)<actualPars.HYBLo,1,'last'):end,1)-actualPars.HYBLo);
% hold all;
% plot(ocp_z_res);
% plot(ocp_z_str);
% hold off;
% legend('original', 'resampled', 'stretched');

% Regular cell
if actualPars.HYBTWscale ~= 1
    fprintf('Stretching the regular cell by %f%%\n', (actualPars.HYBTWscale-1)*100);
    % use a cosine in order to not modify the derivative at the junction
    % points
    tw_z_str = tw_z + (cos(pi*tw_z/tw_z(end))-1)/2 * tw_z(end)*(1-actualPars.HYBTWscale);
else
    % do not modify
    tw_z_str = tw_z;
end
% Debug
% figure(1);
% plot(tw_z);
% hold all;
% plot(tw_z_str);
% hold off;
% legend('original', 'stretched');


%% Put pieces together now

% Input coupling cell
full_z = icp_z_str;
% set z(1) = 0 exactly
full_z(1) = 0;
full_complex = icp_complex;

% TW cells
for ii = 1:actualPars.HYBntw
   zStart = full_z(end);
   amplitudeStart = abs(full_complex(end));
   phasorStart = angle(full_complex(end));
   % append z values
   full_z = [full_z ; tw_z_str(2:end)+zStart];
   % append Ez values
   full_complex = [full_complex ; amplitudeStart*exp(1i*(phasorStart-tw_phasorStart))*tw_complex(2:end)];
end

% Output coupling cell
zStart = full_z(end);
amplitudeStart = abs(full_complex(end));
phasorStart = angle(full_complex(end));
% append z values
full_z = [full_z ; ocp_z_str(2:end)+zStart];
% append Ez values
full_complex = [full_complex ; amplitudeStart*exp(1i*(phasorStart-ocp_phasorStart))*ocp_complex(2:end)];


%% Operations to continue and launch the simulation

% Store some z-locations
regularCellLengthAfterStretch = tw_z_str(end,1) - tw_z_str(1,1);
actualPars.z_firstRegularCellMiddle = icp_z_str(end) + regularCellLengthAfterStretch/2;
actualPars.z_firstRegularCellEnd = icp_z_str(end) + regularCellLengthAfterStretch;
actualPars.z_lastRegularCellMiddle = icp_z_str(end) + (actualPars.HYBntw-0.5)*regularCellLengthAfterStretch;
actualPars.z_lastRegularCellEnd = icp_z_str(end) + actualPars.HYBntw*regularCellLengthAfterStretch;

% Rotate phase to pure real at cathode
full_complex = full_complex * exp(-1i*angle(full_complex(1)));
% Debug
% figure(1);
% plot(full_z, abs(full_complex));

% Save fieldmap for Astra simulation
actualPars.gunFmFolder = runParam.localFieldmapFolder;
actualPars.gunFmRe = [actualPars.gunFmPrefix actualPars.simFilebase '_re.astra'];
actualPars.gunFmIm = [actualPars.gunFmPrefix actualPars.simFilebase '_im.astra'];
tmpFieldmap = [full_z real(full_complex)]; %#ok<NASGU>
save(fullfile(runParam.localFieldmapFolder, actualPars.gunFmRe),'tmpFieldmap','-ascii');
tmpFieldmap = [full_z imag(full_complex)]; %#ok<NASGU>
save(fullfile(runParam.localFieldmapFolder, actualPars.gunFmIm),'tmpFieldmap','-ascii');

% Normalization factors for the fieldmaps
actualPars.gunFmReNorm  = max(abs(real(full_complex))) / max(abs(full_complex));
actualPars.gunFmImNorm  = max(abs(imag(full_complex))) / max(abs(full_complex));


%% Run ASTRA simulation
actualPars = procedure_astra(actualPars);


%% Manage output
switch nargout
    case 0
        runParam.list(runParam.active.simInd) = actualPars;
    case 1
        varargout{1} = actualPars;
    otherwise
        error('Unknown sequence of output parameters');
end


end
