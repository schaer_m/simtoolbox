%%%%% FUNCTION procedure_astra.m %%%%%
% Mattia Schaer
% March 2014
%
% Run standard ASTRA simulation.
%
% CALL [modifiedSimulationParameters] = procedure_astra([actualSimulationParameters])
%           actualSimulationParameters              struct
%
%           modifiedSimulationParameters            struct
%
%%%%%



function varargout = procedure_astra(varargin)

global runParam;


% Manage input
switch nargin
    case 0
        actualPars      = runParam.list(runParam.active.simInd);
        restartingSim   = runParam.simProgress.started(runParam.active.simInd); %#ok<NASGU>
    case 1
        actualPars      = varargin{1};
        restartingSim   = false; %#ok<NASGU>
    otherwise
        error('Unknown sequence of input parameters');
end

% Default fieldmap folders
if isempty(actualPars.gunFmFolder)
    actualPars.gunFmFolder = runParam.globalFieldmapFolder;
end
if isempty(actualPars.gunSolFmFolder)
    actualPars.gunSolFmFolder = runParam.globalFieldmapFolder;
end

% Create solenoid fieldmap
if actualPars.generateGunSolFm
    actualPars.gunSolFmFolder = runParam.localFieldmapFolder;
    actualPars.gunSolFm = [actualPars.gunSolFmPrefix actualPars.simFilebase '.astra'];
    generate_gunSolFm(actualPars.gunSolFmFWHM, actualPars.gunSolFmSlope, ...
                      fullfile(runParam.localFieldmapFolder, actualPars.gunSolFm));
end

% Run generator
if ~isfield(actualPars, 'gunOutDistribution')
    fprintf('... generator\n');
    write_generator_inputfile(actualPars);
    exeString = ['system(''' runParam.genExepath ' ' actualPars.simFilebase '_gen.in'');'];
    evalc(exeString);
end

fprintf('... astra\n');
% Generate ASTRA input file (geometry_name() functions)
% WARNING: with bypasses, the geometry function might change the actual parameters, therefore return them
eval(['actualPars = geometry_' runParam.geometryFunction '(actualPars, restartingSim);']);

% Store names of some ASTRA output files
zstopString = sprintf('%04d', round(str2double(sprintf('%.3f',actualPars.zstop))*100));
actualPars.finalDistrFilename   = [actualPars.simFilebase '.' zstopString '.' sprintf('%03d',actualPars.astraId)];
actualPars.xLineFilename        = [actualPars.simFilebase '.Xemit.' sprintf('%03d',actualPars.astraId)];
actualPars.yLineFilename        = [actualPars.simFilebase '.Yemit.' sprintf('%03d',actualPars.astraId)];
actualPars.zLineFilename        = [actualPars.simFilebase '.Zemit.' sprintf('%03d',actualPars.astraId)];
actualPars.refLineFilename      = [actualPars.simFilebase '.ref.' sprintf('%03d',actualPars.astraId)];

% Run tracker
switch runParam.machineName
    
    case 'merlin'
        write_sge_inputfile(actualPars);
        exeString = ['qsub -q ' runParam.queue ' ' runParam.sgeFilename];
        [~, ~] = system(exeString);
        
    case {'mpc1238', 'localLinux'}
        if runParam.runParallel
            exeString = ['system(''mpirun -np ' num2str(actualPars.Nnodes) ' ' runParam.astraParallelExepath ' ' actualPars.simFilebase '.in > ' ...
                actualPars.simFilebase '.o 2> ' actualPars.simFilebase '.e'');'];
        else
            exeString = ['system(''' runParam.astraExepath ' ' actualPars.simFilebase '.in > ' ...
                actualPars.simFilebase '.o 2> ' actualPars.simFilebase '.e'');'];
        end            
        evalc(exeString);
        
    case 'localWin'
        exeString = ['system(''' runParam.astraExepath ' ' actualPars.simFilebase '.in'');'];
        evalc(exeString);
        
    otherwise
        error('Unknown machineName');
        
end

% Manage output
switch nargout
    case 0
        runParam.list(runParam.active.simInd) = actualPars;
    case 1
        varargout{1} = actualPars;
    otherwise
        error('Unknown sequence of output parameters');
end

end
