%%%%% FUNCTION procedure_tuneTwGun_2.m %%%%%
% Mattia Schaer
% July 2014
%
% Tune first half-cell length, TW cell number, output cell length and and damping
% before running ASTRA simulation with the corresponding fieldmaps.
%
% CALL [modifiedSimulationParameters] = procedure_tuneTwGun_2([actualSimulationParameters])
%           actualSimulationParameters              struct
%
%           modifiedSimulationParameters            struct
%
%%%%%



function varargout = procedure_tuneTwGun_2(varargin)

global runParam;


%% Manage input
switch nargin
    case 0
        actualPars      = runParam.list(runParam.active.simInd);
        restartingSim   = runParam.simProgress.started(runParam.active.simInd); %#ok<NASGU>
    case 1
        actualPars      = varargin{1};
        restartingSim   = false; %#ok<NASGU>
    otherwise
        error('Unknown sequence of input parameters');
end


%% Tune TW Gun

% Load HFSS fieldmaps and convert position on axis from mm to m

% Input coupler
% icp_re = load(fullfile(runParam.globalFieldmapFolder, [actualPars.inputCouplerFm '_re.txt']));
% icp_re(:,1)=icp_re(:,1)/1000.0;
%save('Ez_first_cellA_re.txt','icp_re','-ascii');
icp_re = load(fullfile(runParam.globalFieldmapFolder, [actualPars.inputCouplerFm '_re.txt']), '-ascii');
% icp_im = load(fullfile(runParam.globalFieldmapFolder, [actualPars.inputCouplerFm '_im.txt']));
% icp_im(:,1)=icp_im(:,1)/1000.0;
% icp_im(:,2)=-icp_im(:,2);
%save('Ez_first_cellA_im.txt','icp_im','-ascii');
icp_im = load(fullfile(runParam.globalFieldmapFolder, [actualPars.inputCouplerFm '_im.txt']), '-ascii');
% Select only points belonging to the input coupler (cut the end points)
%icpEndInd = find(min(abs(icp_re(:,1)-actualPars.HYBLi))==abs(icp_re(:,1)-actualPars.HYBLi));
[~, icpEndInd] = min(abs(icp_re(:,1)-actualPars.HYBLi));
icp_re = icp_re(1:icpEndInd, :);
icp_im = icp_im(1:icpEndInd, :);

% Regular cell
% twre = load(fullfile(runParam.globalFieldmapFolder, [actualPars.normalCellsFm '_re.txt']));
% twre(:,1)=twre(:,1)/1000.0;
%save('Ez_regular_cellA_re.txt','twre','-ascii');
tw_re = load(fullfile(runParam.globalFieldmapFolder, [actualPars.normalCellsFm '_re.txt']), '-ascii');
% twim = load(fullfile(runParam.globalFieldmapFolder, [actualPars.normalCellsFm '_im.txt']));
% twim(:,1)=twim(:,1)/1000.0;
% twim(:,2)=-twim(:,2);
%save('Ez_regular_cellA_im.txt','twim','-ascii');
tw_im = load(fullfile(runParam.globalFieldmapFolder, [actualPars.normalCellsFm '_im.txt']), '-ascii');
% Nothing to select for the regular cell, the field map must have the
% correct extension

phaseAdvance=360*actualPars.HYBphaseAdvance;
phasor0=phase(tw_re(1,2)+1i*tw_im(1,2));
if phasor0 < 0
    phasor0 = phasor0 + 2*pi;
end
phasorEnd=phase(tw_re(end,2)+1i*tw_im(end,2));
if phasorEnd < 0
    phasorEnd = phasorEnd + 2*pi;
end
if phasorEnd > phasor0
    % The wave from HFSS is travelling in the negative z-direction
    %(with the definition of time direction as below, exp(-1i*omega*t) )
    % Therefore invert it to get a correct fieldmap
    tw_re(:,2) = tw_re(end:-1:1,2);
    tw_im(:,2) = tw_im(end:-1:1,2);
    phasorTmp = phasor0;
    phasor0 = phasorEnd;
    phasorEnd = phasorTmp;
end
phase_adv_calc=abs(phasorEnd-phasor0)/pi*180;
% figure(1);
% plot(phase(tw_re(:,2)+1i*tw_im(:,2))/pi*180);
% phasor0/pi*180
% phasorEnd/pi*180
if abs(phaseAdvance - phase_adv_calc) > 1.0
    warning('Difference between phase advance given as input (%f deg) and that calculated from the fieldmap (%f deg): %f deg\n', ...
            phaseAdvance, phase_adv_calc, abs(phaseAdvance - phase_adv_calc));
end
DampRCFac=10^(-actualPars.HYBregularCellDamping/20.0);

% Output coupler
% R_I_ocp = load(fullfile(runParam.globalFieldmapFolder, [actualPars.outputCouplerFm '.txt']));
% S_ocp=size(R_I_ocp);
% ocp_re(:,1)=R_I_ocp(actualPars.HYBocpStart:S_ocp(1),1)/1000.0;
% ocp_re(:,2)=R_I_ocp(actualPars.HYBocpStart:S_ocp(1),2);
%save('Ez_last_cellA_re.txt','ocp_re','-ascii');
ocp_re = load(fullfile(runParam.globalFieldmapFolder, [actualPars.outputCouplerFm '_re.txt']), '-ascii');
% ocp_im(:,1)=R_I_ocp(actualPars.HYBocpStart:S_ocp(1),1)/1000.0;
% ocp_im(:,2)=-R_I_ocp(actualPars.HYBocpStart:S_ocp(1),3);
%save('Ez_last_cellA_im.txt','ocp_im','-ascii');
ocp_im = load(fullfile(runParam.globalFieldmapFolder, [actualPars.outputCouplerFm '_im.txt']), '-ascii');
% Select only points belonging to the input coupler (cut the start points)
%ocpStartInd = find(min(abs(ocp_re(:,1)-actualPars.HYBLo))==abs(ocp_re(:,1)-actualPars.HYBLo));
[~, ocpStartInd] = min(abs(ocp_re(:,1)-actualPars.HYBLo));
ocp_re = ocp_re(ocpStartInd:end, :);
ocp_im = ocp_im(ocpStartInd:end, :);
% shift back points to start from z=0
ocp_re(:,1) = ocp_re(:,1) - actualPars.HYBLo;
ocp_im(:,1) = ocp_im(:,1) - actualPars.HYBLo;

% Scale z-axis of input SW part
icpEndZ = icp_re(end,1);
itune = actualPars.HYBdLi/2 * (1+cos(pi*(icp_re(:,1)-icp_re(1,1))/(icpEndZ-icp_re(1,1))));
icp_re(:,1) = icp_re(:,1)-itune+actualPars.HYBdLi;
icp_im(:,1) = icp_im(:,1)-itune+actualPars.HYBdLi;
icpEndZAfterStretch = icp_re(end,1);

% Scale z-axis of output SW part
ocpEndZ = ocp_re(end,1);
otune = actualPars.HYBdLo/2 * (1+cos(pi*(ocp_re(:,1)-ocp_re(1,1))/(ocpEndZ-ocp_re(1,1))));
ocp_re(:,1)=ocp_re(:,1)-otune+actualPars.HYBdLo;
ocp_im(:,1)=ocp_im(:,1)-otune+actualPars.HYBdLo;

% Scale z-axis of TW part
tw_re(:,1)=actualPars.HYBTWscale*tw_re(:,1);
tw_im(:,1)=actualPars.HYBTWscale*tw_im(:,1);
tw_s=size(tw_re);
dz=tw_re(2,1) - tw_re(1,1);
z=tw_re(tw_s(1),1)-tw_re(1,1)+dz;
stwscaling=linspace(1,DampRCFac,tw_s(1))';

for ii = 1:actualPars.HYBntw
   dphasor=(ii-1)*phaseAdvance*2*pi/360;
   %dphasor=(ii-1)*phase_adv_calc*2*pi/360;
   % z values
   Ez_re((1+(ii-1)*tw_s(1)):(ii*tw_s(1)),1)=tw_re(:,1)+(ii-1)*z;
   Ez_im((1+(ii-1)*tw_s(1)):(ii*tw_s(1)),1)=tw_im(:,1)+(ii-1)*z;
   % Ez values
   Ez_re((1+(ii-1)*tw_s(1)):(ii*tw_s(1)),2)=DampRCFac^(ii-1)*stwscaling.*real(exp(1i*(-phasor0-dphasor))*(tw_re(:,2)+1i*tw_im(:,2)));
   Ez_im((1+(ii-1)*tw_s(1)):(ii*tw_s(1)),2)=DampRCFac^(ii-1)*stwscaling.*imag(exp(1i*(-phasor0-dphasor))*(tw_re(:,2)+1i*tw_im(:,2)));
end

icp_s = size(icp_re, 1);
Ez_re_s = size(Ez_re);
ocp_s = size(ocp_re, 1);

Ez_re_min_z=min(Ez_re(:,1));
Ez_re_max_z=max(Ez_re(:,1));

% Store some z-locations
regularCellLengthAfterStretch = tw_re(end,1) - tw_re(1,1);
actualPars.z_firstRegularCellMiddle = icpEndZAfterStretch + regularCellLengthAfterStretch/2;
actualPars.z_firstRegularCellEnd = icpEndZAfterStretch + regularCellLengthAfterStretch;
actualPars.z_lastRegularCellMiddle = icpEndZAfterStretch + (actualPars.HYBntw-0.5)*regularCellLengthAfterStretch;
actualPars.z_lastRegularCellEnd = icpEndZAfterStretch + actualPars.HYBntw*regularCellLengthAfterStretch;

%icp_end_am=abs(icp_re(icp_s(1),2)+1i*icp_im(icp_s(1),2));
%icp_end_ph=phase(icp_re(icp_s(1),2)+1i*icp_im(icp_s(1),2));
% From here on, substituted: icp_s(1) with HYBzSWscaleEndInd
icp_end_am=abs(icp_re(end,2)+1i*icp_im(end,2));
icp_end_ph=phase(icp_re(end,2)+1i*icp_im(end,2));
tw_re_start=abs(Ez_re(1,2)+1i*Ez_im(1,2));
tw_re_end=abs(Ez_re(Ez_re_s(1),2)+1i*Ez_im(Ez_re_s(1),2));
ocp_start_am=abs(ocp_re(1,2)+1i*ocp_im(1,2));
ocp_start_ph=phase(ocp_re(1,2)+1i*ocp_im(1,2));

phaseEndTW=phase(Ez_re(Ez_re_s(1),2)+1i*Ez_im(Ez_re_s(1),2));

% Put everything together
combined = zeros(icp_s+Ez_re_s(1)+ocp_s-2,3);

combined(1:icp_s,1)=icp_re(:,1)+Ez_re_min_z-icpEndZAfterStretch;
combined(icp_s:icp_s+Ez_re_s(1)-1,1)=Ez_re(1:Ez_re_s(1),1);
combined(icp_s+Ez_re_s(1)-1:icp_s+Ez_re_s(1)+ocp_s-2,1)=Ez_re_max_z+ocp_re(1:ocp_s,1)-ocp_re(1,1);
combined(:,1)=combined(:,1)-min(combined(:,1));

combined(1:icp_s,2)=real(exp(-1i*icp_end_ph)*(icp_re(:,2)+1i*icp_im(:,2)))*tw_re_start/icp_end_am;
combined(icp_s:icp_s+Ez_re_s(1)-1,2)=Ez_re(1:Ez_re_s(1),2);
combined(icp_s+Ez_re_s(1)-1:icp_s+Ez_re_s(1)+ocp_s-2,2)=real(exp(-1i*(ocp_start_ph-phaseEndTW))*(ocp_re(1:ocp_s,2)+1i*ocp_im(1:ocp_s,2)))*tw_re_end/ocp_start_am;

combined(1:icp_s,3)=imag(exp(-1i*icp_end_ph)*(icp_re(:,2)+1i*icp_im(:,2)))*tw_re_start/icp_end_am;
combined(icp_s:icp_s+Ez_re_s(1)-1,3)=Ez_im(1:Ez_re_s(1),2);
combined(icp_s+Ez_re_s(1)-1:icp_s+Ez_re_s(1)+ocp_s-2,3)=imag(exp(-1i*(ocp_start_ph-phaseEndTW))*(ocp_re(1:ocp_s,2)+1i*ocp_im(1:ocp_s,2)))*tw_re_end/ocp_start_am;

% Rotate phase to pure real at cathode:
comb_st_ph=phase(combined(1,2)+1i*combined(1,3));
combre=real(exp(-1i*comb_st_ph)*(combined(:,2)+1i*combined(:,3)));
combim=imag(exp(-1i*comb_st_ph)*(combined(:,2)+1i*combined(:,3)));

combined(:,2)=combre;
combined(:,3)=combim;

combinedRe(:,1)=combined(:,1);
combinedRe(:,2)=combined(:,2); %#ok<NASGU>
combinedIm(:,1)=combined(:,1);
combinedIm(:,2)=combined(:,3); %#ok<NASGU>
% combinedIm(:,1)=combined(2:icp_s+Ez_re_s(1)+ocp_s-2,1);
% combinedIm(:,2)=combined(2:icp_s+Ez_re_s(1)+ocp_s-2,3); %#ok<NASGU>

% Save fieldmap for Astra simulation
actualPars.gunFmFolder = runParam.localFieldmapFolder;
actualPars.gunFmRe = [actualPars.gunFmPrefix actualPars.simFilebase '_re.astra'];
actualPars.gunFmIm = [actualPars.gunFmPrefix actualPars.simFilebase '_im.astra'];
save(fullfile(runParam.localFieldmapFolder, actualPars.gunFmRe),'combinedRe','-ascii');
save(fullfile(runParam.localFieldmapFolder, actualPars.gunFmIm),'combinedIm','-ascii');

% Normalization factors for the fieldmaps
maxAmplHybrid=max(abs(combined(:,2)+1i*combined(:,3)));
actualPars.gunFmReNorm  = max(abs(combined(:,2)))/maxAmplHybrid;
actualPars.gunFmImNorm  = max(abs(combined(:,3)))/maxAmplHybrid;


%% Run ASTRA simulation
actualPars = procedure_astra(actualPars);


%% Manage output
switch nargout
    case 0
        runParam.list(runParam.active.simInd) = actualPars;
    case 1
        varargout{1} = actualPars;
    otherwise
        error('Unknown sequence of output parameters');
end


end
