%%%%% FUNCTION procedure_tuneHalfCell.m %%%%%
% Mattia Schaer
% April 2014
%
% Tune first half cell of the gun to a desired length and run ASTRA
% simulation with the corresponding fieldmap.
%
% CALL [modifiedSimulationParameters] = procedure_tuneHalfCell([actualSimulationParameters])
%           actualSimulationParameters              struct
%
%           modifiedSimulationParameters            struct
%
%%%%%



function varargout = procedure_tuneHalfCell(varargin)

global runParam;


%% Manage input
switch nargin
    case 0
        actualPars      = runParam.list(runParam.active.simInd);
        restartingSim   = runParam.simProgress.started(runParam.active.simInd); %#ok<NASGU>
    case 1
        actualPars      = varargin{1};
        restartingSim   = false; %#ok<NASGU>
    otherwise
        error('Unknown sequence of input parameters');
end


%% Tune first half cell
if isfield(actualPars, 'gunHc') && ~isempty(actualPars.gunHc)
    fmFilepath = fullfile(actualPars.fmPath, actualPars.gunFm);
    [~, ~, ext] = fileparts(fmFilepath);
    fmFilebase = actualPars.simFilebase;
    outFmFilepath = fullfile(actualPars.fmPath, ['tuned_' fmFilebase ext]);
    tunedFmFilepath = tuneHalfCell(fmFilepath, actualPars.gunNue*1E9, outFmFilepath, actualPars.gunHc);
    [~, filename, ext] = fileparts(tunedFmFilepath);
    actualPars.gunFm = [filename ext];
end


%% Run ASTRA simulation
actualPars = run_single_astra(actualPars);


%% Manage output
switch nargout
    case 0
        runParam.list(runParam.active.simInd) = actualPars;
    case 1
        varargout{1} = actualPars;
    otherwise
        error('Unknown sequence of output parameters');
end


end
