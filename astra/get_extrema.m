%%%%% FUNCTION get_extrema.m %%%%%
% Mattia Schaer
% March 3014
%
% Find minimum or maximum of a quantity along a 1D profile.
%
% CALL quantitiesAtExtremum = get_extrema(profiles1D, quantityNameToFindMax, extremaType, extremaPos, [plotFoundExtrema], [plotHoldOn])
%           profiles1D:                     cell(M),structs
%           quantityNameToFindMax:          string
%           extremaType:                    string
%                                               'min' OR
%                                               'max'
%           extremaPos:                     int(N)
%                                                1 = first,  2 = second, ...
%                                               -1 = last , -2 = second last, ...
%
%           quantitiesAtExtremum
%
%%%%%



function quantitiesAtExtremum = get_extrema(profiles1D, quantityName, extremaType, extremaPos, varargin)


% Output options
if nargin > 4
    plotFoundExtrema    = varargin{1};
    plotHoldOn          = varargin{2};
else
    plotFoundExtrema    = false;
    plotHoldOn          = false;
end

switch extremaType
    case 'max'
        % Find maxima
        [~, indices] = findpeaks(profiles1D{1}.(quantityName));
    case 'min'
        % Find minima
        [~, indices] = findpeaks(-profiles1D{1}.(quantityName));
end

if isempty(extremaPos)
    % Return all found extrema if no position index was specified
    requestedIndices = indices;
else
    % Return only extrema corresponding to the specified position indices
    requestedIndices = zeros(1, numel(extremaPos));
    % Positive position indices refer to extrema counting "from the beginning"
    % "first, second, third, ..."
    positivePos = extremaPos > 0;
    requestedIndices(positivePos) = indices(extremaPos(positivePos));
    % Negative position indices refer to the extrema counting back "from the end"
    % "last, second last, third last, ..."
    %~positivePos
    %extremaPos
    %indices
    requestedIndices(~positivePos) = indices(end+1+extremaPos(~positivePos));
end

quantitiesAtExtremum = struct;
% Loop over structures in the cell array beamEvolution
for kk = 1:numel(profiles1D)
    profileQuantities = fieldnames(profiles1D{kk});
    % Loop over the requested extrema
    for ii = 1:numel(requestedIndices)
        % Loop over the quantities in the current structure
        for jj = 1:numel(profileQuantities)
            % Do not overreide values with the same name if already present
            if jj == 1 || ...
               ~isfield(quantitiesAtExtremum(ii), profileQuantities{jj}) || ...
               isempty(quantitiesAtExtremum(ii).(profileQuantities{jj}))
                quantitiesAtExtremum(ii).(profileQuantities{jj}) = ...
                    profiles1D{kk}.(profileQuantities{jj})(requestedIndices(ii)); %#ok<AGROW>
            end
        end
    end
end

% Plot requested extrema
if plotFoundExtrema
    hFig = findobj('Type','figure', 'Name','get_extrema');
    if isempty(hFig)
        hFig = figure('Name','get_extrema', 'NumberTitle','off');
        xlabel('Point');
        ylabel(quantityName);
    end
    figure(hFig);
    if plotHoldOn
        hold on;
    else
        hold off;
    end
    plot(profiles1D{1}.(quantityName));
    hold on;
    for ii = 1:numel(requestedIndices)
        plot(requestedIndices(ii), quantitiesAtExtremum(ii).(quantityName), 'ro');
    end
    hold off;
end

end
