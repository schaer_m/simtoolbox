%%%%% FUNCTION write_particleDistribution_darkCurrent.m %%%%%
% Mattia Schaer
% December 2014
%
% Generate distribution file to study dark current trajectories.
%
% CALL write_particleDistribution_darkCurrent(pars)
%           pars:           struct
%%%%%



function [] = write_particleDistribution_darkCurrent(pars)

global constVars;


% Extend particle distribution to include trajectory particles
trajNum = pars.trajRadiusNum * pars.trajPhaseNum * pars.trajInitialEnergyNum * pars.trajInitialAngleNum;
if pars.keepBunch
    originalDistribution = load([pars.simFilebase '_distr.ini']);
    newDistribution = zeros(size(originalDistribution,1) + trajNum, ...
                            size(originalDistribution,2));
    % copy original distribution
    % changing active probe particles to standard particles
    originalDistribution(2:7, 10) = -1;
    newDistribution(1:end-trajNum, :) = originalDistribution;
else
    newDistribution = zeros(1 + trajNum, 10);
    % reference particle
    newDistribution(1, :) = [ ...
        0, ...
        0, ...
        0, ...
        0, ...
        0, ...
        0, ...
        0, ...
        pars.trajParticleQ, ...
        pars.trajParticleIndex, ...
        -1, ...
    ];
end

trajRadius          = linspace(pars.trajRadiusMin, pars.trajRadiusMax, pars.trajRadiusNum);
trajPhase           = linspace(pars.trajPhaseMin, pars.trajPhaseMax, pars.trajPhaseNum);
trajInitialEnergy   = linspace(pars.trajInitialEnergyMin, pars.trajInitialEnergyMax, pars.trajInitialEnergyNum);
trajInitialAngle    = linspace(pars.trajInitialAngleMin, pars.trajInitialAngleMax, pars.trajInitialAngleNum);

% Equation of oblique wall of CTF2 Gun 5
% x = 2.7475*z + 0.01
mCTF2Gun5 = 2.7475;
qCTF2Gun5 = 0.01;   % [m]

for ii = 1:pars.trajRadiusNum
    X = trajRadius(ii);
    for jj = 1:pars.trajPhaseNum
        for kk = 1:pars.trajInitialEnergyNum
            for ll = 1:pars.trajInitialAngleNum
                if ~isempty(strfind(pars.gunFm, '3D_fieldmap_beadpullMatch')) && X > 0.01
                    % oblique wall
                    Z = (X - qCTF2Gun5) / mCTF2Gun5;
                else
                    % normal flat wall
                    Z = 0;
                end

                if pars.trajInitialEnergyNum > 1
                    % To study the influence of initial momentum
                    [~, ~, P] = Ekin_to_p(trajInitialEnergy(kk), constVars.mElectron.nat);
                    Px = P * sind(trajInitialAngle(ll));
                    Pz = P * cosd(trajInitialAngle(ll));
                else
                    Px = 0;
                    Pz = 0;
                end

                %particleIndex = 1 + (ii-1)*pars.trajPhaseNum+jj;
                particleIndex = 1 + sub2ind([pars.trajRadiusNum pars.trajPhaseNum pars.trajInitialEnergyNum pars.trajInitialAngleNum], ii, jj, kk, ll);
                if pars.keepBunch
                    particleIndex = particleIndex + size(originalDistribution,1) - 1;
                end

                newDistribution(particleIndex, :) = [...
                    X, ...
                    0, ...
                    Z, ...
                    Px, ...
                    0, ...
                    Pz, ...
                    trajPhase(jj)/(360*pars.gunNue), ...
                    pars.trajParticleQ, ...
                    pars.trajParticleIndex, ...
                    pars.trajParticleStatus, ...
                ];
            
            end
        end
    end
end

outFile = fopen([pars.simFilebase '_distr.ini'], 'w');
fprintf(outFile, '%19.12E %19.12E %19.12E %19.12E %19.12E %19.12E %19.12E %19.12E %3d %3d\n', newDistribution');
fclose(outFile);


end
