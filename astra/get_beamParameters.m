%%%%% FUNCTION get_beamParameters.m %%%%%
% Original version: Simona Bettoni
% Modified: Mattia Schaer
% April 2014
%
% Calculate slice emittance and mismatch parameter from the ASTRA
% distribution file.
%
% CALL beamParameters = get_beamParameters(distributionFilepath, slicingPars)
%           distributionFilepath            string
%           slicingPars                     struct
%
%           beamParameters                  struct
%
%%%%%



function beamParameters = get_beamParameters(distributionFilepath, slicingPars)

global constVars;


%% Output options
activateDisplay     = false;
plotBunchAverage    = false;
verbose             = false;


%% Read in Astra data
rawDistribution = load(distributionFilepath);

% From the manual: 1st line is the reference particle
% save it
refParticle =  rawDistribution(1,:);
% keep it in the distribution, set longitudinal coordinates to 0
rawDistribution(1,3) = 0.0;
rawDistribution(1,6) = 0.0;
% Find the indices of the particles in the range [-Zrange ; +Zrange]
% the z position (column 3) is relative to that of the reference particle!
ZposInd = abs(rawDistribution(:,3)) < slicingPars.Zrange;
% Find the indices of the particles with non-negative flag (column 10),
% i.e. those which are tracked
flagInd = rawDistribution(:,10) >= 0;
if verbose
    fprintf('Excluding %d active particles over a total of %d\n', sum(flagInd)-sum(ZposInd & flagInd), sum(flagInd));
end
% Filter out undesired particles
filteredDistribution = rawDistribution(ZposInd & flagInd, :);
% Sort particles by z position (column 3)
filteredDistribution = sortrows(filteredDistribution, 3);
% Read out phase space quantities adding offset of reference particle
% leave z relative to reference particle
z       = filteredDistribution(:,3);
% absolute pz
pz_pre  = filteredDistribution(:,6) + refParticle(6);
% total momentum, kinetic energy and corresponding relativistic quantities
p = sqrt(filteredDistribution(:,4).^2 + filteredDistribution(:,5).^2 + pz_pre.^2);   % eV/c
[beta_pre, gamma_pre, Ekin_pre] = p_to_Ekin(p, constVars.mElectron.nat);
% transverse variables relative to those of reference particle
% ASSUMPTION: pz_pre>>px_pre and pz_pre~pz_ref
x_pre   = filteredDistribution(:,1) - refParticle(1);
y_pre   = filteredDistribution(:,2) - refParticle(2);
px_pre  = filteredDistribution(:,4) - refParticle(4);
py_pre  = filteredDistribution(:,5) - refParticle(5);
% these should correspond more or less with the reference particle values
xMean   = mean(filteredDistribution(:,1));
yMean   = mean(filteredDistribution(:,2));
pxMean  = mean(filteredDistribution(:,4));
pyMean  = mean(filteredDistribution(:,5));
if verbose
    fprintf('Reference particle:\t ( %10e m , %10e m , %10e eV/c , %10e eV/c )\n', refParticle([1 2 4 5]));
    fprintf('Mean values:\t\t ( %10e m , %10e m , %10e eV/c , %10e eV/c )\n', xMean, yMean, pxMean, pyMean);
end
% macroparticle charge
charge_pre = filteredDistribution(:,8);
% angles
% ASSUMPTION: pz_pre>>px_pre and pz_pre~pz_ref
xp_pre  = atan(px_pre ./ pz_pre);
yp_pre  = atan(py_pre ./ pz_pre);


%% First: do not slice to compute zero (average) parameters
% emittances
emittance_g_x0      = sqrt( mean(x_pre.^2)*mean(xp_pre.^2) - mean(x_pre.*xp_pre)^2 );   % [m]
% normalize with beta*gamma of the reference particle
emittance_norm_x0   = emittance_g_x0 * beta_pre(1)*gamma_pre(1);   % [m]
emittance_g_y0      = sqrt( mean(y_pre.^2)*mean(yp_pre.^2) - mean(y_pre.*yp_pre)^2 );   % [m]
% normalize with beta*gamma of the reference particle
emittance_norm_y0   = emittance_g_y0 * beta_pre(1)*gamma_pre(1);   % [m]
% Twiss parameters
alpha_x0            = -1 * mean(x_pre.*xp_pre) / emittance_g_x0;   % []
beta_x0             = mean(x_pre.^2) / emittance_g_x0;   % [1/m];
gamma_x0            = (1+alpha_x0^2) / beta_x0;
alpha_y0            = -1 * mean(y_pre.*yp_pre) / emittance_g_y0;   % []
beta_y0             = mean(y_pre.^2) / emittance_g_y0;   % [1/m]
gamma_y0            = (1+alpha_y0^2) / beta_y0;
% Energy spread
dE_rms_norm_0       = std(Ekin_pre) / mean(Ekin_pre);   % [%]
% Total bunch charge
charge_0            = sum(charge_pre);   % [nC]


%% Perform slicing to compute slices parameters
% slices length
deltaZ = (max(z)-min(z)) / slicingPars.Nslice;

for i = 1:slicingPars.Nslice

    if slicingPars.constChargeSlices
        % constant charge slicing
        startInd    = floor( length(z)/(slicingPars.Nslice) ) * (i-1) + 1;
        endInd      = floor( length(z)/(slicingPars.Nslice) ) * i;
        zStart(i)   = z(startInd);
        zEnd(i)     = z(endInd);    
    else
        % constant length slicing
        if i == 1
            zStart(i)   = min(z);
            zEnd(i)     = zStart(i) + deltaZ;
        else
            zStart(i)   = zEnd(i-1);
            zEnd(i)     = zStart(i)+deltaZ;
        end
        startInd    = find(z >= zStart(i), 1, 'First');
        endInd      = find(z <= zEnd(i), 1, 'Last'); 
    end

    % Select only particles in the current slice
    x       = x_pre(startInd:endInd);
    y       = y_pre(startInd:endInd);
    xp      = xp_pre(startInd:endInd);
    yp      = yp_pre(startInd:endInd);
    px      = px_pre(startInd:endInd);
    py      = py_pre(startInd:endInd);
    pz      = pz_pre(startInd:endInd);
    beta    = beta_pre(startInd:endInd);
    gamma   = gamma_pre(startInd:endInd);
    Ekin    = Ekin_pre(startInd:endInd);
    charge  = charge_pre(startInd:endInd);

    % slice emittances
    emittance_g_x(i)    = sqrt( mean(x.^2)*mean(xp.^2) - mean(x.*xp)^2 );   % [m]
    % USE BETA*GAMMA OF REF PART OR SLICE MEAN?
    emittance_norm_x(i) = emittance_g_x(i) * beta_pre(1)*gamma_pre(1);   % [m]
    emittance_g_y(i)    = sqrt( mean(y.^2)*mean(yp.^2) - mean(y.*yp)^2 );   % [m]
    % USE BETA*GAMMA OF REF PART OR SLICE MEAN?
    emittance_norm_y(i) = emittance_g_y(i) * beta_pre(1)*gamma_pre(1);   % [m]
    % slice Twiss parameters
    alpha_x(i)          = -1 * mean(x.*xp) / emittance_g_x(i);
    beta_x(i)           = mean(x.^2) / emittance_g_x(i);
    gamma_x(i)          = (1+alpha_x(i)^2) / beta_x(i);
    alpha_y(i)          = -1 * mean(y.*yp) / emittance_g_y(i);
    beta_y(i)           = mean(y.^2) / emittance_g_y(i);
    gamma_y(i)          = (1+alpha_y(i)^2) / beta_y(i);
    % slice energy spread
    dE_rms_norm(i)      = std(Ekin) / mean(Ekin);   % [%]
    % slice current
    sliceCharge(i)      = sum(charge);   % [nC]
    sliceCurrent(i)     = abs(sliceCharge(i)) / (z(endInd)-z(startInd)) * constVars.c.si * mean(beta) * 1E-9;   % [A]

end


%% Function output
beamParameters.zCenter          = (zStart+zEnd) / 2;
beamParameters.mismatchX        = 1/2 * (beta_x0*gamma_x-2*alpha_x0*alpha_x+gamma_x0*beta_x);
beamParameters.mismatchY        = 1/2 * (beta_y0*gamma_y-2*alpha_y0*alpha_y+gamma_y0*beta_y);
beamParameters.mismatchXcentral = 1/2 * (beta_x(fix(slicingPars.Nslice/2))*gamma_x-2*alpha_x(fix(slicingPars.Nslice/2))*alpha_x+gamma_x(fix(slicingPars.Nslice/2))*beta_x);
beamParameters.mismatchYcentral = 1/2 * (beta_y(fix(slicingPars.Nslice/2))*gamma_y-2*alpha_y(fix(slicingPars.Nslice/2))*alpha_y+gamma_y(fix(slicingPars.Nslice/2))*beta_y);
beamParameters.normSliceEmitX   = emittance_norm_x;
beamParameters.normSliceEmitY   = emittance_norm_y;
beamParameters.normProjEmitX    = emittance_norm_x0;
beamParameters.normProjEmitY    = emittance_norm_y0;
beamParameters.charge           = sliceCharge;
beamParameters.current          = sliceCurrent;


%% Plot results

if activateDisplay
    col1 = [0.25 0.25 rand(1)];
    col2 = [0.25 rand(1) 0.25];
    hFig = findobj('Type','figure', 'Name','get_beamParameters');
    if isempty(hFig)
        hFig = figure('Name','get_beamParameters', 'NumberTitle','off');
    end
    figure(hFig);
    % Mismatch parameter
    subplot(3,1,1);
    [ax, h1, h2] = plotyy(beamParameters.zCenter, beamParameters.mismatchX, ...
                          beamParameters.zCenter, beamParameters.mismatchY);
    set(h1, 'Color',col1);
    set(h2, 'Color',col2);
    hold on;
    % ideally
    if plotBunchAverage
        plot(beamParameters.zCenter([1 end]), [1 1], 'k', 'LineWidth',2);
    end
    title('Mismatch parameter');
    xlabel('z [m]');
    set(get(ax(1),'Ylabel'), 'String','Mismatch_x'); 
    set(get(ax(2),'Ylabel'), 'String','Mismatch_y');
    % Emittances
    subplot(3,1,2);
    % slice
    [ax, h1, h2] = plotyy(beamParameters.zCenter, beamParameters.normSliceEmitX, ...
                        beamParameters.zCenter, beamParameters.normSliceEmitY);
    set(h1, 'Color',col1);
    set(h2, 'Color',col2);
    hold on;
    % projected
    if plotBunchAverage
        [ax, h1, h2] = plotyy(beamParameters.zCenter([1 end]), [beamParameters.normProjEmitX beamParameters.normProjEmitX], ...
                              beamParameters.zCenter([1 end]), [beamParameters.normProjEmitY beamParameters.normProjEmitY]);
        set(h1, 'Color',col1, 'LineWidth',2);
        set(h2, 'Color',col2, 'LineWidth',2);
    end
    title('Normalized slice emittance');
    xlabel('z [m]');
    set(get(ax(1),'Ylabel'), 'String','Epsilon_{x,n} [m]'); 
    set(get(ax(2),'Ylabel'), 'String','Epsilon_{y,n} [m]');
    % Twiss parameters
    subplot(3,1,3);
    % slice
    [ax, h1, h2] = plotyy(beamParameters.zCenter, alpha_x, ...
                        beamParameters.zCenter, beta_x);
    set(h1, 'Color',col1);
    set(h2, 'Color',col2);
    hold on;
    % bunch
    if plotBunchAverage
        [ax, h1, h2] = plotyy(beamParameters.zCenter([1 end]), [alpha_x0 alpha_x0], ...
                              beamParameters.zCenter([1 end]), [beta_x0 beta_x0]);
        set(h1, 'Color',col1, 'LineWidth',2);
        set(h2, 'Color',col2, 'LineWidth',2);
    end
    title('Twiss parameters');
    xlabel('z [m]');
    set(get(ax(1),'Ylabel'), 'String','Alpha_x'); 
    set(get(ax(2),'Ylabel'), 'String','Beta_x');
end


end
