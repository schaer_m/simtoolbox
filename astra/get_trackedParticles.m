%%%%% FUNCTION get_trackedParticles.m %%%%%
% Mattia Schaer
% December 2014
%
% Read in trajectories of tracked particles.
%
% CALL trackedParticles = get_trackedParticles(filepath)
%           filepath                        string
%
%           trackedParticles                float(NtrackedParticles, Nz, 6)
%
%%%%%



function [trackedParticles, rawData] = get_trackedParticles(filepath)


%% Read in Astra data
rawData = load(filepath);


%% Organize data
% Sort entries by particle index
rawData = sortrows(rawData, 1);
% Store data particle by particle
particleStartInd = 1;
particleInd = 1;
while particleStartInd < size(rawData, 1)
    particleEndInd = find(rawData(particleStartInd:end,1) ~= rawData(particleStartInd,1), 1, 'first') ...
                     + particleStartInd - 2;
    if isempty(particleEndInd)
        % we are at the end of the list
        particleEndInd = size(rawData, 1);
    end
    particle = rawData(particleStartInd:particleEndInd, :);
    trackedParticles(particleInd).ind = particle(:, 1);
    trackedParticles(particleInd).z = particle(:, 3);   % [m]
    trackedParticles(particleInd).x = particle(:, 4);   % [mm]
    trackedParticles(particleInd).y = particle(:, 5);   % [mm]
    trackedParticles(particleInd).Ez = particle(:, 6);   % [V/m]
    trackedParticles(particleInd).Ex = particle(:, 7);   % [V/m]
    trackedParticles(particleInd).Ey = particle(:, 8);   % [V/m]
    particleStartInd = particleEndInd + 1;
    particleInd = particleInd + 1;
end


end
