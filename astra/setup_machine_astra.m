%%%%% FUNCTION setup_machine_astra.m %%%%%
% Mattia Schaer
% March 2014
%
% Setup environment to use ASTRA on the current machine
%
% CALL setup_machine_astra()
%
%%%%%



function setup_machine_astra()

global runParam;


switch runParam.machineName
    
    case 'merlin'
        runParam.genExepath             = '/gpfs/home/schaer_m/EXEC/astra_tia/generator';
        runParam.astraExepath           = '/gpfs/home/schaer_m/EXEC/astra_tia/Astra';
        %runParam.astraParallelExepath   = '/gpfs/home/schaer_m/EXEC/astra_tia/astra_r62_Linux_x86_64_OpenMPI_1.4.3';
        runParam.astraParallelExepath   = '/gpfs/home/schaer_m/EXEC/astra_tia/astra_r62M_Linux_x86_64_OpenMPI_1.4.3';
        runParam.workingPath            = ['/gpfs/home/schaer_m/astraRuns/' runParam.geometryFunction '/'];
        runParam.maxSimInSameFolder     = 1000;
        fmPath                          = '/gpfs/home/schaer_m/PSI-CBand-Gun/fieldmaps/';
        distrPath                       = [runParam.workingPath 'distributions/'];
        runParam.maxJobsNum             = 50;
        
    case 'mpc1238'
        runParam.genExepath             = '/r0disk/software/astra_tia/generator';
        runParam.astraExepath           = '/r0disk/software/astra_tia/astra_r62_Linux_x86_64_OpenMPI_1.4.3';
        runParam.workingPath            = ['/r0disk/schaer_m/astraRuns/' runParam.geometryFunction '/'];
        runParam.maxSimInSameFolder     = 1000;
        fmPath                          = '/r0disk/schaer_m/PSI-CBand-Gun/fieldmaps/';
    
    case 'localLinux'
        runParam.genExepath             = '/opt/astra/generator';
        runParam.astraExepath           = '/opt/astra/Astra';
        runParam.astraParallelExepath   = '/opt/astra/astra_r62_Linux_x86_64_OpenMPI_1.4.3';
        runParam.workingPath            = ['/media/ext4Data/astraRuns/' runParam.geometryFunction '/'];
        runParam.maxSimInSameFolder     = 1000;
        fmPath                          = '/afs/psi.ch/project/newgun/fieldmaps/';
        distrPath                       = [runParam.workingPath 'distributions/'];
        runParam.maxJobsNum             = 50;
        
    case 'localWin'
        runParam.genExepath             = 'C:\Progra~1\astra_win_20130730\generator.exe';
        runParam.astraExepath           = 'C:\Progra~1\astra_win_20130730\Astra.exe';
        runParam.workingPath            = ['C:\Docume~1\schaer_m\astraRuns\' runParam.geometryFunction '\'];
        runParam.maxSimInSameFolder     = 1000;
        fmPath                          = 'W:\fieldmaps\';
end

runParam.globalFieldmapFolder   =  'fieldmaps';
runParam.localFieldmapFolder    =  'fieldmapsLocal';

% save these as a "geometry" parameter for technical reasons
runParam.fmPath.nom                 = fmPath;
runParam.fmPath.unit                = 'string';
runParam.fmPath.var                 = [];

runParam.distrPath.nom              = distrPath;
runParam.distrPath.unit             = 'string';
runParam.distrPath.var              = [];

% Define fields of the structure list which will be filled during the run

% General
runParam.list(1).finalDistrFilename     = [];
runParam.list(1).xLineFilename          = [];
runParam.list(1).yLineFilename          = [];
runParam.list(1).zLineFilename          = [];
runParam.list(1).refLineFilename        = [];
runParam.list(1).gunFmFolder            = [];
runParam.list(1).gunSolFmFolder         = [];

switch runParam.procedure
    case {'tuneTwGun' 'tuneTwGun_2' 'tuneTwGun_3'}
        runParam.list(1).gunFmFolder                = [];
        runParam.list(1).gunFmRe                    = [];
        runParam.list(1).gunFmReNorm                = [];
        runParam.list(1).gunFmIm                    = [];
        runParam.list(1).gunFmImNorm                = [];
        runParam.list(1).z_firstRegularCellMiddle   = [];
        runParam.list(1).z_firstRegularCellEnd      = [];
        runParam.list(1).z_lastRegularCellMiddle    = [];
        runParam.list(1).z_lastRegularCellEnd       = [];
end

end
