%%%%% SCRIPT run_CBandTwGunSBandInjector_fullTwGun120deg1_5ps.m %%%%%
% Mattia Schaer
% August 2015
%
% Run multiple Astra simulations/optimizations of the injector with a
% TW gun in C-band
%
%%%%%



clearvars -global runParam;
global runParam;


% Force user to check parameters before starting simulations
runParam.forceParamCheck = true;
if ~runParam.forceParamCheck
    addpath(genpath('~/PSI-CBand-Gun/CODES/MATLAB/simToolbox/'));
end

% Email results
runParam.emailAddress = 'mattia.schaer@psi.ch';

% Working on which machine?
runParam.machineName = 'merlin';
runParam.queue = 'prime_bd.q';
%runParam.queue = 'all.q';
%runParam.queue = 'short.q';
% Astra version
runParam.runParallel = true;
% Number of nodes for a single simulation
runParam.Nnodes.nom                 = 8;
runParam.Nnodes.unit                = 'int';
runParam.Nnodes.var                 = [];
% max execution time for one simulation
runParam.maxExecutionTime = 10;   % [min]

% Simulation type
runParam.simType = 'astra';
% Specific procedure
runParam.procedure = 'tuneTwGun_3';

% Geometry function
runParam.geometryFunction = 'twGunSwissfelInjector';

% Prefix for this run
runParam.folderPrefix = 'fullTwGun120deg1-5ps-tune';


%% Astra Input Parameters

% Initial particle distribution

runParam.spaceCharge.nom            = true;
runParam.spaceCharge.unit           = 'bool';
runParam.spaceCharge.var            = [];

runParam.bunchCharge.nom            = 0.200;
runParam.bunchCharge.unit           = 'nC';
runParam.bunchCharge.var            = [];

runParam.Npart.nom                  = 10000;
runParam.Npart.unit                 = 'int';
runParam.Npart.var                  = []; %[10000 20000 50000];

runParam.longitudinalProfile.nom    = 'plateau';
runParam.longitudinalProfile.unit   = 'string';
runParam.longitudinalProfile.var    = [];

runParam.pulseFWHM.nom              = 5E-3;
runParam.pulseFWHM.unit             = 'ns';
runParam.pulseFWHM.var              = []; %-5.0E-3:1.0E-3:0.0E-3;
%runParam.pulseFWHM.opt              = [4E-3 3E-3 5E-3 0.01E-3];

runParam.pulseRaisingTime.nom       = 0.75E-3;
runParam.pulseRaisingTime.unit      = 'ns';
runParam.pulseRaisingTime.var       = [];

runParam.laserSigma.nom             = 0.2;
runParam.laserSigma.unit            = 'mm';
runParam.laserSigma.var             = []; %0.0:0.01:0.1;
runParam.laserSigma.opt             = [0.2 0.18 0.22 0.001];

%runParam.Ekin0.nom                  = 0.63E-3;   % 0.91 um/mm-rms
runParam.Ekin0.nom                  = 0.232E-3;   % 0.55 um/mm-rms
runParam.Ekin0.unit                 = 'keV';
runParam.Ekin0.var                  = [];

% Beam dynamics

runParam.title.nom                  = 'SwissFEL Injector with C-band Hybrid Gun';
runParam.title.unit                 = 'string';
runParam.title.var                  = [];

runParam.astraId.nom                = 1;
runParam.astraId.unit               = 'int';
runParam.astraId.var                = [];

runParam.zstop.nom                  = [];
runParam.zstop.unit                 = 'm';
runParam.zstop.var                  = [];

runParam.zPhase.nom                 = 1;
runParam.zPhase.unit                = 'int';
runParam.zPhase.var                 = [];

% CAVITY

runParam.NactiveTws.nom             = 1;
runParam.NactiveTws.unit            = [];
runParam.NactiveTws.var             = [];

runParam.autophasing.nom            = false;
runParam.autophasing.unit           = [];
runParam.autophasing.var            = [];

% When autophasing is turned off, use reference phase at reference position
% to automatically stay on-crest
runParam.stayOnCrest.nom            = true;
runParam.stayOnCrest.unit           = 'bool';
runParam.stayOnCrest.var            = [];

% Calibration for 21 regular cells, 135 MV/m, S-band structures
runParam.tws1RefPos.nom             = 2.000;
runParam.tws1RefPos.unit            = 'm';
runParam.tws1RefPos.var             = [];
runParam.tws1RefPhi.nom             = 64.5;
runParam.tws1RefPhi.unit            = 'deg';
runParam.tws1RefPhi.var             = [];

% % Calibration for 21 regular cells, 135 MV/m, C-band structures
% runParam.tws1RefPos.nom             = 2.100;
% runParam.tws1RefPos.unit            = 'm';
% runParam.tws1RefPos.var             = [];
% runParam.tws1RefPhi.nom             = 49;
% runParam.tws1RefPhi.unit            = 'deg';
% runParam.tws1RefPhi.var             = [];


% Bypasses
% automatically determine position of 2., 3. and 4. TW structure (and surrounding solenoids) from the 1. structure position
runParam.bypassTwsPos.nom           = true;
runParam.bypassTwsPos.unit          = 'bool';
runParam.bypassTwsPos.var           = [];

% set the same accelerating gradient for all of the TW structures
runParam.bypassTwsMaxE.nom          = true;
runParam.bypassTwsMaxE.unit         = 'bool';
runParam.bypassTwsMaxE.var          = [];

% automatically determine position of the 1. solenoid around the 1. TW structure
runParam.bypassSol11Pos.nom         = false;
runParam.bypassSol11Pos.unit        = 'bool';
runParam.bypassSol11Pos.var         = [];

% set the field strength for the second solenoid as that of the first one
runParam.bypassSol12MaxB.nom        = false;
runParam.bypassSol12MaxB.unit       = 'bool';
runParam.bypassSol12MaxB.var        = [];

% set the same field strength for all of the solenoids as that of the second one
runParam.bypassSolMaxB.nom          = true;
runParam.bypassSolMaxB.unit         = 'bool';
runParam.bypassSolMaxB.var          = [];


% Hybrid gun

% input cell length change
runParam.HYBdLi.nom                 = 0.004675;
%runParam.HYBdLi.nom                 = 0;
runParam.HYBdLi.unit                = 'm';
runParam.HYBdLi.var                 = []; %-0.001:0.0005:0.001;
%runParam.HYBdLi.opt                 = [0.004675 0.004 0.005 0.00001];

% output cell length change
runParam.HYBdLo.nom                 = 0.000;
runParam.HYBdLo.unit                = 'm';
runParam.HYBdLo.var                 = []; %-0.0015:0.0005:0.0015;
%runParam.HYBdLo.opt                 = [0.0010445 -0.002 0.002 0.00001];

% scaling in z-direction for the TW cells
runParam.HYBTWscale.nom             = 1;
runParam.HYBTWscale.unit            = '%';
runParam.HYBTWscale.var             = []; %[0.9833 1];
%runParam.HYBTWscale.opt             = [1.008 0.99 1.02 0.0001];

% z position of the end of SW cell
runParam.HYBLi.nom                  = 0.008;   % For: Ez_fullTwGun_120deg_1_firstCell
runParam.HYBLi.unit                 = 'm';
runParam.HYBLi.var                  = [];

% z position of the start of output coupler
runParam.HYBLo.nom                  = 0.182949;   % For: Ez_fullTwGun_120deg_1_lastCell
runParam.HYBLo.unit                 = 'm';
runParam.HYBLo.var                  = [];

% number of TW cells
runParam.HYBntw.nom                 = 10;
runParam.HYBntw.unit                = 'int';
runParam.HYBntw.var                 = []; %-1:1:1;
%runParam.HYBntw.opt                 = [18 13 23 1];

% phase advance: 2*pi * HYBphaseAdvance
runParam.HYBphaseAdvance.nom        = 1/3;
runParam.HYBphaseAdvance.unit       = '2pi';
runParam.HYBphaseAdvance.var        = [];

runParam.HYBregularCellDamping.nom  = 0.11574;   % For: Ez_fullTwGun_120deg_1_regularCell
runParam.HYBregularCellDamping.unit = 'dB';
runParam.HYBregularCellDamping.var  = [];

runParam.gunMaxE.nom                = 135.0;
runParam.gunMaxE.unit               = 'MV/m';
runParam.gunMaxE.var                = [];

runParam.inputCouplerFm.nom         = 'Ez_fullTwGun_120deg_1_firstCell';
runParam.inputCouplerFm.unit        = 'string';
runParam.inputCouplerFm.var         = [];

runParam.outputCouplerFm.nom        = 'Ez_fullTwGun_120deg_1_lastCell';
runParam.outputCouplerFm.unit       = 'string';
runParam.outputCouplerFm.var        = [];

runParam.normalCellsFm.nom          = 'Ez_fullTwGun_120deg_1_regularCell';
runParam.normalCellsFm.unit         = 'string';
runParam.normalCellsFm.var          = [];

runParam.gunFmPrefix.nom            = 'tune3_';
runParam.gunFmPrefix.unit           = 'string';
runParam.gunFmPrefix.var            = [];

runParam.gunComplexFmSumSign.nom    = '-';
runParam.gunComplexFmSumSign.unit   = 'string';
runParam.gunComplexFmSumSign.var    = [];

runParam.gunNue.nom                 = 5.712;
runParam.gunNue.unit                = 'GHz';
runParam.gunNue.var                 = [];

runParam.gunPhi.nom                 = 196.62;
runParam.gunPhi.unit                = 'deg';
runParam.gunPhi.var                 = []; %-10:5:10;
runParam.gunPhi.opt                 = [196.62 186 206 0.01];

runParam.gunPos.nom                 = 0.0;
runParam.gunPos.unit                = 'm';
runParam.gunPos.var                 = [];

runParam.gunSmooth.nom              = 0;
runParam.gunSmooth.unit             = 'int';
runParam.gunSmooth.var              = [];

% 1st S-band TWS
runParam.tws1Fm.nom                 = 'TWS_PSI_SBand.astra';
%runParam.tws1Fm.nom                 = 'TWS_PSI_CBand.astra';
runParam.tws1Fm.unit                = 'string';
runParam.tws1Fm.var                 = [];

runParam.tws1Nue.nom                = 2.997924;
%runParam.tws1Nue.nom                = 5.712;
runParam.tws1Nue.unit               = 'GHz';
runParam.tws1Nue.var                = [];

runParam.tws1MaxE.nom               = 25;
%runParam.tws1MaxE.nom               = 22;
runParam.tws1MaxE.unit              = 'MV/m';
runParam.tws1MaxE.var               = []; %-2:1:8;
%runParam.tws1MaxE.opt               = [20 15 25 0.1];

runParam.tws1Phi.nom                = 0;
runParam.tws1Phi.unit               = 'deg';
runParam.tws1Phi.var                = []; %-2:0.5:2;
%runParam.tws1Phi.opt                = [0.41 -10 10 0.01];

runParam.tws1Pos.nom                = 2.795;
runParam.tws1Pos.unit               = 'm';
runParam.tws1Pos.var                = []; %-0.5:0.1:0.5;
runParam.tws1Pos.opt                = [2.795 2.5 3.3 0.0001];

runParam.tws1Smooth.nom             = 0;
runParam.tws1Smooth.unit            = 'int';
runParam.tws1Smooth.var             = [];

runParam.tws1Numb.nom               = 120;
%runParam.tws1Numb.nom               = 114;   % In reality: SwissFEL C-band 2m structures have 113 regular cells
runParam.tws1Numb.unit              = 'int';
runParam.tws1Numb.var               = [];

% 2nd S-band TWS
runParam.tws2Fm.nom                 = 'TWS_PSI_SBand.astra';
%runParam.tws2Fm.nom                 = 'TWS_PSI_CBand.astra';
runParam.tws2Fm.unit                = 'string';
runParam.tws2Fm.var                 = [];

runParam.tws2Nue.nom                = 2.997924;
%runParam.tws2Nue.nom                = 5.712;
runParam.tws2Nue.unit               = 'GHz';
runParam.tws2Nue.var                = [];

runParam.tws2MaxE.nom               = 25.0;
%runParam.tws2MaxE.nom               = 40.0;
runParam.tws2MaxE.unit              = 'MV/m';
runParam.tws2MaxE.var               = [];

runParam.tws2Phi.nom                = 0;
runParam.tws2Phi.unit               = 'deg';
runParam.tws2Phi.var                = [];

runParam.tws2Pos.nom                = 7.424;
runParam.tws2Pos.unit               = 'm';
runParam.tws2Pos.var                = [];

runParam.tws2Smooth.nom             = 0;
runParam.tws2Smooth.unit            = 'int';
runParam.tws2Smooth.var             = [];

runParam.tws2Numb.nom               = 120;
%runParam.tws2Numb.nom               = 114;   % In reality: SwissFEL C-band 2m structures have 113 regular cells
runParam.tws2Numb.unit              = 'int';
runParam.tws2Numb.var               = [];

% 3rd S-band TWS
%runParam.tws3Fm.nom                 = 'TWS_PSI_SBand.astra';
runParam.tws3Fm.nom                 = 'TWS_PSI_CBand.astra';
runParam.tws3Fm.unit                = 'string';
runParam.tws3Fm.var                 = [];

%runParam.tws3Nue.nom                = 2.997924;
runParam.tws3Nue.nom                = 5.712;
runParam.tws3Nue.unit               = 'GHz';
runParam.tws3Nue.var                = [];

%runParam.tws3MaxE.nom               = 20.0;
runParam.tws3MaxE.nom               = 40.0;
runParam.tws3MaxE.unit              = 'MV/m';
runParam.tws3MaxE.var               = [];

runParam.tws3Phi.nom                = 0.0;
runParam.tws3Phi.unit               = 'deg';
runParam.tws3Phi.var                = [];

runParam.tws3Pos.nom                = 7.95;
runParam.tws3Pos.unit               = 'm';
runParam.tws3Pos.var                = [];

runParam.tws3Smooth.nom             = 0;
runParam.tws3Smooth.unit            = 'int';
runParam.tws3Smooth.var             = [];

%runParam.tws3Numb.nom               = 120;
runParam.tws3Numb.nom               = 114;   % In reality: SwissFEL C-band 2m structures have 113 regular cells
runParam.tws3Numb.unit              = 'int';
runParam.tws3Numb.var               = [];

% 4th S-band TWS
%runParam.tws4Fm.nom                 = 'TWS_PSI_SBand.astra';
runParam.tws4Fm.nom                 = 'TWS_PSI_CBand.astra';
runParam.tws4Fm.unit                = 'string';
runParam.tws4Fm.var                 = [];

%runParam.tws4Nue.nom                = 2.997924;
runParam.tws4Nue.nom                = 5.712;
runParam.tws4Nue.unit               = 'GHz';
runParam.tws4Nue.var                = [];

%runParam.tws4MaxE.nom               = 20.0;
runParam.tws4MaxE.nom               = 40.0;
runParam.tws4MaxE.unit              = 'MV/m';
runParam.tws4MaxE.var               = [];

runParam.tws4Phi.nom                = 0.0;
runParam.tws4Phi.unit               = 'deg';
runParam.tws4Phi.var                = [];

runParam.tws4Pos.nom                = 7.95;
runParam.tws4Pos.unit               = 'm';
runParam.tws4Pos.var                = [];

runParam.tws4Smooth.nom             = 0;
runParam.tws4Smooth.unit            = 'int';
runParam.tws4Smooth.var             = [];

%runParam.tws4Numb.nom               = 120;
runParam.tws4Numb.nom               = 114;   % In reality: SwissFEL C-band 2m structures have 113 regular cells
runParam.tws4Numb.unit              = 'int';
runParam.tws4Numb.var               = [];


% SOLENOIDS

% Gun

runParam.generateGunSolFm.nom       = false;
runParam.generateGunSolFm.unit      = 'bool';
runParam.generateGunSolFm.var       = [];

%runParam.gunSolFm.nom               = 'combinedSolenoid_CBand_long_1.astra';
%runParam.gunSolFm.nom               = 'solGunEnl_hybridGun.astra';
%runParam.gunSolFm.nom               = 'combinedSolenoid_twGun_3a.astra';
runParam.gunSolFm.nom               = 'combinedSolenoid_forTwGun_23_3.astra';
runParam.gunSolFm.unit              = 'string';
runParam.gunSolFm.var               = [];

runParam.gunSolMaxB.nom             = 0.477026;
runParam.gunSolMaxB.unit            = 'T';
runParam.gunSolMaxB.var             = []; %-0.01:0.005:0.01;
runParam.gunSolMaxB.opt             = [0.477026 0.47 0.48 0.00001];

%runParam.gunSolPos.nom              = 0.30;
runParam.gunSolPos.nom              = 0.0;
runParam.gunSolPos.unit             = 'm';
runParam.gunSolPos.var              = [];

runParam.gunSolSmooth.nom           = 0;
runParam.gunSolSmooth.unit          = 'int';
runParam.gunSolSmooth.var           = [];


% 1st TWS
runParam.sol11Fm.nom                = 'INSB_MSLAC_Bz_NOFRINGE.astra';
runParam.sol11Fm.unit               = 'string';
runParam.sol11Fm.var                = [];

runParam.sol11MaxB.nom              = -0.15;
runParam.sol11MaxB.unit             = 'T';
runParam.sol11MaxB.var              = [];
runParam.sol11MaxB.opt              = [-0.15 -0.2 -0.1 0.005];

runParam.sol11Pos.nom               = -0.3;
runParam.sol11Pos.unit              = 'm';
runParam.sol11Pos.var               = [];

runParam.sol11Smooth.nom            = 0;
runParam.sol11Smooth.unit           = 'int';
runParam.sol11Smooth.var            = [];

runParam.sol12Fm.nom                = 'INSB_MSLAC_Bz_NOFRINGE.astra';
runParam.sol12Fm.unit               = 'string';
runParam.sol12Fm.var                = [];

runParam.sol12MaxB.nom              = 0.125;
runParam.sol12MaxB.unit             = 'T';
runParam.sol12MaxB.var              = []; %0:0.05:0.15;
%runParam.sol12MaxB.opt              = [];

runParam.sol12Pos.nom               = 3.85;
runParam.sol12Pos.unit              = 'm';
runParam.sol12Pos.var               = [];

runParam.sol12Smooth.nom            = 0;
runParam.sol12Smooth.unit           = 'int';
runParam.sol12Smooth.var            = [];

runParam.sol13Fm.nom                = 'INSB_MSLAC_Bz_NOFRINGE.astra';
runParam.sol13Fm.unit               = 'string';
runParam.sol13Fm.var                = [];

runParam.sol13MaxB.nom              = 0.1;
runParam.sol13MaxB.unit             = 'T';
runParam.sol13MaxB.var              = [];
%runParam.sol13MaxB.opt              = [0.092 0.01 0.2 0.005];

runParam.sol13Pos.nom               = 4.7;
runParam.sol13Pos.unit              = 'm';
runParam.sol13Pos.var               = [];

runParam.sol13Smooth.nom            = 0;
runParam.sol13Smooth.unit           = 'int';
runParam.sol13Smooth.var            = [];

runParam.sol14Fm.nom                = 'INSB_MSLAC_Bz_NOFRINGE.astra';
runParam.sol14Fm.unit               = 'string';
runParam.sol14Fm.var                = [];

runParam.sol14MaxB.nom              = 0.1;
runParam.sol14MaxB.unit             = 'T';
runParam.sol14MaxB.var              = [];
%runParam.sol14MaxB.opt              = [0.092 0.01 0.2 0.005];

runParam.sol14Pos.nom               = 5.55;
runParam.sol14Pos.unit              = 'm';
runParam.sol14Pos.var               = [];

runParam.sol14Smooth.nom            = 0;
runParam.sol14Smooth.unit           = 'int';
runParam.sol14Smooth.var            = [];

% 2nd TWS
runParam.sol21Fm.nom                = 'INSB_MSLAC_Bz_NOFRINGE.astra';
runParam.sol21Fm.unit               = 'string';
runParam.sol21Fm.var                = [];

runParam.sol21MaxB.nom              = 0.092;
runParam.sol21MaxB.unit             = 'T';
runParam.sol21MaxB.var              = [];
%runParam.sol21MaxB.opt              = [0.092 0.01 0.2 0.005];

runParam.sol21Pos.nom               = 8.0;
runParam.sol21Pos.unit              = 'm';
runParam.sol21Pos.var               = [];

runParam.sol21Smooth.nom            = 0;
runParam.sol21Smooth.unit           = 'int';
runParam.sol21Smooth.var            = [];

runParam.sol22Fm.nom                = 'INSB_MSLAC_Bz_NOFRINGE.astra';
runParam.sol22Fm.unit               = 'string';
runParam.sol22Fm.var                = [];

runParam.sol22MaxB.nom              = 0.1395;
runParam.sol22MaxB.unit             = 'T';
runParam.sol22MaxB.var              = [];
%runParam.sol22MaxB.opt              = [0.092 0.01 0.2 0.005];

runParam.sol22Pos.nom               = 8.85;
runParam.sol22Pos.unit              = 'm';
runParam.sol22Pos.var               = [];

runParam.sol22Smooth.nom            = 0;
runParam.sol22Smooth.unit           = 'int';
runParam.sol22Smooth.var            = [];

runParam.sol23Fm.nom                = 'INSB_MSLAC_Bz_NOFRINGE.astra';
runParam.sol23Fm.unit               = 'string';
runParam.sol23Fm.var                = [];

runParam.sol23MaxB.nom              = 0.1395;
runParam.sol23MaxB.unit             = 'T';
runParam.sol23MaxB.var              = [];
%runParam.sol23MaxB.opt              = [0.092 0.01 0.2 0.005];

runParam.sol23Pos.nom               = 9.70;
runParam.sol23Pos.unit              = 'm';
runParam.sol23Pos.var               = [];

runParam.sol23Smooth.nom            = 0;
runParam.sol23Smooth.unit           = 'int';
runParam.sol23Smooth.var            = [];

runParam.sol24Fm.nom                = 'INSB_MSLAC_Bz_NOFRINGE.astra';
runParam.sol24Fm.unit               = 'string';
runParam.sol24Fm.var                = [];

runParam.sol24MaxB.nom              = 0.1395;
runParam.sol24MaxB.unit             = 'T';
runParam.sol24MaxB.var              = [];
%runParam.sol24MaxB.opt              = [0.092 0.01 0.2 0.005];

runParam.sol24Pos.nom               = 10.55;
runParam.sol24Pos.unit              = 'm';
runParam.sol24Pos.var               = [];

runParam.sol24Smooth.nom            = 0;
runParam.sol24Smooth.unit           = 'int';
runParam.sol24Smooth.var            = [];

% 3rd TWS
runParam.sol31Fm.nom                = 'INSB_MSLAC_Bz_NOFRINGE.astra';
runParam.sol31Fm.unit               = 'string';
runParam.sol31Fm.var                = [];

runParam.sol31MaxB.nom              = 0.02;
runParam.sol31MaxB.unit             = 'T';
runParam.sol31MaxB.var              = [];

runParam.sol31Pos.nom               = 8.0;
runParam.sol31Pos.unit              = 'm';
runParam.sol31Pos.var               = [];

runParam.sol31Smooth.nom            = 0;
runParam.sol31Smooth.unit           = 'int';
runParam.sol31Smooth.var            = [];

runParam.sol32Fm.nom                = 'INSB_MSLAC_Bz_NOFRINGE.astra';
runParam.sol32Fm.unit               = 'string';
runParam.sol32Fm.var                = [];

runParam.sol32MaxB.nom              = 0.02;
runParam.sol32MaxB.unit             = 'T';
runParam.sol32MaxB.var              = [];

runParam.sol32Pos.nom               = 8.85;
runParam.sol32Pos.unit              = 'm';
runParam.sol32Pos.var               = [];

runParam.sol32Smooth.nom            = 0;
runParam.sol32Smooth.unit           = 'int';
runParam.sol32Smooth.var            = [];

runParam.sol33Fm.nom                = 'INSB_MSLAC_Bz_NOFRINGE.astra';
runParam.sol33Fm.unit               = 'string';
runParam.sol33Fm.var                = [];

runParam.sol33MaxB.nom              = 0.02;
runParam.sol33MaxB.unit             = 'T';
runParam.sol33MaxB.var              = [];

runParam.sol33Pos.nom               = 9.70;
runParam.sol33Pos.unit              = 'm';
runParam.sol33Pos.var               = [];

runParam.sol33Smooth.nom            = 0;
runParam.sol33Smooth.unit           = 'int';
runParam.sol33Smooth.var            = [];

runParam.sol34Fm.nom                = 'INSB_MSLAC_Bz_NOFRINGE.astra';
runParam.sol34Fm.unit               = 'string';
runParam.sol34Fm.var                = [];

runParam.sol34MaxB.nom              = 0.02;
runParam.sol34MaxB.unit             = 'T';
runParam.sol34MaxB.var              = [];

runParam.sol34Pos.nom               = 10.55;
runParam.sol34Pos.unit              = 'm';
runParam.sol34Pos.var               = [];

runParam.sol34Smooth.nom            = 0;
runParam.sol34Smooth.unit           = 'int';
runParam.sol34Smooth.var            = [];

% 4th TWS
runParam.sol41Fm.nom                = 'INSB_MSLAC_Bz_NOFRINGE.astra';
runParam.sol41Fm.unit               = 'string';
runParam.sol41Fm.var                = [];

runParam.sol41MaxB.nom              = 0.02;
runParam.sol41MaxB.unit             = 'T';
runParam.sol41MaxB.var              = [];

runParam.sol41Pos.nom               = 8.0;
runParam.sol41Pos.unit              = 'm';
runParam.sol41Pos.var               = [];

runParam.sol41Smooth.nom            = 0;
runParam.sol41Smooth.unit           = 'int';
runParam.sol41Smooth.var            = [];

runParam.sol42Fm.nom                = 'INSB_MSLAC_Bz_NOFRINGE.astra';
runParam.sol42Fm.unit               = 'string';
runParam.sol42Fm.var                = [];

runParam.sol42MaxB.nom              = 0.02;
runParam.sol42MaxB.unit             = 'T';
runParam.sol42MaxB.var              = [];

runParam.sol42Pos.nom               = 8.85;
runParam.sol42Pos.unit              = 'm';
runParam.sol42Pos.var               = [];

runParam.sol42Smooth.nom            = 0;
runParam.sol42Smooth.unit           = 'int';
runParam.sol42Smooth.var            = [];

runParam.sol43Fm.nom                = 'INSB_MSLAC_Bz_NOFRINGE.astra';
runParam.sol43Fm.unit               = 'string';
runParam.sol43Fm.var                = [];

runParam.sol43MaxB.nom              = 0.02;
runParam.sol43MaxB.unit             = 'T';
runParam.sol43MaxB.var              = [];

runParam.sol43Pos.nom               = 9.70;
runParam.sol43Pos.unit              = 'm';
runParam.sol43Pos.var               = [];

runParam.sol43Smooth.nom            = 0;
runParam.sol43Smooth.unit           = 'int';
runParam.sol43Smooth.var            = [];

runParam.sol44Fm.nom                = 'INSB_MSLAC_Bz_NOFRINGE.astra';
runParam.sol44Fm.unit               = 'string';
runParam.sol44Fm.var                = [];

runParam.sol44MaxB.nom              = 0.02;
runParam.sol44MaxB.unit             = 'T';
runParam.sol44MaxB.var              = [];

runParam.sol44Pos.nom               = 10.55;
runParam.sol44Pos.unit              = 'm';
runParam.sol44Pos.var               = [];

runParam.sol44Smooth.nom            = 0;
runParam.sol44Smooth.unit           = 'int';
runParam.sol44Smooth.var            = [];



% Screens

% 1. TWS entrance
runParam.screenTws1Entrance.nom     = false;
runParam.screenTws1Entrance.unit    = 'bool';
runParam.screenTws1Entrance.var     = [];


%% POSTPROCESS
runParam.postprocess.start = true;
runParam.postprocess.fomIndices = [1 3 4 5 6 7];
% runParam.postprocess.fomIndices = [9 10 11 12 13];


%% OPTIMIZE
runParam.optimize.start = true;

% Algorithm
%runParam.optimize.algorithm = 'patternsearch';
%runParam.optimize.algorithm = 'fmincon';
%runParam.optimize.algorithm = 'fminsearch';
%runParam.optimize.algorithm = 'simulannealbnd';
%runParam.optimize.algorithm = 'gamultiobj';

runParam.optimize.algorithm = 'NLOPT_LN_NELDERMEAD';
%runParam.optimize.algorithm = 'NLOPT_LN_SBPLX';
%runParam.optimize.algorithm = 'NLOPT_GN_DIRECT_L';
%runParam.optimize.algorithm = 'NLOPT_GN_DIRECT';
%runParam.optimize.algorithm = 'NLOPT_GN_ISRES';

% Figure-of-merit to use for the optimization
runParam.optimize.fomToOpt  = 3;
% runParam.optimize.fomToOpt  = 9;

% max number of simulations
runParam.optimize.MaxFunEval = 1000;


% Figure-of-merit definition

runParam.fomDef{1}.name                     = 'slice+mismatch';
runParam.fomDef{1}.weights.sliceEmit        = 2.5E6;
runParam.fomDef{1}.weights.mismatch         = 1.0;
% longitudinal length to be considered for slicing
runParam.fomDef{1}.Zrange                   = 2.0E-3;   % [m]
% select slicing with constant charge or constant length
runParam.fomDef{1}.constChargeSlices        = true;
% number of slices the bunch must be divided into
runParam.fomDef{1}.Nslice                   = 20;
% external slices to neglect
runParam.fomDef{1}.neglectExternalSlicesNo  = 2;
% absolute tolerance on this fom to end optimization
runParam.fomDef{1}.fomTol                   = 0.0001;

runParam.fomDef{2}.name                     = 'slice+mismatch+flat';
runParam.fomDef{2}.weights.sliceEmit        = 2.5E6;
runParam.fomDef{2}.weights.mismatch         = 1.0;
runParam.fomDef{2}.weights.flatness         = 0.5;
runParam.fomDef{2}.Zrange                   = 2.0E-3;   % [m]
runParam.fomDef{2}.constChargeSlices        = true;
runParam.fomDef{2}.Nslice                   = 20;
runParam.fomDef{2}.neglectExternalSlicesNo  = 2;
runParam.fomDef{2}.fomTol                   = 0.0001;

runParam.fomDef{3}.name                     = 'brightness+mismatch';
runParam.fomDef{3}.weights.brightness       = 1E-14;
runParam.fomDef{3}.weights.mismatch         = 10;
runParam.fomDef{3}.Zrange                   = 2.0E-3;   % [m]
runParam.fomDef{3}.constChargeSlices        = true;
runParam.fomDef{3}.Nslice                   = 20;
runParam.fomDef{3}.neglectExternalSlicesNo  = 2;
runParam.fomDef{3}.fomTol                   = 0.0001;

runParam.fomDef{4}.name                     = 'sliceEmit';
runParam.fomDef{4}.Zrange                   = 2.0E-3;   % [m]
runParam.fomDef{4}.constChargeSlices        = true;
runParam.fomDef{4}.Nslice                   = 20;
runParam.fomDef{4}.neglectExternalSlicesNo  = 2;
runParam.fomDef{4}.fomTol                   = 0.005E-6;

runParam.fomDef{5}.name                     = 'mismatchPar';
runParam.fomDef{5}.Zrange                   = 2.0E-3;   % [m]
runParam.fomDef{5}.constChargeSlices        = true;
runParam.fomDef{5}.Nslice                   = 20;
runParam.fomDef{5}.neglectExternalSlicesNo  = 2;
runParam.fomDef{5}.fomTol                   = 0.01;

runParam.fomDef{6}.name                     = 'sliceBrightness';
runParam.fomDef{6}.Zrange                   = 2.0E-3;   % [m]
runParam.fomDef{6}.constChargeSlices        = true;
runParam.fomDef{6}.Nslice                   = 20;
runParam.fomDef{6}.neglectExternalSlicesNo  = 2;
runParam.fomDef{6}.fomTol                   = 1E11;

runParam.fomDef{7}.name                     = 'peakCurrent';
runParam.fomDef{7}.Zrange                   = 2.0E-3;   % [m]
runParam.fomDef{7}.constChargeSlices        = true;
runParam.fomDef{7}.Nslice                   = 20;
runParam.fomDef{7}.neglectExternalSlicesNo  = 2;
runParam.fomDef{7}.fomTol                   = 0.1;   % [A]

runParam.fomDef{8}.name                     = 'brightness_ferrarioMatching';
runParam.fomDef{8}.weights.brightness       = 1/8E13;
runParam.fomDef{8}.weights.ferrario         = 1;
% minimum distance at which the Ferrario matching point should be
runParam.fomDef{8}.minimumDistance          = 1.0;   % [m]
% default Ferrario mismatch to be taken in the fom calculation when the
% emittance Maximum is not found
runParam.fomDef{8}.defaultFerrarioMismatch  = 1.0;   % [m]
runParam.fomDef{8}.fomTol                   = 0.0001;

runParam.fomDef{9}.name                     = 'projectedEmit_min';
runParam.fomDef{9}.fomTol                   = 0.0001;

runParam.fomDef{10}.name                    = 'brightness';
runParam.fomDef{10}.fomTol                  = 1E11;

runParam.fomDef{11}.name                    = 'projectedEmit_flatness';
% If zEmitStart and zEmitEnd are not specified
% the end of the first and last regular cell is taken (respectively)
%runParam.fomDef{11}.zEmitStart              = 0.015748;   % [m]
%runParam.fomDef{11}.zEmitEnd                = 0.164462;   % [m]
runParam.fomDef{11}.fomTol                  = 0.0001;

runParam.fomDef{12}.name                    = 'projectedEmit_at';
%runParam.fomDef{12}.zLocation               = 'z_firstRegularCellMiddle';
%runParam.fomDef{12}.zLocation               = 'z_firstRegularCellEnd';
%runParam.fomDef{12}.zLocation               = 'z_lastRegularCellEnd';
runParam.fomDef{12}.zAt                     = 0.235;   % [m]
runParam.fomDef{12}.fomTol                  = 0.0001;

runParam.fomDef{13}.name                     = 'current';
runParam.fomDef{13}.fomTol                   = 0.01;


%% Start run
run_start(runParam);
