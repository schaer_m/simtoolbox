%%%%% FUNCTION write_astraSteering.m %%%%%
% Mattia Schaer
% December 2014
%
% Redefine particle flags considered for emittance calculation. See ASTRA
% manual, chapter 4.13.9, "General redirection of emittance calculation"
%
% CALL write_astraSteering(pars)
%           pars:           struct
%%%%%



function [] = write_astraSteering(pars)


outFile = fopen('Astra_steering.par', 'w');

fprintf(outFile, '&Steering_parameters\n');
fprintf(outFile, '\tStat(2,-90)=F\n');
fprintf(outFile, '\tStat(2,0)=F\n');
fprintf(outFile, '\tStat(2,1)=F\n');
fprintf(outFile, '\tStat(2,2)=T\n');
fprintf(outFile, '\tStat(2,3)=T\n');
fprintf(outFile, '\tStat(2,4)=T\n');
fprintf(outFile, '\tStat(2,5)=T\n');
fprintf(outFile, '/\n\n');

fclose(outFile);


end
