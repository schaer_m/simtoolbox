%%%%% FUNCTION figureOfMerit_projected.m %%%%%
% Mattia Schaer
% March 2014
%
% Compute figure-of-merits based on projected parameters
%
% CALL fom = figureOfMerit_projected(fomDefinition, projectedBeamEvolution)
%           fomDefinition                       struct
%           projectedBeamEvolution              struct
%
% CALL fom = figureOfMerit_projected(fomDefinition, [])
%           fomDefinition                       struct
%      --> Plot the behaviour of the fom based on the passed weights, etc.
%
%%%%%



function fom = figureOfMerit_projected(fomDef, beamData)

global constVars


%% Default weights and parameters for the different foms
if ~isfield(fomDef, 'weights') || isempty(fomDef.weights)
    switch fomDef.name
        case {'brightness' 'brightness_ferrarioMatching'}
            fomDef.weights.brightness   = 1/8E13;
            fomDef.weights.ferrario     = 1;
        case {'projectedEmit_min-(max-min)'}
            fomDef.weights.minEmit      = 1;
            fomDef.weights.emitDiff     = 0.5;
    end
end
if ~isfield(fomDef, 'minimumDistance') || isempty(fomDef.minimumDistance)
    fomDef.minimumDistance = 1.0;   % [m]
end
if ~isfield(fomDef, 'bunchCharge') || isempty(fomDef.bunchCharge)
    fomDef.bunchCharge = 200E-12;   % [C]
end


%% Prepare values to compute figures of merit
% If no beam is specified, only plot behaviour of figure of merit
% in the region of interest
if isempty(beamData)
    
    switch fomDef.name
        case 'projectedEmit_min-(max-min)'
            % next 2 values just to simulate that max and min of emittance were found
            zMaxEmit        = 1.5;   % [m]
            zMinEmit        = 2.0;   % [m]
            minEmitMean         = linspace(0.2, 1.0, 50);   % [um]
            % Variation along 2. dimension --> X-axis
            emitDiff        = repmat(0.0:minEmitMean(2)-minEmitMean(1):1.0, numel(minEmitMean), 1);   % [um]
            % Variation along 1. dimension --> Y-axis
            minEmitMean         = repmat(minEmitMean', 1, size(emitDiff, 2));   % [um]
        case 'brightness_ferrarioMatching'
            % 1E13 is the brightness with 20 A and 0.4 um projected emittance
            % 8E13 is the brightness with 40 A and 0.2 um projected emittance
            brightnessRange = linspace(1E13, 8E13, 50);
            ferrarioMismatchRange = linspace(0, 0.50, 50);
            % Variation along 1. dimension --> Y-axis
            brightness      = repmat(brightnessRange', 1, numel(ferrarioMismatchRange));
            % Variation along 2. dimension --> X-axis
            ferrarioMismatch = repmat(ferrarioMismatchRange, numel(brightnessRange), 1);   % [um]
        otherwise
            fprintf('The chosen fomType is 1D, nothing to display...\n');
            return
    end
    % Prepare figure for plotting
    hFig = findobj('Type','figure', 'Name','figureOfMerit_projected');
    if isempty(hFig)
        hFig = figure('Name','figureOfMerit_projected', 'NumberTitle','off');
    end
    figure(hFig);

% Otherwise normally compute the figure of merit
else
    
    % Extract relevant beam data
    if sum(strcmp(fomDef.name, {'projectedEmit_min-(max-min)', 'projectedEmit_min', 'brightness', 'brightness_ferrarioMatching', 'current'})) == 1
        maxEmit         = get_extrema({beamData{1:3}}, 'xEmitNorm', 'max', -1, false, true);
        minEmit         = get_extrema({beamData{1:3}}, 'xEmitNorm', 'min', -1, false, false);
        maxEmitMean     = 0.5 * (maxEmit.xEmitNorm + maxEmit.yEmitNorm);
        minEmitMean     = 0.5 * (minEmit.xEmitNorm + minEmit.yEmitNorm);
        emitDiff        = maxEmitMean - minEmitMean;
        % Mean current
        % assuming a longitudinal flattop electron distribution at the speed of light
        % factor 2*sqrt(3) is to go from RMS to FWHM for a 1D uniform distribution
        current = constVars.c.si / (2*sqrt(3) * minEmit.zRms*1E-3) * fomDef.bunchCharge;   % [A]
        % Transverse brightness
        % taking factor 4*pi in the definition
        brightness = current / (4*pi * (minEmitMean*1E-6)^2);   % [A/m^2]
    end
    
    % Compute standard beam parameters
    if strcmp(fomDef.name, 'brightness_ferrarioMatching')
        minBeamsize     = get_extrema({beamData{1:3}}, 'xRms', 'min', -1, false, false);
        if minBeamsize.z > fomDef.minimumDistance && maxEmit.z > fomDef.minimumDistance
            ferrarioMismatch = abs(maxEmit.z-minBeamsize.z);
        else
            ferrarioMismatch = fomDef.defaultFerrarioMismatch;
        end
    end
    if strcmp(fomDef.name,  'projectedEmit_flatness')
        emitStart   = interp1(beamData{1}.z, beamData{1}.xEmitNorm, fomDef.zEmitStart);
        emitEnd     = interp1(beamData{1}.z, beamData{1}.xEmitNorm, fomDef.zEmitEnd);
    end

end


%% Define different figures of merit
% Note: the repmat() in the following expressios are needed for the
% plotting of the fom behaviour
switch fomDef.name
    
    % Current at emittance minimum
    case 'current'
        % Make it negative, so that minimizing fom means maximizing the brightness
        fom = -current;
    
    % Transverse brightness
    case 'brightness'
        % Make it negative, so that minimizing fom means maximizing the brightness
        fom = -brightness;
    
    % Transverse brightness and Ferrario matching point precision (position of max. of
    % transverse emittance relative to that of min. of beam size)
    case 'brightness_ferrarioMatching'
        fom = fomDef.weights.brightness * brightness;
        fom = fom - fomDef.weights.ferrario * ferrarioMismatch;
        % Make it negative for minimization
        fom = -fom;
        % If no beam is specified, plot behaviour of figure-of-merit
        if isempty(beamData)
            imagesc(ferrarioMismatchRange, brightnessRange, fom);
            hColorbar = colorbar();
            xlabel('Position difference between emittance max and beam size min [m]');
            ylabel('Brightness at emittance min [A/m^2]');
            title(hColorbar, 'fom');
        end
        
    % Transverse emittance
    case 'projectedEmit_min'
        % Mean X and Y values
        fom = minEmitMean;
        
    % Difference between maximum and minimum of emittance and minimal emittance
    case 'projectedEmit_min-(max-min)'
        % Add last emittance minimum to the fom
        fom = fomDef.weights.minEmit*minEmitMean;
        % Add the difference between last emittance minimum and last emittance maximum, if:
        %     - found maximum (corresponding to the first TW structure position) is at a given minimum distance from the cathode
        %     - found minimum is after the maximum
        if maxEmit.z > fomDef.minimumDistance && maxEmit.z < minEmit.z
            fom = fom - fomDef.weights.emitDiff*emitDiff;
        end
        % If no beam is specified, plot behaviour of figure-of-merit
        if isempty(beamData)
            imagesc(emitDiff(1,:), minEmitMean(:,1), fom);
            hColorbar = colorbar();
            xlabel('Emittance difference between max and min [um]');
            ylabel('Emittance minimum [um]');
            title(hColorbar, 'fom');
        end
        
    % Difference between emittance at two locations along the beamline
    case 'projectedEmit_flatness'
        fom = emitEnd - emitStart;

    % Emittance at a certain location along the beam line
    case 'projectedEmit_at'
        if isnumeric(fomDef.zAt)
            fom = interp1(beamData{1}.z, beamData{1}.xEmitNorm, fomDef.zAt);
        elseif strcmp(fomDef.zAt, 'start')
            fom =  beamData{1}.xEmitNorm(1);
        elseif strcmp(fomDef.zAt, 'end')
            fom =  beamData{1}.xEmitNorm(end);
        end
        
    case 'averageEnergy_at'
        if isnumeric(fomDef.zAt)
            fom = interp1(beamData{3}.z, beamData{3}.Ekin, fomDef.zAt);
        elseif strcmp(fomDef.zAt, 'start')
            fom =  beamData{3}.Ekin(1);
        elseif strcmp(fomDef.zAt, 'end')
            fom =  beamData{3}.Ekin(end);
        end
        
    case 'dEdz_at'
        if isnumeric(fomDef.zAt)
            fom = interp1(beamData{4}.z, beamData{4}.dEdz, fomDef.zAt);
        elseif strcmp(fomDef.zAt, 'start')
            fom =  beamData{4}.dEdz(1);
        elseif strcmp(fomDef.zAt, 'end')
            fom =  beamData{4}.dEdz(end);
        end
        
    case 'energySpread_at'
        if isnumeric(fomDef.zAt)
            fom = interp1(beamData{3}.z, beamData{3}.Erms, fomDef.zAt);
        elseif strcmp(fomDef.zAt, 'start')
            fom =  beamData{3}.Erms(1);
        elseif strcmp(fomDef.zAt, 'end')
            fom =  beamData{3}.Erms(end);
        end
        
    otherwise
        error('UnknownfomDef.name');
        
end


end
