%%%%% FUNCTION figureOfMerit_slice.m %%%%%
% Mattia Schaer
% April 2014
%
% Compute figure-of-merits based on slice parameters
%
% CALL fom = figureOfMerit_slice(fomDefinition, beam)
%           fomDefinition                       struct
%           beam                                struct
%
% CALL fom = figureOfMerit_slice(fomDefinition, [])
%           fomDefinition                       struct
%      --> Plot the behaviour of the fom based on the passed weights, etc.
%
%%%%%



function fom = figureOfMerit_slice(fomDef, beam)


%% Default weights and parameters

FontSize = 16;

% Global
requiredFields = { ...
    'Nslice', ...
    'neglectExternalSlicesNo' ...
};
defaultValues = [ ...
    20; ...
    3 ...
];

% FOM specific
%if ~isfield(fomDef, 'weights') || isempty(fomDef.weights)
    switch fomDef.name
        case {'slice+mismatch' 'slice+mismatch+flat'}
            requiredFields = {requiredFields{:}, ...
                'refEmittance', ...
                'refMismatch', ...
                'equivEmittanceFactor', ...
                'negligibleMismatch', ...
                'equivMismatch'
            };
            defaultValues = [defaultValues(:); ...
                0.21E-6; ...
                1; ...
                0.75; ...
                1.05; ...
                1.2 ...
            ];
            if strcmp(fomDef.name, 'slice+mismatch+flat')
                requiredFields = {requiredFields{:}, ...
                    'weightFlatness' ...
                };
                defaultValues = [defaultValues(:); ...
                    0.5 ...
                ];
            end
                
        case {'brightness+mismatch'}
            requiredFields = {requiredFields{:}, ...
                'refBrightness', ...
                'refMismatch', ...
                'equivBrightnessFactor', ...
                'negligibleMismatch', ...
                'equivMismatch'
            };
            % Set 1: for improvement of SwissFEL baseline with intrinsic
            % emittance 910 nm/mm/rms
%             defaultValues = [defaultValues(:); ...
%                 4.5351E14; ...
%                 1.03; ...
%                 3; ...
%                 1.05; ...
%                 1.2 ...
%             ];
            % Set 2: for improvement of SwissFEL baseline with intrinsic
            % emittance 550 nm/mm/rms
            defaultValues = [defaultValues(:); ...
                9.65E14; ...
                1.14; ...
                3; ...
                1.05; ...
                1.2 ...
            ];
            %
            % Set 3: for improvement with 200 MV/m
%             fomDef.weights.brightness   = 2E-15;
%             fomDef.weights.mismatch     = 56.7;
            %
    end
%end
for ii = 1:numel(requiredFields)
    if ~isfield(fomDef, requiredFields{ii}) || isempty(fomDef.(requiredFields{ii}))
        fomDef.(requiredFields{ii}) = defaultValues(ii);
        warning('Setting default fomDef.%s = %f', requiredFields{ii}, defaultValues(ii));
    end
end


%% Prepare values to compute figures of merit
% If no beam is specified, only plot behaviour of figure-of-merit
% in the region of interest
if isempty(beam)
    switch fomDef.name
        case {'slice+mismatch' 'slice+mismatch+flat'}
            sliceEmitRange  = linspace(0.15E-6, 0.25E-6, 50);   % [m]
            mismatchRange   = linspace(1.0, 1.2, 50);   % []
            % Variation along 1. dimension --> Y-axis
            meanNormSliceEmitX = repmat(sliceEmitRange', 1, numel(mismatchRange));   % [m]
            % Variation along 2. dimension --> X-axis
            meanMismatchX = repmat(mismatchRange, numel(sliceEmitRange), 1);   % []
            % only add mismatch if it is above the negligible threshold
            mismatchContribution = zeros(size(meanMismatchX));
            if strcmp(fomDef.name, 'slice+mismatch')
                addMismatchIndices = meanMismatchX > fomDef.negligibleMismatch;
                mismatchContribution(addMismatchIndices) = ...
                    meanMismatchX(addMismatchIndices) - fomDef.negligibleMismatch;
            elseif strcmp(fomDef.name, 'slice+mismatch+flat')
                % flatness values
                stdNormSliceEmitX   = 0.1E-6;   % [m]
                stdMismatchX        = 0.1;   % []
                addMismatchIndices = meanMismatchX+fomDef.weightFlatness*stdMismatchX > fomDef.negligibleMismatch;
                mismatchContribution(addMismatchIndices) = ...
                    meanMismatchX(addMismatchIndices) + fomDef.weightFlatness*stdMismatchX - fomDef.negligibleMismatch;
            end
        case 'brightness+mismatch'
            % 5E14 is the brightness with 20 A and 0.2 um projected emittance
            % 1.778E15 is the brightness with 40 A and 0.15 um projected emittance
            % without 1/4pi factor in brightness definition
%             brightnessRange  = linspace(0, 5*fomDef.refBrightness, 50);
            %
            % 1.78E15 is the brightness with 40 A and 0.15 um projected emittance
            % 6E15 is the brightness with 60 A and 0.1 um projected emittance
            % without 1/4pi factor in brightness definition
            brightnessRange  = linspace(0, 5*fomDef.refBrightness, 50);
            %
            mismatchRange   = linspace(1.0, 1.2, 50);
            %
            % Variation along 1. dimension --> Y-axis
            brightness = repmat(brightnessRange', 1, numel(mismatchRange));   % [A/m^2]
            % Variation along 2. dimension --> X-axis
            meanMismatchX = repmat(mismatchRange, numel(brightnessRange), 1);
            % only add mismatch if it is above the negligible threshold
            addMismatchIndices = meanMismatchX > fomDef.negligibleMismatch;
            mismatchContribution = zeros(size(meanMismatchX));
            mismatchContribution(addMismatchIndices) = meanMismatchX(addMismatchIndices) - fomDef.negligibleMismatch;
            
        otherwise
            fprintf('The chosen fomType is 1D, nothing to display...\n');
            return
    end
    % Prepare figure for plotting
    hFig = findobj('Type','figure', 'Name','figureOfMerit_slice');
    if isempty(hFig)
        hFig = figure('Name','figureOfMerit_slice', 'NumberTitle','off');
    end
    figure(hFig);
    
% Otherwise normally compute the figure-of-merit
else
    sliceIndices        = 1+fomDef.neglectExternalSlicesNo : fomDef.Nslice-fomDef.neglectExternalSlicesNo;
    meanNormSliceEmitX  = mean(beam.normSliceEmitX(sliceIndices));
    stdNormSliceEmitX   = std(beam.normSliceEmitX(sliceIndices));
    meanMismatchX       = mean(beam.mismatchX(sliceIndices));
    stdMismatchX        = std(beam.mismatchX(sliceIndices));
    switch fomDef.name
        case {'slice+mismatch' 'brightness+mismatch'}
            if meanMismatchX > fomDef.negligibleMismatch
                mismatchContribution = meanMismatchX-fomDef.negligibleMismatch;
            else
                mismatchContribution = 0.0;
            end
        case 'slice+mismatch+flat'
            if meanMismatchX+fomDef.weightFlatness*stdMismatchX > fomDef.negligibleMismatch
                mismatchContribution = meanMismatchX + fomDef.weightFlatness*stdMismatchX - fomDef.negligibleMismatch;
            else
                mismatchContribution = 0.0;
            end
    end
    bunchCharge         = sum(beam.charge);
    peakCurrent         = max(beam.current);
    %brightness          = peakCurrent / (4*pi * (meanNormSliceEmitX)^2);
    brightness          = peakCurrent / meanNormSliceEmitX^2;
end

% Define different figures of merit
% Note: the repmat() in the following expressios are needed for the
% plotting of the fom behaviour
switch fomDef.name

    % Slice emittance + mismatch parameter, WITHOUT flatness
    case 'slice+mismatch'
        weightEmittance       = 1/fomDef.refEmittance;
        weightMismatch         = (fomDef.equivEmittanceFactor-1) / (fomDef.equivMismatch-fomDef.negligibleMismatch);
        fom = weightEmittance * meanNormSliceEmitX;
        fom = fom - weightMismatch * mismatchContribution;
        % If no beam is specified, plot behaviour of figure of merit
        if isempty(beam)
%             imagesc(mismatchRange, sliceEmitRange, fom);
%             hColorbar = colorbar();
%             title(hColorbar, 'fom');
            [C, ~] = contour(mismatchRange, sliceEmitRange, fom, -0.6:0.05:1.4);
            clabel(C, 'FontSize',FontSize);
            xlabel('Mean mismatch []');
            ylabel('Mean normalized slice emittance [m]');
        end
        
    % Slice emittance + mismatch parameter, WITH flatness   
    case 'slice+mismatch+flat'
        fom = fomDef.weightSliceEmit * (meanNormSliceEmitX+fomDef.weightFlatness*stdNormSliceEmitX);
        fom = fom + fomDef.weightMismatch * mismatchContribution;
        % If no beam is specified, plot behaviour of figure of merit
        if isempty(beam)
            imagesc(mismatchRange, sliceEmitRange, fom);
            hColorbar = colorbar();
            xlabel('Mean mismatch []');
            ylabel('Mean normalized slice emittance [m]');
            title(hColorbar, 'fom');
        end

    % Slice emittance only
    case 'sliceEmit'
        fom = meanNormSliceEmitX;

    % Mismatch parameter only
    case 'mismatchPar'
        fom = meanMismatchX;
        
    % Bunch charge only
    case 'bunchCharge'
        fom = -bunchCharge;
        
    % Peak current only
    case 'peakCurrent'
        fom = peakCurrent;
        % Make it negative for minimization
        fom = -fom;
    
    % Brightness
    case 'sliceBrightness'
        fom = -brightness;
        
    % Brightness and mismatch
    case 'brightness+mismatch'
        weightBrightness       = 1/fomDef.refBrightness;
        weightMismatch         = (fomDef.equivBrightnessFactor-1) / (fomDef.equivMismatch-fomDef.negligibleMismatch);
        fom = weightBrightness * brightness;
        fom = fom - weightMismatch * mismatchContribution;
        % Normalize referenc design to unity
%         if fomDef.refMismatch > fomDef.negligibleMismatch
%             fom = fom + fomDef.weightMismatch * (fomDef.refMismatch-fomDef.negligibleMismatch);
%         end
        % Make it negative for minimization
        fom = -fom;
        % If no beam is specified, plot behaviour of figure of merit
        if isempty(beam)
            %imagesc(mismatchRange, brightnessRange, fom);
            %hColorbar = colorbar();
            %title(hColorbar, 'fom');
            [C, ~] = contour(mismatchRange, brightnessRange, fom, -5:0.5:1);
            clabel(C, 'FontSize',FontSize);
            %set(h,'ShowText','on');
            xlabel('Mismatch', 'FontSize',FontSize);
            ylabel('Brightness [A/m^2]', 'FontSize',FontSize);
            set(gca, 'FontSize',FontSize);
%             % Data for tikz
%             listExport = [brightness(:), meanMismatchX(:), fom(:)]; %#ok<NASGU>
%             %save('/afs/psi.ch/project/newgun/reports/GIT_phdthesis/chapters/PhotoInjectorModeling/figures/penaltyFunction_swGun.txt', 'listExport', '-ascii');
%             save('/afs/psi.ch/project/newgun/reports/GIT_phdthesis/chapters/PhotoInjectorModeling/figures/penaltyFunction_twGun.txt', 'listExport', '-ascii');
        end

end


end
