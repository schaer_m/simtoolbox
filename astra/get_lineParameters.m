%%%%% FUNCTION get_lineParameters.m %%%%%
% Mattia Schaer
% December 2013
%
% Read in beam parameters along the beamline from the Astra output files.
% If a second argument is specified, this is an array of positions at which
% tha values must be extracted
%
% CALL beamParameters = get_lineParameters(filepath, [requestedPositions])
%               filepath                        string
%               [requestedPositions]            float(N)
%
%%%%%



function beamParameters = get_lineParameters(filepath, varargin)


%% Recognize file type
[~,filename,~] = fileparts(filepath);
indices = strfind(filename, '.');
fileType = filename(indices(1)+1:end);


%% Read in Astra data
switch fileType
    case {'Xemit' 'Yemit' 'Zemit' 'ref'}
        rawData = load(filepath);
        if nargin > 1
            % Keep only data at the requested positions
            rawData = interp1(rawData(:,1), rawData, varargin{1});
        end
    case 'Cathode'
        inFile = fopen(filepath);
        rawData = textscan(inFile, '%f %f %f %f %f %f %f %c');
        fclose(inFile);
    otherwise
        error('Unknown ASTRA output file');
end 


%% Recognize corresponding quantities
switch fileType
    case 'Xemit'
        beamParameters.z            = rawData(:, 1)';
        beamParameters.t            = rawData(:, 2)';
        beamParameters.xAvg         = rawData(:, 3)';
        beamParameters.xRms         = rawData(:, 4)';
        beamParameters.xPrimeRms    = rawData(:, 5)';
        beamParameters.xEmitNorm    = rawData(:, 6)';
        beamParameters.xxPrimeAvg   = rawData(:, 7)';
    case 'Yemit'
        beamParameters.z            = rawData(:, 1)';
        beamParameters.t            = rawData(:, 2)';
        beamParameters.yAvg         = rawData(:, 3)';
        beamParameters.yRms         = rawData(:, 4)';
        beamParameters.yPrimeRms    = rawData(:, 5)';
        beamParameters.yEmitNorm    = rawData(:, 6)';
        beamParameters.yyPrimeAvg   = rawData(:, 7)';
    case 'Zemit'
        beamParameters.z            = rawData(:, 1)';
        beamParameters.t            = rawData(:, 2)';
        beamParameters.Ekin         = rawData(:, 3)';
        beamParameters.zRms         = rawData(:, 4)';
        beamParameters.Erms         = rawData(:, 5)';
        beamParameters.zEmitNorm    = rawData(:, 6)';
        beamParameters.zEPrimeAvg   = rawData(:, 7)';
    case 'Cathode'
        beamParameters.z                = rawData{1}';
        beamParameters.t                = rawData{2}';
        beamParameters.longScAtCathode  = rawData{3}';
        beamParameters.EzAtCathode      = rawData{4}';
        beamParameters.charge           = rawData{5}';
        beamParameters.minGridPos       = rawData{6}';
        beamParameters.maxGridPos       = rawData{7}';
        beamParameters.emissionFlag     = rawData{8}';
    case 'ref'
        beamParameters.z                = rawData(:, 1)';
        beamParameters.t                = rawData(:, 2)';
        beamParameters.pz               = rawData(:, 3)';
        beamParameters.dEdz             = rawData(:, 4)';
        beamParameters.larmorAngle      = rawData(:, 5)';
        beamParameters.xOff             = rawData(:, 6)';
        beamParameters.yOff             = rawData(:, 7)';
end 


end
