%%%%% FUNCTION check_charge.m %%%%%
% Mattia Schaer
% August 2015
%
% Check that the declared charge in the input file is actually
% extracted from the cathode
%
% CALL chargeOK = check_charge(cathodeFilename, declaredBunchCharge)
%           cathodeFilename         string
%           declaredBunchCharge     float
%
%           chargeOK                bool
%
%%%%%



function chargeOK = check_charge(cathodeFilename, declaredBunchCharge)


% number of end values to consider to determine if charge is constant
endValuesNum = 3;
cathodeParameters = get_lineParameters(cathodeFilename);
finalCharge = cathodeParameters.charge(end-endValuesNum+1:end);
chargeOK = abs(abs(finalCharge(end))-declaredBunchCharge) < 1E-3;
if ~chargeOK
    warning('Loosing charge: %f nC emitted of %f nC declared.', finalCharge(end), declaredBunchCharge);
end


end
