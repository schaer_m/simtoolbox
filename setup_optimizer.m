%%%%% FUNCTION setup_optimizer.m %%%%%
% Mattia Schaer
% March 2014
%
% Depending on the chosen algorithm, setup parameters
%
% CALL setup_optimizer()
%
%%%%%



function NLopt_opt = setup_optimizer(varargin)
% TODO: reorganizre parameters

global runParam;

% For the standard MATLAB optimizers, we do not need NLopt_opt
NLopt_opt = [];

switch runParam.optimize.algorithm
    
    % Standard MATLAB optimizers
    
    case 'patternsearch'
        MeshTol = 0.001;
        % set it small to avoid stopping because of TolX and TolFun
        smallTol = 1E-9;
        runParam.optimize.algorithmOptions = psoptimset( ...
            'MaxFunEvals', runParam.optimize.MaxFunEval, ...
            'Cache', 'on', ...
            'CacheTol', MeshTol*sqrt(runParam.optimize.no), ...
            'CompletePoll', 'on', ...
            'MaxMeshSize', 1, ...
            'TolMesh', MeshTol, ...
            'TolX', smallTol, ...
            'TolFun', smallTol, ...
            'PlotFcns', {@psplotbestf, @psplotmeshsize, @psplotfuncount, @psplotbestx} ...
            );
        
    case 'fmincon'
        TolX = 0.01;
        TolFun = 0.0001;
        runParam.optimize.algorithmOptions = optimoptions( ...
            @fmincon, ...
            'MaxFunEvals',runParam.optimize.MaxFunEval, ...
            'Algorithm', 'interior-point', ...
            'DiffMinChange', TolX, ...
            'TolX', TolX/10, ...
            'TolFun', TolFun, ...
            'PlotFcns', {@optimplotx, @optimplotfunccount, @optimplotfval, @optimplotstepsize} ...
            );
        
    case 'fminsearch'
        TolX = 0.01;
        TolFun = 0.0001;
        runParam.optimize.algorithmOptions = optimset( ...
            'TolFun', TolFun, ...
            'TolX', TolX, ...
            'MaxFunEval', runParam.optimize.MaxFunEval, ...
            'MaxIter', runParam.optimize.MaxFunEval ...
            );
        
    case 'simulannealbnd'
        TolFun = 0.0001;
        runParam.optimize.algorithmOptions = saoptimset( ...
            'TolFun', TolFun, ...
            'MaxFunEval', runParam.optimize.MaxFunEval, ...
            'MaxIter', runParam.optimize.MaxFunEval, ...
            'PlotFcns', {@saplotx, @saplotf, @saplottemperature} ...
            );
        
    case 'gamultiobj'
        runParam.optimize.algorithmOptions = 0;
        
        
        % NLopt optimizers
        
    case 'NLOPT_LN_NELDERMEAD'
        NLopt_opt.algorithm         = NLOPT_LN_NELDERMEAD;
        %NLopt_opt.max_objective     = IF YOU WANT TO MAXIMIZE
        NLopt_opt.min_objective     = @run_single_opt;
        NLopt_opt.lower_bounds      = varargin{1};
        NLopt_opt.upper_bounds      = varargin{2};
        NLopt_opt.xtol_abs          = varargin{3};
        NLopt_opt.ftol_abs          = runParam.fomDef{runParam.optimize.fomToOpt}.fomTol;
        NLopt_opt.maxeval           = runParam.optimize.MaxFunEval;
        
    case 'NLOPT_LN_SBPLX'
        NLopt_opt.algorithm         = NLOPT_LN_SBPLX;
        NLopt_opt.min_objective     = @run_single_opt;
        NLopt_opt.lower_bounds      = varargin{1};
        NLopt_opt.upper_bounds      = varargin{2};
        NLopt_opt.xtol_abs          = varargin{3};
        NLopt_opt.ftol_abs          = runParam.fomDef{runParam.optimize.fomToOpt}.fomTol;
        NLopt_opt.maxeval           = runParam.optimize.MaxFunEval;
        
    case 'NLOPT_GN_DIRECT_L'
        NLopt_opt.algorithm         = NLOPT_GN_DIRECT_L;
        NLopt_opt.min_objective     = @run_single_opt;
        NLopt_opt.lower_bounds      = varargin{1};
        NLopt_opt.upper_bounds      = varargin{2};
        NLopt_opt.xtol_abs          = varargin{3};
        NLopt_opt.ftol_abs          = runParam.fomDef{runParam.optimize.fomToOpt}.fomTol;
        NLopt_opt.maxeval           = runParam.optimize.MaxFunEval;
        
    case 'NLOPT_GN_DIRECT'
        NLopt_opt.algorithm         = NLOPT_GN_DIRECT;
        NLopt_opt.min_objective     = @run_single_opt;
        NLopt_opt.lower_bounds      = varargin{1};
        NLopt_opt.upper_bounds      = varargin{2};
        NLopt_opt.xtol_abs          = varargin{3};
        NLopt_opt.ftol_abs          = runParam.fomDef{runParam.optimize.fomToOpt}.fomTol;
        NLopt_opt.maxeval           = runParam.optimize.MaxFunEval;
        
    case 'NLOPT_GN_ISRES'
        NLopt_opt.algorithm         = NLOPT_GN_ISRES;
        NLopt_opt.min_objective     = @run_single_opt;
        NLopt_opt.lower_bounds      = varargin{1};
        NLopt_opt.upper_bounds      = varargin{2};
        NLopt_opt.xtol_abs          = varargin{3};
        NLopt_opt.ftol_abs          = runParam.fomDef{runParam.optimize.fomToOpt}.fomTol;
        NLopt_opt.maxeval           = runParam.optimize.MaxFunEval;
        NLopt_opt.population        = 5 * runParam.optimize.no;
        
        
    otherwise
        error('Unknown optimization method');
        
end

end
