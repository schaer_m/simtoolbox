%%%%% FUNCTION run_start.m %%%%%
% Mattia Schaer
% March 2014
%
% Once all of the necessary parameters have been defined start the
% simulation run
%
% CALL run_start(runParam)
%           runParam                    struct
%
% CALL run_start(runFilepath)
%           runFilepath                 string
%
%%%%%



function run_start(varargin)

global runParam;


if isstruct(varargin{1})
    
    runParam = varargin{1};
    runParam.newRun = true;
    % Setup run
    setup_machine();
    executeRun = setup_run();
    
elseif ischar(varargin{1})

    runFilepath = varargin{1};
    if nargin == 1
        executeRun = restart_run(runFilepath);
    elseif nargin == 2
        executeRun = restart_run(runFilepath, varargin{2});
    end

else
    
    error('First input argument must be either struct (new run) or string (run restart)');

end

setup_constants();

% Execute run
if executeRun
    startRun = tic();
    fprintf(['\n********************\n' ...
             'RUN STARTED\n'
             ]);
    
    if runParam.newRun
        % Create run folder
        mkdir([runParam.workingPath runParam.runFolderName]);
        if strcmp(runParam.simType, 'astra')
            % Create local fieldmap folder
            mkdir(fullfile(runParam.workingPath, runParam.runFolderName, runParam.localFieldmapFolder));
        end
    end
    % Jump into run folder
    cd([runParam.workingPath runParam.runFolderName]);
    if runParam.newRun
        % If necessary, create subfolders to avoid too large number of
        % files in the same folder
        if runParam.active.combinationsNo > runParam.maxSimInSameFolder
            for ii = 1:ceil(runParam.active.combinationsNo/runParam.maxSimInSameFolder)
                mkdir(num2str(ii*runParam.maxSimInSameFolder));
            end
        end
        if strcmp(runParam.simType, 'astra')
            % Link to fieldmaps
            system(['ln -s ' runParam.fmPath.nom ' ./fieldmaps']);
%             fOut = fopen('tmp.sh', 'w');
%             fprintf(fOut, ['#!/bin/bash\nln -s ' runParam.fmPath.nom ' ./fieldmaps']);
%             fclose(fOut);
%             ! chmod u+rwx tmp.sh
%             ! source tmp.sh
            % Link to distributions
            system(['ln -s ' runParam.distrPath.nom ' ./distributions']);
        end
    end
    
    if runParam.optimize.start
        % Create archive folder where to move simulations after completion
        for ii = 1:runParam.active.combinationsNo
            mkdir(['archive_' num2str(ii)]);
        end
        % Optimize parameters
        fprintf('optimizing...\n');
        run_optimize();
    else
        % Run normally
        fprintf('looping...\n');
        run_loop();
    end
    
    % Save run parameters to MATLAB file
    save(fullfile(runParam.workingPath, runParam.runFolderName, runParam.logFilename), 'runParam');
    
    fprintf(['\nRUN FINISHED\n' ...
             '********************\n\n'
             ]);
    % Stop time
    toc(startRun);
end

end
