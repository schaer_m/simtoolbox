%%%%% FUNCTION run_loop.m %%%%%
% Mattia Schaer
% March 2014
%
% Perform the parameter sweep
%
% CALL run_loop()
%
%%%%%



function run_loop()

global runParam;


%% Prepare variable to store figure-of-merits
if runParam.newRun && runParam.postprocess.start
    runParam.fom = NaN(numel(runParam.postprocess.fomIndices), ...
                             runParam.active.combinationsNo );
end


%% Loop over active parameters

% % Work in parallel if not on the cluster with SGE
% if numel(gcp('nocreate')) == 0
%     if strcmp(runParam.machineName,'merlin') || runParam.runParallel
%         parpool('local', 1);
%     else
%         parpool('local', runParam.Nnodes.nom);
%     end
% end
% parfor simIndex = runParam.active.restartInd:runParam.active.combinationsNo

for simIndex = runParam.active.restartInd:runParam.active.combinationsNo
    fprintf('\nRunning simulation %d/%d\n', simIndex, runParam.active.combinationsNo);
    runParam.active.simInd = simIndex;
    
    if ~runParam.simProgress.finished(simIndex)
        % Remove eventual old files
        if runParam.simProgress.started(simIndex)
            delete(['*' runParam.list(simIndex).simFilebase '.*']);
        end
        % Start simulation
        eval(['procedure_' runParam.procedure '();']);
        % WARNING: We put this here because procedure_xxx() functions
        % can modify runParam.list which is saved in the .mat file within
        % few cycles
        runParam.simProgress.started(simIndex) = true;
    end
    
    if strcmp(runParam.machineName, 'merlin')
        postproInd = check_jobs_merlin();
    else
        runParam.simProgress.finished(simIndex) = true;
        runParam.active.restartInd = simIndex;
        postproInd = simIndex;
    end
    
    % Postprocess performed simulations
    if runParam.postprocess.start
        for ii = postproInd
            % Calculate figure-of-merits for finished simulations
            for jj = 1:numel(runParam.postprocess.fomIndices)
                runParam.fom(jj,ii) = run_fom(runParam.fomDef{runParam.postprocess.fomIndices(jj)}, runParam.list(ii));
            end
        end
    end
    
    % For large runs, move files to subfolders
    if runParam.active.combinationsNo > runParam.maxSimInSameFolder
        for ii = postproInd
            exeString = [runParam.moveCommand ' *' runParam.list(ii).simFilebase '.* ' num2str(runParam.maxSimInSameFolder*ceil(ii/runParam.maxSimInSameFolder))];
            system(exeString);
            exeString = [runParam.moveCommand ' *' runParam.list(ii).simFilebase '_* ' num2str(runParam.maxSimInSameFolder*ceil(ii/runParam.maxSimInSameFolder))];
            system(exeString);
            exeString = [runParam.moveCommand ' r' num2str(runParam.runNo) '_' num2str(ii) '.* ' num2str(runParam.maxSimInSameFolder*ceil(ii/runParam.maxSimInSameFolder))];
            system(exeString);
        end
    end
    
    % Save run parameters to .mat file
    if ~isempty(postproInd)
        save(fullfile(runParam.workingPath, runParam.runFolderName, runParam.logFilename), 'runParam');
    end
    
end

end
