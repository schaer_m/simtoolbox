function [data, fileOk] = read_data_file(path, filename, filetype, varargin)

switch nargin
    % Default (nothing specified)
    case 3
        sf7Type = 'superfish';
    case 4
        sf7Type = varargin{1};
    otherwise
        error('Unknown input parameter sequence');
end


data = [];
fileOk = true;

foundFile = dir(fullfile(path, filename));
% If the file is not found
if isempty(foundFile)
    fprintf(['File ' filename ' not found!\n']);
    fileOk = false;
    return
end

%Read file
foundFilepath = fullfile(path, foundFile.name);
switch filetype
    case 'sf7'
        data = read_sf7_outputfile(foundFilepath, sf7Type);
    case 'fish'
        data = read_fish_outputfile(foundFilepath);
    case 'sfo'
        data = read_sfo_outputfile(foundFilepath);
end

% If the file has been found but does not contain any data
if isempty(data)
    fprintf(['File ' strrep(foundFilepath, '\', '\\') ' does not contain valid data!\n']);
    fileOk = false;
    return
end