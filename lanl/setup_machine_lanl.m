%%%%% FUNCTION setup_machine_lanl.m %%%%%
% Mattia Schaer
% March 2014
%
% Setup environment to use ASTRA on the current machine
%
% CALL setup_machine_lanl()
%
%%%%%



function setup_machine_lanl()

global runParam;

switch runParam.machineName
    
    case 'localLinux'
        runParam.lanlExepath            = 'wine ~/.wine/drive_c/LANL/';
        runParam.workingPath            = ['/scratch/lanlRuns/' runParam.geometryFunction '/'];
        runParam.maxSimInSameFolder     = 1000;
    
    case 'laptopLinux'
        runParam.lanlExepath            = 'wine ~/.wine/drive_c/LANL/';
        runParam.workingPath            = ['/home/tia/Documents/ethz_doktorat/lanlRuns/' runParam.geometryFunction '/'];
        runParam.maxSimInSameFolder     = 1000;
        
    case 'localWin'
        runParam.lanlExepath            = 'C:\PROGRA~2\LANL\';
        %runParam.workingPath            = ['E:\lanlRuns\' runParam.geometryFunction '\'];
        runParam.workingPath            = ['C:\Users\schaer_m\Documents\lanlRuns\' runParam.geometryFunction '\'];
        runParam.maxSimInSameFolder     = 1000;
        
end

% Define fields of the structure list which will be filled during the run
switch runParam.procedure
    
    case {'autofish' 'cfish'}
        % for normal accelerating structures (not guns) just interpret
        % "xCathode" as "xStart"
        runParam.list(1).xCathode           = [];
        runParam.list(1).xEnd               = [];
        
    case 'irisOptim'
        runParam.list(1).xEnd               = [];
        
    case 'poisson'
        runParam.list(1).domainLmin         = [];
        runParam.list(1).domainLmax         = [];
        runParam.list(1).domainR            = [];
        
    otherwise
        error('Unknown procedure');
        
end

end
