%%% FUNCTION read_sfo_outputfile.m %%%
% Mattia Schaer
% July 2013
%
% Import data from the output file of SFO
%
% CALL sfoData = read_sfo_outputfile(filepath)
%
%           sfoData:   struct
%           filepath:   string
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function sfoData = read_sfo_outputfile(filepath)

    % Open file
    inFile = fopen(filepath);

    % Look for the desired data
    sfoData.q = [];
    sfoData.shuntImpedance = [];
    sfoData.EmaxSurface = [];
    sfoData.HmaxSurface = [];
    sfoData.Ezero = [];
    resultsWanted = 5;
    
    resultsReaden = 0;
    while resultsReaden < resultsWanted
        tmpLine = fgetl(inFile);
        % Q and Shunt impedance (same line)
        searchQ = strfind(tmpLine, 'Q');
        searchShuntImp = strfind(tmpLine, 'Shunt impedance');
        searchEmax = strfind(tmpLine, 'EMAX');
        searchHmax = strfind(tmpLine, 'HMAX');
        searchEzero = strfind(tmpLine, 'EZERO');
        if  ~isempty(searchQ) && ~isempty(searchShuntImp)
            sfoData.q               = str2double(tmpLine(7:26));
            sfoData.shuntImpedance  = str2double(tmpLine(44:56));
            resultsReaden = resultsReaden + 2;
            searchQ = [];
            searchShuntImp = [];
        elseif ~isempty(searchEmax) && isempty(sfoData.EmaxSurface)
            sfoData.EmaxSurface = str2double(tmpLine(5:32));
            resultsReaden = resultsReaden + 1;
            searchEmax = [];
        elseif ~isempty(searchHmax) && isempty(sfoData.HmaxSurface)
            sfoData.HmaxSurface = str2double(tmpLine(5:32));
            resultsReaden = resultsReaden + 1;
            searchHmax = [];
        elseif ~isempty(searchEzero) && ~isempty(sfoData.EmaxSurface) && isempty(sfoData.Ezero)
            sfoData.Ezero = str2double(tmpLine(6:32));
            resultsReaden = resultsReaden + 1;
            searchEzero = [];
        end
    end
    
    % Close file
    fclose(inFile);
    
end