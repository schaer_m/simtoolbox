%%%%% FUNCTION procedure_autofish.m %%%%%
% Mattia Schaer
% March 2014
%
% Run the single AutoFish simulation
%
% CALL [modifiedSimulationParameters] = procedure_autofish([actualSimulationParameters])
%           actualSimulationParameters              struct
%
%           modifiedSimulationParameters            struct
%
%%%%%



function varargout = procedure_autofish(varargin)

global runParam;


% Manage input
switch nargin
    case 0
        actualPars = runParam.list(runParam.active.simInd);
    case 1
        actualPars = varargin{1};
    otherwise
        error('Unknown sequence of input parameters');
end

% Run Autofish
fprintf('... Autofish\n');
% call geometry function to generate AutoFish input file
% WARNING: with bypasses, the geometry function might change the actual parameters, therefore return them
eval(['actualPars = geometry_' runParam.geometryFunction '(actualPars);']);
exeString = [runParam.lanlExepath 'AUTOFISH.EXE ' actualPars.simFilebase '.af'];
system(exeString);
% Rename files
exeString = [runParam.moveCommand ' OUTAUT.TXT ' actualPars.simFilebase '.MESH'];
[~,~] = system(exeString);
exeString = [runParam.moveCommand ' OUTFIS.TXT ' actualPars.simFilebase '.FISH'];
[~,~] = system(exeString);
if actualPars.freqScan
    exeString = [runParam.moveCommand ' FishScan.TBL ' actualPars.simFilebase '.TBL'];
    system(exeString);
end

% Run SF7 to get fieldmap (on-axis)
fprintf('... SF7\n');
stepNo = round((actualPars.xEnd-actualPars.xCathode) / runParam.sf7.stepSize);
write_sf7_inputfile(actualPars.simFilebase, 'Line', ...
    [actualPars.xCathode 0.0 actualPars.xEnd 0.0], stepNo);
exeString = [runParam.lanlExepath 'SF7.EXE ' actualPars.simFilebase '.IN7 ' upper(actualPars.simFilebase) '.T35'];
system(exeString);
exeString = [runParam.moveCommand ' OUTSF7.TXT ' actualPars.simFilebase '.OUT7'];
[~,~] = system(exeString);

% Manage output
switch nargout
    case 0
        runParam.list(runParam.active.simInd) = actualPars;
    case 1
        varargout{1} = actualPars;
    otherwise
        error('Unknown sequence of output parameters');
end

end
