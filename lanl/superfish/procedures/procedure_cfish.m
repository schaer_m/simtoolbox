%%%%% FUNCTION procedure_cfish.m %%%%%
% Mattia Schaer
% March 2014
%
% Run the single AutoFish simulation
%
% CALL procedure_cfish([actualParameters])
%           actualParameters                    struct
%
%%%%%



function varargout = procedure_cfish(varargin)

global runParam;


% Manage input
switch nargin
    case 0
        actualPars = runParam.list(runParam.active.simInd);
    case 1
        actualPars = varargin{1};
    otherwise
        error('Unknown sequence of input parameters');
end

% Run Automesh
fprintf('... Automesh\n');
% call geometry function to generate AutoFish input file
% WARNING: with bypasses, the geometry function might change the actual parameters, therefore return them
eval(['actualPars = geometry_' runParam.geometryFunction '(actualPars);']);
exeString = [runParam.lanlExepath 'AUTOMESH.EXE ' actualPars.simFilebase '.am'];
system(exeString);
% Rename files
exeString = [runParam.moveCommand ' OUTAUT.TXT ' actualPars.simFilebase '.MESH'];
[~,~] = system(exeString);
if actualPars.freqScan
    exeString = [runParam.moveCommand ' FishScan.TBL ' actualPars.simFilebase '.TBL'];
    system(exeString);
end
% Run CFish
fprintf('... CFish\n');
exeString = [runParam.lanlExepath 'CFISH.EXE ' upper(actualPars.simFilebase) '.T35'];
system(exeString);
exeString = [runParam.moveCommand ' OUTFIS.TXT ' actualPars.simFilebase '.FISH'];
system(exeString);
% Run SFO
fprintf('... SFO\n');
exeString = [runParam.lanlExepath 'SFO.EXE ' upper(actualPars.simFilebase) '.T35'];
system(exeString);


% % Run SF7 to get fieldmap (on-axis)
% fprintf('... SF7\n');
% stepNo = round((actualPars.xEnd-actualPars.xCathode) / runParam.sf7.stepSize);
% write_sf7_inputfile(actualPars.simFilebase, 'Line', ...
%     [actualPars.xCathode 0.0 actualPars.xEnd 0.0], stepNo);
% exeString = [runParam.lanlExepath 'SF7.EXE ' actualPars.simFilebase '.IN7 ' upper(actualPars.simFilebase) '.T35'];
% system(exeString);
% exeString = [runParam.moveCommand ' OUTSF7.TXT ' actualPars.simFilebase '.OUT7'];
% [~,~] = system(exeString);
% 
% % TODO: Eventually move this to postprocessing
% % Run SF7 to get total electric field along the gun radius
% fprintf('... SF7\n');
% stepNo = round(actualPars.xEnd / runParam.sf7.stepSize);
% write_sf7_inputfile([actualPars.simFilebase '_radial'], 'Line', ...
%     [actualPars.xCathode 0.0 actualPars.xCathode actualPars.r1], stepNo);
% exeString = [runParam.lanlExepath 'SF7.EXE ' actualPars.simFilebase '_radial.IN7 ' upper(actualPars.simFilebase) '.T35'];
% system(exeString);
% exeString = [runParam.moveCommand ' OUTSF7.TXT ' actualPars.simFilebase '_radial.OUT7'];
% [~,~] = system(exeString);

% Manage output
switch nargout
    case 0
        runParam.list(runParam.active.simInd) = actualPars;
    case 1
        varargout{1} = actualPars;
    otherwise
        error('Unknown sequence of output parameters');
end

end
