%%%%% FUNCTION procedure_irisOptim.m %%%%%
% Mattia Schaer
% March 2014
%
% Run some autofish simulations in order to optimize the iris of a single
% cell
%
% CALL procedure_irisOptim()
%
%%%%%



function varargout = procedure_irisOptim(varargin)

global runParam;


% Manage input
switch nargin
    case 0
        simIndex    = runParam.active.simInd;
        actualPars  = runParam.list(runParam.active.simInd);
    otherwise
        error('Unknown sequence of input parameters');
end

% Run Autofish first time with the radius of the nearest geometry
if simIndex > 1
    if mod(simIndex-1, runParam.active.length(1)) ~= 0
        actualPars.r1 = runParam.list(simIndex-1).r1;
    else
        actualPars.r1 = runParam.list(simIndex-runParam.active.length(1)).r1;
    end
end
procedure_autofish(actualPars);
% get resulting frequency
[fishData, fileOK] = read_data_file([], [actualPars.simFilebase '.FISH'], 'fish');
r1      = [];
freq    = [];
if fileOK
    r1(1)           = actualPars.r1;
    freq(1)         = fishData.freq;
else
    warning('Something went wrong in iteration %d (point 1).', runParam.active.simInd);
    return
end

% compute r on the other side with respect to the goal frequency
r1(2)            = r1(1) + 1/runParam.dfOVERdr * 2*(actualPars.freq - fishData.freq);

% Run autofish with new radius
actualPars.r1   = r1(2);
procedure_autofish(actualPars);
% get new frequency
[fishData, fileOK] = read_data_file([], [actualPars.simFilebase '.FISH'], 'fish');
if fileOK
    freq(2)         = fishData.freq;
else
    warning('Something went wrong in iteration %d (point 2).', runParam.active.simInd);
    return
end

% Interpolate between the two points to get radius for goal frequency
if ~isempty(freq) && numel(freq) == 2 && abs(freq(1)-freq(2)) > 1E-9
    actualPars.r1   = interp1(freq, r1, actualPars.freq);
    % Run final simulation
    procedure_autofish(actualPars);
else
    warning('Something went wrong in iteration %d.', runParam.active.simInd);
end

% Manage output
switch nargout
    case 0
        runParam.list(runParam.active.simInd) = actualPars;
    otherwise
        error('Unknown sequence of output parameters');
end

end
