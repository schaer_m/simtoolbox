%%% FUNCTION read_fish_outputfile.m %%%
% Mattia Schaer
% February 2013
%
% Import data from the output file of SuperFish
%
% CALL fishData = read_fish_outputfile(filepath)
%
%           fishData:   struct
%           filepath:   string
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function fishData = read_fish_outputfile(filepath)

    % Open file
    inFile = fopen(filepath);
    
    % Jump to the results section
    resultsFound = false;
    while ~resultsFound
        tmpLine = fgetl(inFile);
        % Look for the string determining the beginning of the results section
        if strfind(tmpLine, 'Problem variables computed by this code.')
            resultsFound = true;
        % If Fish outputfile exists but does not contain the desired section
        % e.g. by a frequency scan
        elseif strfind(tmpLine, 'Starting scan with the driving point at')
            fishData = [];
            return;
        end
    end

    % Look for the desired data
    resultsWanted = 1;
    resultsReaden = 0;
    while resultsReaden < resultsWanted
        tmpLine = fgetl(inFile);
        if strfind(tmpLine, 'FREQ')
            fishData.freq = str2double(tmpLine(10:32));
            resultsReaden = resultsReaden + 1;
        end
    end
    
    % Close file
    fclose(inFile);
    
end
