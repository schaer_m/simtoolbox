%%%%% SCRIPT run_PSIGun1_withPlug_spring.m %%%%%
% Mattia Schaer
% March 2014
%
% Run multiple SuperFish simulations of the PSI Gun 1 with cathode plug
% (short circuited by the RF contact spring)
%%%%%



clearvars -global runParam;


% Force user to check parameters before starting simulations
runParam.forceParamCheck = true;

% Working on which machine?
runParam.machineName = 'localWin';

% Simulation type
runParam.simType = 'lanl';
% Specific procedure
runParam.procedure = 'autofish';

% Geometry function
runParam.geometryFunction = 'PSIGun1_withPlug_spring';

% Prefix for this run
runParam.folderPrefix = 'retractedCathode';


%% SuperFish geometry parameters

runParam.cellsNum.nom       = 3;
runParam.cellsNum.unit      = 'int';
runParam.cellsNum.var       = [];

runParam.meshSizeX.nom      = 0.02;
runParam.meshSizeX.unit     = 'cm';
runParam.meshSizeX.var      = []; %[0.0 0.005 0.015 0.025];

runParam.meshSizeY.nom      = 0.025;
runParam.meshSizeY.unit     = 'cm';
runParam.meshSizeY.var      = [];

runParam.lineRegions.nom    = [1 1];
runParam.lineRegions.unit   = 'bool';
runParam.lineRegions.var    = [];

runParam.xCathode.nom       = 0.0;
runParam.xCathode.unit      = 'cm';
runParam.xCathode.var       = -0.0300:0.0050:0.000;

runParam.hPlug.nom       = 1.0;
runParam.hPlug.unit      = 'cm';
runParam.hPlug.var       = [];

runParam.rPlug.nom       = 0.0500;
runParam.rPlug.unit      = 'cm';
runParam.rPlug.var       = [];

runParam.rCathode.nom       = 0.0500;
runParam.rCathode.unit      = 'cm';
runParam.rCathode.var       = [];

runParam.dxGap.nom          = 0.5000;
runParam.dxGap.unit         = 'cm';
runParam.dxGap.var          = [];

runParam.dyGap.nom          = 0.0500;
runParam.dyGap.unit         = 'cm';
runParam.dyGap.var          = [];

runParam.rGap.nom           = 0.0500;
runParam.rGap.unit          = 'cm';
runParam.rGap.var           = [];

runParam.l1.nom             = 3.05;
runParam.l1.unit            = 'cm';
runParam.l1.var             = [];

runParam.hHalf.nom          = 4.1676;
runParam.hHalf.unit         = 'cm';
runParam.hHalf.var          = [];

runParam.rHalf.nom          = 0.6;
runParam.rHalf.unit         = 'cm';
runParam.rHalf.var          = [];

runParam.aHalf.nom          = 1.0;
runParam.aHalf.unit         = 'cm';
runParam.aHalf.var          = [];

runParam.bHalf.nom          = 1.7;
runParam.bHalf.unit         = 'cm';
runParam.bHalf.var          = [];

runParam.iHalf.nom          = 1.6;
runParam.iHalf.unit         = 'cm';
runParam.iHalf.var          = [];

runParam.l2.nom             = 4.6;
runParam.l2.unit            = 'cm';
runParam.l2.var             = [];

runParam.h2.nom          = 4.2714;
runParam.h2.unit         = 'cm';
runParam.h2.var          = [];

runParam.a2.nom          = 1.0;
runParam.a2.unit         = 'cm';
runParam.a2.var          = [];

runParam.b2.nom          = 1.7;
runParam.b2.unit         = 'cm';
runParam.b2.var          = [];

runParam.i2.nom          = 1.6;
runParam.i2.unit         = 'cm';
runParam.i2.var          = [];

runParam.l3.nom             = 4.6;
runParam.l3.unit            = 'cm';
runParam.l3.var             = [];

runParam.h3.nom          = 4.28703;
runParam.h3.unit         = 'cm';
runParam.h3.var          = [];

runParam.r3.nom          = 0.6;
runParam.r3.unit         = 'cm';
runParam.r3.var          = [];

runParam.a3.nom          = 1.0;
runParam.a3.unit         = 'cm';
runParam.a3.var          = [];

runParam.b3.nom          = 1.7;
runParam.b3.unit         = 'cm';
runParam.b3.var          = [];

runParam.i3.nom          = 1.6;
runParam.i3.unit         = 'cm';
runParam.i3.var          = [];

runParam.xEnd.nom           = 17.5;
runParam.xEnd.unit          = 'cm';
runParam.xEnd.var           = [];

% Frequency of the mode
runParam.freq.nom                       = 2997.912;
runParam.freq.unit                      = 'MHz';
runParam.freq.var                       = []; %10:10:50;

% Perform frequency scan
runParam.freqScan.nom                   = false;
runParam.freqScan.unit                  = 'bool';
runParam.freqScan.var                   = [];

runParam.freqScanStartFreq.nom          = 2900.0;
runParam.freqScanStartFreq.unit         = 'MHz';
runParam.freqScanStartFreq.var          = [];

runParam.freqScanStepFreq.nom           = 1.0;
runParam.freqScanStepFreq.unit          = 'MHz';
runParam.freqScanStepFreq.var           = [];

runParam.freqScanStepNo.nom             = 101;
runParam.freqScanStepNo.unit            = 'int';
runParam.freqScanStepNo.var             = [];


% Step size [cm] for the output fieldmap
runParam.sf7.stepSize = 0.1;


%% POSTPROCESSING
runParam.postprocess.start = false;


%% OPTIMIZATION
runParam.optimize.start = false;


%% Start run
run_start(runParam);
