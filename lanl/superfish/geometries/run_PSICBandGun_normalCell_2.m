%%%%% SCRIPT run_PSICBandGun_normalCell_2.m %%%%%
% Mattia Schaer
% March 2014
%
% Run multiple SuperFish simulations of a normal cell for a C-band gun
% WITH a flat edge at the end of the iris curvature
%
%%%%%



clearvars -global runParam;
global runParam;


% Force user to check parameters before starting simulations
runParam.forceParamCheck = true;

% Working on which machine?
runParam.machineName = 'localWin';
%runParam.machineName = 'laptopLinux';

% Simulation type
runParam.simType = 'lanl';
% Specific procedure
runParam.procedure = 'autofish';
%runParam.procedure = 'irisOptim';

% Geometry function
runParam.geometryFunction = 'PSICBandGun_normalCell_2';

% Prefix for this run
runParam.folderPrefix = 'rEdge1mm-i1p2mm-optimIris';
% Estimation of df/dr
runParam.dfOVERdr = -2650;   % [MHz/cm]


%% SuperFish input parameters

runParam.cellsNum.nom                   = 1;
runParam.cellsNum.unit                  = 'int';
runParam.cellsNum.var                   = [];

runParam.xCathode.nom                   = 0.0;
runParam.xCathode.unit                  = 'cm';
runParam.xCathode.var                   = [];

runParam.rEdge.nom                      = 0.1;
runParam.rEdge.unit                     = 'cm';
runParam.rEdge.var                      = [];

runParam.r1.nom                         = 2.4736;
runParam.r1.unit                        = 'cm';
runParam.r1.var                         = []; %-0.01:0.005:0.01;

runParam.lFlat.nom                      = 0.0100;
runParam.lFlat.unit                     = 'cm';
runParam.lFlat.var                      = [];

runParam.l1.nom                         = 2.6242;   % beta = 1
runParam.l1.unit                        = 'cm';
runParam.l1.var                         = [];

runParam.a1.nom                         = 0.7500;
runParam.a1.unit                        = 'cm';
runParam.a1.var                         = []; %0:0.025:0.1;

runParam.b1.nom                         = 1.125;
runParam.b1.unit                        = 'cm';
runParam.b1.var                         = []; %0:0.025:0.1;

runParam.i1.nom                         = 1.2;
runParam.i1.unit                        = 'cm';
runParam.i1.var                         = [];

runParam.mesh.nom                       = 0.01;
runParam.mesh.unit                      = 'cm';
runParam.mesh.var                       = []; %0.0:0.02:0.1;

% Frequency of the mode
runParam.freq.nom                       = 5712.0;
runParam.freq.unit                      = 'MHz';
runParam.freq.var                       = []; %10:10:50;

% Perform frequency scan
runParam.freqScan.nom                   = false;
runParam.freqScan.unit                  = 'bool';
runParam.freqScan.var                   = [];

runParam.freqScanStartFreq.nom          = 5662.0;
runParam.freqScanStartFreq.unit         = 'MHz';
runParam.freqScanStartFreq.var          = [];

runParam.freqScanStepFreq.nom           = 1.0;
runParam.freqScanStepFreq.unit          = 'MHz';
runParam.freqScanStepFreq.var           = [];

runParam.freqScanStepNo.nom             = 101;
runParam.freqScanStepNo.unit            = 'int';
runParam.freqScanStepNo.var             = [];


% Step size [cm] for the output fieldmap
runParam.sf7.stepSize                   = 0.1;


%% POSTPROCESSING
runParam.postprocess.start = false;


%% OPTIMIZATION
runParam.optimize.start                 = false;


%% Figure-of-merit definitions


%%
run_start(runParam);
