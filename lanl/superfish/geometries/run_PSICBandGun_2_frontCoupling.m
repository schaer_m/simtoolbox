%%%%% SCRIPT run_PSICBandGun_2_frontCoupling.m %%%%%
% Mattia Schaer
% March 2014
%
% Run multiple SuperFish simulations of a C-band gun with a coaxial coupler
% on the front
%
%%%%%



clearvars -global runParam;
global runParam;


% Force user to check parameters before starting simulations
runParam.forceParamCheck = true;

% Working on which machine?
runParam.machineName = 'localWin';
%runParam.machineName = 'laptopLinux';

% Simulation type
runParam.simType = 'lanl';
% Specific procedure
runParam.procedure = 'cfish';
%runParam.procedure = 'irisOptim';

% Geometry function
runParam.geometryFunction = 'PSICBandGun_2_frontCoupling';

% Prefix for this run
runParam.folderPrefix = 'trial';


%% Geometry related parameters

runParam.cellsNum.nom                   = 6;
runParam.cellsNum.unit                  = 'int';
runParam.cellsNum.var                   = [];

runParam.xCathode.nom                   = 0.0;
runParam.xCathode.unit                  = 'cm';
runParam.xCathode.var                   = [];

runParam.rEdge.nom                      = 0.5;
runParam.rEdge.unit                     = 'cm';
runParam.rEdge.var                      = [];

runParam.lFlat.nom                      = 0.0500;
runParam.lFlat.unit                     = 'cm';
runParam.lFlat.var                      = [];

runParam.r1.nom                         = 2.2575;
runParam.r1.unit                        = 'cm';
runParam.r1.var                         = []; %0.0:0.0005:0.003;
runParam.r1.opt                         = [2.2575 2.2570 2.2580 0.0001];

runParam.l1.nom                         = 1.5745;
runParam.l1.unit                        = 'cm';
runParam.l1.var                         = []; %-0.02:0.01:0.02;

runParam.a1.nom                         = 0.5;
runParam.a1.unit                        = 'cm';
runParam.a1.var                         = [];

runParam.b1.nom                         = 0.7;
runParam.b1.unit                        = 'cm';
runParam.b1.var                         = []; %-0.1:0.025:0.0;

runParam.ir1.nom                         = 1.0;
runParam.ir1.unit                        = 'cm';
runParam.ir1.var                         = [];

runParam.bypassNormalCellParameters.nom = false;
runParam.bypassNormalCellParameters.unit= 'bool';
runParam.bypassNormalCellParameters.var = [];

runParam.r2.nom                         = 2.3326;
runParam.r2.unit                        = 'cm';
runParam.r2.var                         = []; %-0.001:0.00025:0.0;
%runParam.r2.opt                         = [2.3326 2.3316 2.3336 0.0002];

runParam.l2.nom                         = 2.6242;
runParam.l2.unit                        = 'cm';
runParam.l2.var                         = [];

runParam.a2.nom                         = 0.5;
runParam.a2.unit                        = 'cm';
runParam.a2.var                         = [];

runParam.b2.nom                         = 0.7;
runParam.b2.unit                        = 'cm';
runParam.b2.var                         = [];

runParam.ir2.nom                         = 1.0;
runParam.ir2.unit                        = 'cm';
runParam.ir2.var                         = [];

runParam.r3.nom                         = [];
runParam.r3.unit                        = 'cm';
runParam.r3.var                         = [];

runParam.l3.nom                         = [];
runParam.l3.unit                        = 'cm';
runParam.l3.var                         = [];

runParam.a3.nom                         = [];
runParam.a3.unit                        = 'cm';
runParam.a3.var                         = [];

runParam.b3.nom                         = [];
runParam.b3.unit                        = 'cm';
runParam.b3.var                         = [];

runParam.ir3.nom                         = [];
runParam.ir3.unit                        = 'cm';
runParam.ir3.var                         = [];

runParam.r4.nom                         = [];
runParam.r4.unit                        = 'cm';
runParam.r4.var                         = [];

runParam.l4.nom                         = [];
runParam.l4.unit                        = 'cm';
runParam.l4.var                         = [];

runParam.a4.nom                         = [];
runParam.a4.unit                        = 'cm';
runParam.a4.var                         = [];

runParam.b4.nom                         = [];
runParam.b4.unit                        = 'cm';
runParam.b4.var                         = [];

runParam.ir4.nom                         = [];
runParam.ir4.unit                        = 'cm';
runParam.ir4.var                         = [];

runParam.r5.nom                         = [];
runParam.r5.unit                        = 'cm';
runParam.r5.var                         = [];

runParam.l5.nom                         = [];
runParam.l5.unit                        = 'cm';
runParam.l5.var                         = [];

runParam.a5.nom                         = [];
runParam.a5.unit                        = 'cm';
runParam.a5.var                         = [];

runParam.b5.nom                         = [];
runParam.b5.unit                        = 'cm';
runParam.b5.var                         = [];

runParam.ir5.nom                         = [];
runParam.ir5.unit                        = 'cm';
runParam.ir5.var                         = [];

runParam.r6.nom                         = 2.3195;
runParam.r6.unit                        = 'cm';
runParam.r6.var                         = []; %0.0:0.0005:0.002;
runParam.r6.opt                         = [2.3195 2.3190 2.3200 0.0001];

runParam.l6.nom                         = [];
runParam.l6.unit                        = 'cm';
runParam.l6.var                         = [];

runParam.a6.nom                         = [];
runParam.a6.unit                        = 'cm';
runParam.a6.var                         = [];

runParam.b6.nom                         = [];
runParam.b6.unit                        = 'cm';
runParam.b6.var                         = [];

runParam.ir6.nom                         = [];
runParam.ir6.unit                        = 'cm';
runParam.ir6.var                         = [];

runParam.rCoupler.nom                   = 0.2;
runParam.rCoupler.unit                  = 'cm';
runParam.rCoupler.var                   = [];

runParam.thetaCoupler.nom               = 45.0;
runParam.thetaCoupler.unit              = 'deg';
runParam.thetaCoupler.var               = [];

runParam.rCouplerEdge.nom               = 0.1;
runParam.rCouplerEdge.unit              = 'cm';
runParam.rCouplerEdge.var               = [];

runParam.rInnerConductor.nom            = 0.6;
runParam.rInnerConductor.unit           = 'cm';
runParam.rInnerConductor.var            = [];

runParam.innerConductorThickness.nom    = 0.2;
runParam.innerConductorThickness.unit   = 'cm';
runParam.innerConductorThickness.var    = [];

runParam.rOuterConductor.nom            = 1.4;
runParam.rOuterConductor.unit           = 'cm';
runParam.rOuterConductor.var            = [];

runParam.lCoupling.nom                  = 0.5;
runParam.lCoupling.unit                 = 'cm';
runParam.lCoupling.var                  = [];

runParam.lAbsorber.nom                  = 3.0;
runParam.lAbsorber.unit                 = 'cm';
runParam.lAbsorber.var                  = [];

runParam.lTube.nom                      = 8.0;
runParam.lTube.unit                     = 'cm';
runParam.lTube.var                      = [];

runParam.mesh.nom                       = 0.03;
runParam.mesh.unit                      = 'cm';
runParam.mesh.var                       = []; %0.0:0.02:0.1;

% Frequency of the mode
runParam.freq.nom                       = 5715.0;
runParam.freq.unit                      = 'MHz';
runParam.freq.var                       = []; %10:10:50;

% Perform frequency scan
runParam.freqScan.nom                   = false;
runParam.freqScan.unit                  = 'bool';
runParam.freqScan.var                   = [];

runParam.freqScanStartFreq.nom          = 5662.0;
runParam.freqScanStartFreq.unit         = 'MHz';
runParam.freqScanStartFreq.var          = [];

runParam.freqScanStepFreq.nom           = 1.0;
runParam.freqScanStepFreq.unit          = 'MHz';
runParam.freqScanStepFreq.var           = [];

runParam.freqScanStepNo.nom             = 101;
runParam.freqScanStepNo.unit            = 'int';
runParam.freqScanStepNo.var             = [];


% Step size [cm] for the output fieldmap
runParam.sf7.stepSize                   = 0.1;


%% POSTPROCESSING
runParam.postprocess = false;


%% OPTIMIZATION
runParam.optimize.start                 = false;

%runParam.optimize.algorithm = 'NLOPT_LN_NELDERMEAD';
runParam.optimize.algorithm = 'NLOPT_LN_SBPLX';
%runParam.optimize.algorithm = 'NLOPT_GN_DIRECT_L';

% Figure-of-merit to use for the optimization
runParam.optimize.fomToOpt  = 1;

% max number of simulations
runParam.optimize.MaxFunEval = 20;


%% Figure-of-merit definition

runParam.fomDef{1}.name                     = 'fieldBalance';
% absolute tolerance on this fom to end optimization
runParam.fomDef{1}.fomTol                   = 0.005;


%%
run_start(runParam);
