%%%%% SCRIPT run_CBandSWBooster.m %%%%%
% Mattia Schaer
% November 2014
%
% Run multiple SuperFish simulations of a C-band SW accelerating structure
%
%%%%%



clearvars -global runParam;
global runParam;


% Force user to check parameters before starting simulations
runParam.forceParamCheck = true;

% Working on which machine?
runParam.machineName = 'localWin';
%runParam.machineName = 'laptopLinux';

% Simulation type
runParam.simType = 'lanl';
% Specific procedure
runParam.procedure = 'autofish';
%runParam.procedure = 'irisOptim';

% Geometry function
runParam.geometryFunction = 'CBandSWBooster';

% Prefix for this run
runParam.folderPrefix = '3cellForVelocityBunching';


%% Geometry related parameters

runParam.startTube.nom                  = 5.0;
runParam.startTube.unit                 = 'cm';
runParam.startTube.var                  = [];

runParam.bypassEndTube.nom              = true;
runParam.bypassEndTube.unit             = 'bool';
runParam.bypassEndTube.var              = [];

runParam.endTube.nom                    = 5.0;
runParam.endTube.unit                   = 'cm';
runParam.endTube.var                    = [];

runParam.cellsNum.nom                   = 3;
runParam.cellsNum.unit                  = 'int';
runParam.cellsNum.var                   = [];

runParam.rEdge.nom                      = 0.1;
runParam.rEdge.unit                     = 'cm';
runParam.rEdge.var                      = [];

runParam.lFlat.nom                      = 0.0100;
runParam.lFlat.unit                     = 'cm';
runParam.lFlat.var                      = [];

runParam.r1.nom                         = 2.4463;
runParam.r1.unit                        = 'cm';
runParam.r1.var                         = []; %0.000:0.0002:0.001;
runParam.r1.opt                         = [2.2575 2.2570 2.2580 0.0001];

runParam.l1.nom                         = 2.6242;
runParam.l1.unit                        = 'cm';
runParam.l1.var                         = []; %-0.02:0.01:0.02;

runParam.a1.nom                         = 0.7500;
runParam.a1.unit                        = 'cm';
runParam.a1.var                         = [];

runParam.b1.nom                         = 1.1250;
runParam.b1.unit                        = 'cm';
runParam.b1.var                         = []; %-0.1:0.025:0.0;

runParam.ir1.nom                         = 1.2;
runParam.ir1.unit                        = 'cm';
runParam.ir1.var                         = [];

runParam.r2.nom                         = 2.4708;
runParam.r2.unit                        = 'cm';
runParam.r2.var                         = []; %-0.003:0.001:0.000;
%runParam.r2.opt                         = [2.3326 2.3316 2.3336 0.0002];

runParam.l2.nom                         = 2.6242;
runParam.l2.unit                        = 'cm';
runParam.l2.var                         = [];

runParam.a2.nom                         = 0.7500;
runParam.a2.unit                        = 'cm';
runParam.a2.var                         = [];

runParam.b2.nom                         = 1.1250;
runParam.b2.unit                        = 'cm';
runParam.b2.var                         = [];

runParam.ir2.nom                         = 1.2;
runParam.ir2.unit                        = 'cm';
runParam.ir2.var                         = [];

runParam.r3.nom                         = 2.4463;
runParam.r3.unit                        = 'cm';
runParam.r3.var                         = []; %0.0:0.0002:0.001;

runParam.l3.nom                         = 2.6242;
runParam.l3.unit                        = 'cm';
runParam.l3.var                         = [];

runParam.a3.nom                         = 0.75;
runParam.a3.unit                        = 'cm';
runParam.a3.var                         = [];

runParam.b3.nom                         = 1.1250;
runParam.b3.unit                        = 'cm';
runParam.b3.var                         = [];

runParam.ir3.nom                         = 1.2;
runParam.ir3.unit                        = 'cm';
runParam.ir3.var                         = [];

runParam.mesh.nom                       = 0.019;
runParam.mesh.unit                      = 'cm';
runParam.mesh.var                       = []; %0.0:0.02:0.1;

% Frequency of the mode
runParam.freq.nom                       = 5712.0;
runParam.freq.unit                      = 'MHz';
runParam.freq.var                       = []; %10:10:50;

% Perform frequency scan
runParam.freqScan.nom                   = false;
runParam.freqScan.unit                  = 'bool';
runParam.freqScan.var                   = [];

runParam.freqScanStartFreq.nom          = 5682.0;
runParam.freqScanStartFreq.unit         = 'MHz';
runParam.freqScanStartFreq.var          = [];

runParam.freqScanStepFreq.nom           = 1.0;
runParam.freqScanStepFreq.unit          = 'MHz';
runParam.freqScanStepFreq.var           = [];

runParam.freqScanStepNo.nom             = 36;
runParam.freqScanStepNo.unit            = 'int';
runParam.freqScanStepNo.var             = [];


% Step size [cm] for the output fieldmap
runParam.sf7.stepSize                   = 0.1;


%% POSTPROCESSING
runParam.postprocess.start = false;


%% OPTIMIZER
runParam.optimize.start                 = false;

%runParam.optimize.algorithm = 'NLOPT_LN_NELDERMEAD';
runParam.optimize.algorithm = 'NLOPT_LN_SBPLX';
%runParam.optimize.algorithm = 'NLOPT_GN_DIRECT_L';

% Figure-of-merit to use for the optimization
runParam.optimize.fomToOpt  = 1;

% max number of simulations
runParam.optimize.MaxFunEval = 20;


% Figure-of-merit definition

runParam.fomDef{1}.name                     = 'fieldBalance';
% absolute tolerance on this fom to end optimization
runParam.fomDef{1}.fomTol                   = 0.005;


%% Start run
run_start(runParam);
