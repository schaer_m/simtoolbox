%%%%% SCRIPT run_CBandOvermoded.m %%%%%
% Mattia Schaer
% January 2015
%
% Run multiple SuperFish simulations of the C-band overmoded "pill-box"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



clearvars -global runParam;
global runParam;


% Force user to check parameters before starting simulations
runParam.forceParamCheck = true;

% Working on which machine?
runParam.machineName = 'localWin';
%runParam.machineName = 'laptopLinux';

% Simulation type
runParam.simType = 'lanl';
% Specific procedure
runParam.procedure = 'autofish';
%runParam.procedure = 'irisOptim';

% Geometry function
runParam.geometryFunction = 'CBandOvermoded';

% Prefix for this run
%runParam.folderPrefix = '2p5cells';
runParam.folderPrefix = '3p5cells';


%% Geometry related parameters

runParam.cellsNum.nom                   = 4;
runParam.cellsNum.unit                  = 'int';
runParam.cellsNum.var                   = [];

runParam.xCathode.nom                   = 0.0;
runParam.xCathode.unit                  = 'cm';
runParam.xCathode.var                   = [];

%runParam.r1.nom                         = 6.7;   % For 2.5 cells
runParam.r1.nom                         = 11.32;   % For 3.5 cells
runParam.r1.unit                        = 'cm';
runParam.r1.var                         = [];
runParam.r1.opt                         = [];

%runParam.l1.nom                         = 5.5;   % For 2.5 cells
runParam.l1.nom                         = 8.0;   % For 3.5 cells
runParam.l1.unit                        = 'cm';
runParam.l1.var                         = [];
runParam.l1.opt                         = [];

%runParam.r2.nom                         = 6.68;   % For 2.5 cells
runParam.r2.nom                         = 11.32;   % For 3.5 cells
runParam.r2.unit                        = 'cm';
runParam.r2.var                         = []; %-0.1:0.02:0.1;
runParam.r2.opt                         = [];

runParam.a1.nom                         = 0.5;
runParam.a1.unit                        = 'cm';
runParam.a1.var                         = [];
runParam.a1.opt                         = [];

runParam.i1.nom                         = 0.5;
runParam.i1.unit                        = 'cm';
runParam.i1.var                         = [];
runParam.i1.opt                         = [];

runParam.lTube.nom                      = 5.0;
runParam.lTube.unit                     = 'cm';
runParam.lTube.var                      = [];
runParam.lTube.opt                      = [];

runParam.mesh.nom                       = 0.04;
runParam.mesh.unit                      = 'cm';
runParam.mesh.var                       = [];
runParam.mesh.opt                       = [];

% Frequency of the mode
runParam.freq.nom                       = 5712.0;
%runParam.freq.nom                       = 6000;
runParam.freq.unit                      = 'MHz';
runParam.freq.var                       = [];

% Perform frequency scan
runParam.freqScan.nom                   = false;
runParam.freqScan.unit                  = 'bool';
runParam.freqScan.var                   = [];

runParam.freqScanStartFreq.nom          = 5740.0;
runParam.freqScanStartFreq.unit         = 'MHz';
runParam.freqScanStartFreq.var          = [];

runParam.freqScanStepFreq.nom           = 2.0;
runParam.freqScanStepFreq.unit          = 'MHz';
runParam.freqScanStepFreq.var           = [];

runParam.freqScanStepNo.nom             = 26;
runParam.freqScanStepNo.unit            = 'int';
runParam.freqScanStepNo.var             = [];


% Step size [cm] for the output fieldmap
runParam.sf7.stepSize                   = 0.1;


%% POSTPROCESSING
runParam.postprocess.start = false;


%% OPTIMIZER
runParam.optimize.start = false;

%runParam.optimize.algorithm = 'NLOPT_LN_NELDERMEAD';
runParam.optimize.algorithm = 'NLOPT_LN_SBPLX';
%runParam.optimize.algorithm = 'NLOPT_GN_DIRECT_L';

% Figure-of-merit to use for the optimization
runParam.optimize.fomToOpt  = 1;

% max number of simulations
runParam.optimize.MaxFunEval = 20;


% Figure-of-merit definition

runParam.fomDef{1}.name                     = 'fieldBalance';
% absolute tolerance on this fom to end optimization
runParam.fomDef{1}.fomTol                   = 0.005;


%% Start run
run_start(runParam);
