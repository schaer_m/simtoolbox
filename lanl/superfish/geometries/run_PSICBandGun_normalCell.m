%%%%% SCRIPT run_PSICBandGun_normalCell.m %%%%%
% Mattia Schaer
% March 2014
%
% Run multiple SuperFish simulations of a normal cell for a C-band gun
% WITHOUT a flat edge at the end of the iris curvature
%
%%%%%



clearvars -global runParam;


% Force user to check parameters before starting simulations
runParam.forceParamCheck = true;

% Working on which machine?
runParam.machineName = 'localWin';

% Simulation type
runParam.simType = 'lanl';
% Specific procedure
runParam.procedure = 'irisOptim';

% Geometry function
runParam.geometryFunction = 'PSICBandGun_normalCell';

% Prefix for this run
runParam.folderPrefix = 'ellipticIris';


%% SuperFish input parameters

runParam.cellsNum.nom                   = 1;
runParam.cellsNum.unit                  = 'int';
runParam.cellsNum.var                   = [];

runParam.xCathode.nom                   = 0.0;
runParam.xCathode.unit                  = 'cm';
runParam.xCathode.var                   = [];

runParam.r1.nom                         = 2.2863;
runParam.r1.unit                        = 'cm';
runParam.r1.var                         = []; %-0.01:0.005:0.01;

runParam.l1.nom                         = 2.5558;
runParam.l1.unit                        = 'cm';
runParam.l1.var                         = []; %-0.01:0.001:0.01;

runParam.a1.nom                         = 0.5;
runParam.a1.unit                        = 'cm';
runParam.a1.var                         = 0:0.05:0.3;

runParam.b1.nom                         = 1.0;
runParam.b1.unit                        = 'cm';
runParam.b1.var                         = 0:0.05:0.25;

runParam.i1.nom                         = 1.0;
runParam.i1.unit                        = 'cm';
runParam.i1.var                         = [];

runParam.mesh.nom                       = 0.02;
runParam.mesh.unit                      = 'cm';
runParam.mesh.var                       = []; %0.0:0.02:0.1;

% Frequency of the mode
runParam.freq.nom                       = 5712.0;
runParam.freq.unit                      = 'MHz';
runParam.freq.var                       = []; %10:10:50;

% Perform frequency scan
runParam.freqScan.nom                   = false;
runParam.freqScan.unit                  = 'bool';
runParam.freqScan.var                   = [];

runParam.freqScanStartFreq.nom          = 5662.0;
runParam.freqScanStartFreq.unit         = 'MHz';
runParam.freqScanStartFreq.var          = [];

runParam.freqScanStepFreq.nom           = 1.0;
runParam.freqScanStepFreq.unit          = 'MHz';
runParam.freqScanStepFreq.var           = [];

runParam.freqScanStepNo.nom             = 101;
runParam.freqScanStepNo.unit            = 'int';
runParam.freqScanStepNo.var             = [];


% Step size [cm] for the output fieldmap
runParam.sf7.stepSize                   = 0.1;


%% Do postprocessing when looping?
runParam.postprocess = false;


%% No optimizer implemented yet
runParam.optimize.start                 = false;


%% Start run
run_start(runParam);
