%%%%% SCRIPT run_PSICBandGun_normalCell_2.m %%%%%
% Mattia Schaer
% March 2014
%
% Run multiple SuperFish simulations of a normal cell for a C-band gun
%
%%%%%



clearvars -global runParam;
global runParam;


% Force user to check parameters before starting simulations
runParam.forceParamCheck = true;

% Working on which machine?
runParam.machineName = 'localWin';
%runParam.machineName = 'laptopLinux';

% Simulation type
runParam.simType = 'lanl';
% Specific procedure
%runParam.procedure = 'autofish';
runParam.procedure = 'irisOptim';
% Estimation of df/dr
runParam.dfOVERdr = -2650;   % [MHz/cm]

% Geometry function
runParam.geometryFunction = 'PSICBandGun_lastCell_2';

% Prefix for this run
runParam.folderPrefix = 'rEdge1mm-i1p2mm-beta0p75';


%% SuperFish input parameters

runParam.cellsNum.nom                   = 1;
runParam.cellsNum.unit                  = 'int';
runParam.cellsNum.var                   = [];

runParam.xCathode.nom                   = 0.0;
runParam.xCathode.unit                  = 'cm';
runParam.xCathode.var                   = [];

runParam.rEdge.nom                      = 0.1;
runParam.rEdge.unit                     = 'cm';
runParam.rEdge.var                      = [];

runParam.r1.nom                         = 2.3917;
runParam.r1.unit                        = 'cm';
runParam.r1.var                         = []; %-0.01:0.005:0.01;

runParam.lFlat.nom                      = 0.0100;
runParam.lFlat.unit                     = 'cm';
runParam.lFlat.var                      = [];

%runParam.l1.nom                         = 2.6242;   % beta = 1
runParam.l1.nom                         = 1.9681;   % beta = 0.75
runParam.l1.unit                        = 'cm';
runParam.l1.var                         = [];

% Left side

%runParam.a1.nom                         = 0.7500;
runParam.a1.nom                         = 0.55;
runParam.a1.unit                        = 'cm';
runParam.a1.var                         = 0:0.05:0.2;

runParam.b1.nom                         = 1.05;
runParam.b1.unit                        = 'cm';
runParam.b1.var                         = []; %0:0.05:0.3;

runParam.i1.nom                         = 1.2;
runParam.i1.unit                        = 'cm';
runParam.i1.var                         = [];

% Right side

runParam.bypassA.nom                    = false;
runParam.bypassA.unit                   = 'bool';
runParam.bypassA.var                    = [];

runParam.a2.nom                         = 0.3;
runParam.a2.unit                        = 'cm';
runParam.a2.var                         = 0:0.05:0.2;

runParam.b2.nom                         = 1.25;
runParam.b2.unit                        = 'cm';
runParam.b2.var                         = []; %0:0.05:0.3;

runParam.i2.nom                         = 1.0;
runParam.i2.unit                        = 'cm';
runParam.i2.var                         = [];

runParam.lTube.nom                      = 5.0;
runParam.lTube.unit                     = 'cm';
runParam.lTube.var                      = [];

runParam.mesh.nom                       = 0.015;
runParam.mesh.unit                      = 'cm';
runParam.mesh.var                       = []; %0.0:0.02:0.1;

% Frequency of the mode
runParam.freq.nom                       = 5712.0;
runParam.freq.unit                      = 'MHz';
runParam.freq.var                       = []; %10:10:50;

% Perform frequency scan
runParam.freqScan.nom                   = false;
runParam.freqScan.unit                  = 'bool';
runParam.freqScan.var                   = [];

runParam.freqScanStartFreq.nom          = 5662.0;
runParam.freqScanStartFreq.unit         = 'MHz';
runParam.freqScanStartFreq.var          = [];

runParam.freqScanStepFreq.nom           = 1.0;
runParam.freqScanStepFreq.unit          = 'MHz';
runParam.freqScanStepFreq.var           = [];

runParam.freqScanStepNo.nom             = 101;
runParam.freqScanStepNo.unit            = 'int';
runParam.freqScanStepNo.var             = [];


% Step size [cm] for the output fieldmap
runParam.sf7.stepSize                   = 0.1;


%% POSTPROCESSING
runParam.postprocess.start = false;


%% OPTIMIZER
runParam.optimize.start                 = false;


%% Start run
run_start(runParam);
