%%%%% SCRIPT run_PSICBandGun_halfCell_2.m %%%%%
% Mattia Schaer
% March 2014
%
% Run multiple SuperFish simulations of a half cell for a C-band gun
%
%%%%%



clearvars -global runParam;
global runParam;


% Force user to check parameters before starting simulations
runParam.forceParamCheck = true;

% Working on which machine?
runParam.machineName = 'localWin';

% Simulation type
runParam.simType = 'lanl';
% Specific procedure
%runParam.procedure = 'autofish';
runParam.procedure = 'irisOptim';
% Estimation of df/dr
runParam.dfOVERdr = -2650;   % [MHz/cm]

% Geometry function
runParam.geometryFunction = 'PSICBandGun_halfCell_2';

% Prefix for this run
runParam.folderPrefix = 'rEdge1mm-i1p2mm-optimIris';


%% SuperFish input parameters

runParam.cellsNum.nom                   = 1;
runParam.cellsNum.unit                  = 'int';
runParam.cellsNum.var                   = [];

runParam.xCathode.nom                   = 0.0;
runParam.xCathode.unit                  = 'cm';
runParam.xCathode.var                   = [];

runParam.rEdge.nom                      = 0.1;
runParam.rEdge.unit                     = 'cm';
runParam.rEdge.var                      = [];

runParam.r1.nom                         = 2.3482;
runParam.r1.unit                        = 'cm';
runParam.r1.var                         = []; %-0.01:0.005:0.01;

runParam.lFlat.nom                      = 0.0100;
runParam.lFlat.unit                     = 'cm';
runParam.lFlat.var                      = [];

%runParam.l1.nom                         = 2.6242;   % 1.0, normal cell
%runParam.l1.nom                         = 1.8369;   % 0.7
runParam.l1.nom                         = 1.5745;   % 0.6
%runParam.l1.nom                         = 1.3121;   % 0.5
runParam.l1.unit                        = 'cm';
runParam.l1.var                         = [];

runParam.a1.nom                         = 0.7500;
runParam.a1.unit                        = 'cm';
runParam.a1.var                         = []; %0:0.1:0.3;

runParam.b1.nom                         = 1.0000;
runParam.b1.unit                        = 'cm';
runParam.b1.var                         = []; %0:0.1:0.3;

runParam.i1.nom                         = 1.2;
runParam.i1.unit                        = 'cm';
runParam.i1.var                         = [];

runParam.mesh.nom                       = 0.01;
runParam.mesh.unit                      = 'cm';
runParam.mesh.var                       = []; %0.0:0.02:0.1;

% Frequency of the mode
runParam.freq.nom                       = 5712.0;
runParam.freq.unit                      = 'MHz';
runParam.freq.var                       = []; %10:10:50;

% Perform frequency scan
runParam.freqScan.nom                   = false;
runParam.freqScan.unit                  = 'bool';
runParam.freqScan.var                   = [];

runParam.freqScanStartFreq.nom          = 5662.0;
runParam.freqScanStartFreq.unit         = 'MHz';
runParam.freqScanStartFreq.var          = [];

runParam.freqScanStepFreq.nom           = 1.0;
runParam.freqScanStepFreq.unit          = 'MHz';
runParam.freqScanStepFreq.var           = [];

runParam.freqScanStepNo.nom             = 101;
runParam.freqScanStepNo.unit            = 'int';
runParam.freqScanStepNo.var             = [];


% Step size [cm] for the output fieldmap
runParam.sf7.stepSize                   = 0.1;


%% POSTPROCESSING
runParam.postprocess = false;


%% OPTIMIZER
runParam.optimize.start                 = false;


%% Start run
run_start(runParam);
