%%%%% SCRIPT run_CTF2Gun5_withPlug_simple.m %%%%%
% Mattia Schaer
% November 2013
%
% Run multiple SuperFish simulations of the CTF2 Gun 5 with cathode plug
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clearvars -global runParam;
global runParam;


% Force user to check parameters before starting simulations
runParam.forceParamCheck = true;

% Working on which machine?
runParam.machineName = 'localWin';

% Solver
runParam.solver = 'autofish';

% Geometry function
runParam.geometryFunction = 'CTF2Gun5_withPlug_simple';

% Prefix for this run
runParam.folderPrefix = 'beadpullMatch';


% Geometry related parameters

runParam.cellsNum.nom       = 3;
runParam.cellsNum.unit      = 'int';
runParam.cellsNum.var       = [];

runParam.meshSizeX.nom      = 0.025;
runParam.meshSizeX.unit     = 'cm';
runParam.meshSizeX.var      = []; %[0.0 0.005 0.015 0.025];

runParam.meshSizeY.nom      = 0.025;
runParam.meshSizeY.unit     = 'cm';
runParam.meshSizeY.var      = [];

runParam.lineRegions.nom    = false;
runParam.lineRegions.unit   = 'bool';
runParam.lineRegions.var    = [];

runParam.xCathode.nom       = 0.0;
runParam.xCathode.unit      = 'cm';
runParam.xCathode.var       = -0.0500:0.0500:0.0500;

runParam.l1.nom             = 3.2;
runParam.l1.unit            = 'cm';
runParam.l1.var             = [];

runParam.hHalf.nom          = 9.7307; % FIELD BALANCE 9.7246; % ORIGINAL (Mafia) 9.7345;
runParam.hHalf.unit         = 'cm';
runParam.hHalf.var          = []; %-0.0005:0.0005:0.0005;

runParam.rHalf.nom          = 1.8000;
runParam.rHalf.unit         = 'cm';
runParam.rHalf.var          = [];

runParam.l2.nom             = 4.6;
runParam.l2.unit            = 'cm';
runParam.l2.var             = [];

runParam.hSecond.nom        = 4.4750; % FIELD BALANCE 4.4745;
runParam.hSecond.unit       = 'cm';
runParam.hSecond.var        = []; %-0.0005:0.0005:0.0005;

runParam.rSecond.nom        = 0.9;
runParam.rSecond.unit       = 'cm';
runParam.rSecond.var        = [];

runParam.l3.nom             = 4.7;
runParam.l3.unit            = 'cm';
runParam.l3.var             = [];

runParam.hThird.nom         = 4.4765; % FIELD BALANCE 4.4721;
runParam.hThird.unit        = 'cm';
runParam.hThird.var         = []; %-0.0005:0.0005:0.0005;

runParam.rThird.nom         = 0.8;
runParam.rThird.unit        = 'cm';
runParam.rThird.var         = [];

runParam.xEnd.nom           = 17.5;
runParam.xEnd.unit          = 'cm';
runParam.xEnd.var           = [];


% Step size [cm] for the output fieldmap
runParam.sf7.stepSize = 0.1;


% No optimizer implemented yet
runParam.optimize.start = false;


lanl_run();
