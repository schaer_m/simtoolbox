%%%%% SCRIPT run_PSICBandGun_3andHalfCells.m %%%%%
% Mattia Schaer
% July 2013
%
% Run multiple SuperFish simulations of a C-band gun 1 3 and half cells
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clearvars -global runParam;
global runParam;


% Force user to check parameters before starting simulations
runParam.forceParamCheck = true;

% Working on which machine?
runParam.machineName = 'localWin';

% Solver
runParam.solver = 'autofish';

% Geometry function
runParam.geometryFunction = 'PSICBandGun_3andHalfCells';

% Prefix for this run
runParam.folderPrefix = 'design1';


% Geometry related parameters

runParam.cellsNum.nom   = 4;
runParam.cellsNum.unit  = 'int';
runParam.cellsNum.var   = [];

runParam.xCathode.nom     = 0.0;
runParam.xCathode.unit    = 'cm';
runParam.xCathode.var     = [];

runParam.hHalf.nom     = 2.1676;
runParam.hHalf.unit    = 'cm';
runParam.hHalf.var     = []; %-0.0003:0.0001:-0.0001;

runParam.l1.nom     = 1.6008;
runParam.l1.unit    = 'cm';
runParam.l1.var     = []; %0.0:0.05:0.1;

runParam.a1.nom     = 0.5248;
runParam.a1.unit    = 'cm';
runParam.a1.var     = [];

runParam.b1.nom     = 0.8922;
runParam.b1.unit    = 'cm';
runParam.b1.var     = [];

runParam.i1.nom     = 0.8398;
runParam.i1.unit    = 'cm';
runParam.i1.var     = [];

runParam.r2.nom     = 2.2238;
runParam.r2.unit    = 'cm';
runParam.r2.var     = []; %-0.005:0.001:0.0;

runParam.l2.nom     = 2.5558;
runParam.l2.unit    = 'cm';
runParam.l2.var     = [];

runParam.a2.nom     = 0.5248;
runParam.a2.unit    = 'cm';
runParam.a2.var     = [];

runParam.b2.nom     = 0.8922;
runParam.b2.unit    = 'cm';
runParam.b2.var     = [];

runParam.i2.nom     = 0.8398;
runParam.i2.unit    = 'cm';
runParam.i2.var     = [];

runParam.r3.nom     = 2.225;
runParam.r3.unit    = 'cm';
runParam.r3.var     = []; %0.0:0.0002:0.001;

runParam.l3.nom     = 2.5558;
runParam.l3.unit    = 'cm';
runParam.l3.var     = [];

runParam.a3.nom     = 0.5248;
runParam.a3.unit    = 'cm';
runParam.a3.var     = [];

runParam.b3.nom     = 0.8922;
runParam.b3.unit    = 'cm';
runParam.b3.var     = [];

runParam.i3.nom     = 0.8398;
runParam.i3.unit    = 'cm';
runParam.i3.var     = [];

runParam.r4.nom     = 2.2342;
runParam.r4.unit    = 'cm';
runParam.r4.var     = [];%-0.0001:0.0001:0.0001;

runParam.l4.nom     = 2.4143;
runParam.l4.unit    = 'cm';
runParam.l4.var     = [];

runParam.a4.nom     = 0.5248;
runParam.a4.unit    = 'cm';
runParam.a4.var     = [];

runParam.b4.nom     = 0.8922;
runParam.b4.unit    = 'cm';
runParam.b4.var     = [];

runParam.i4.nom     = 0.8398;
runParam.i4.unit    = 'cm';
runParam.i4.var     = [];

runParam.xEnd.nom   = 15.0;
runParam.xEnd.unit  = 'cm';
runParam.xEnd.var   = [];

runParam.mesh.nom   = 0.02;
runParam.mesh.unit  = 'cm';
runParam.mesh.var   = []; %0.0:0.02:0.1;

% Frequency of the mode
runParam.freq.nom   = 5712.0;
runParam.freq.unit  = 'MHz';
runParam.freq.var   = [];

% Perform frequency scan
runParam.freqScan.nom       = false;
runParam.freqScan.unit      = 'bool';
runParam.freqScan.var       = [];

runParam.freqScanStartFreq.nom      = 5682.0;
runParam.freqScanStartFreq.unit     = 'MHz';
runParam.freqScanStartFreq.var      = [];

runParam.freqScanStepFreq.nom       = 1.0;
runParam.freqScanStepFreq.unit      = 'MHz';
runParam.freqScanStepFreq.var       = [];

runParam.freqScanStepNo.nom         = 41;
runParam.freqScanStepNo.unit        = 'int';
runParam.freqScanStepNo.var         = [];


% Step size [cm] for the output fieldmap
runParam.sf7.stepSize = 0.1;


% No optimizer implemented yet
runParam.optimize.start = false;


lanl_run();
