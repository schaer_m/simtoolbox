%%%%% SCRIPT run_CTF2Gun5.m %%%%%
% Mattia Schaer
% November 2013
%
% Run multiple SuperFish simulations of the CTF2 Gun 5
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clearvars -global runParam;
global runParam;


% Force user to check parameters before starting simulations
runParam.forceParamCheck = true;

% Working on which machine?
runParam.machineName = 'localLinux';

% Solver
runParam.solver = 'autofish';

% Geometry function
runParam.geometryFunction = 'CTF2Gun5';

% Prefix for this run
runParam.folderPrefix = 'fieldBalance';


% Geometry related parameters

runParam.cellsNum.nom       = 3;
runParam.cellsNum.unit      = 'int';
runParam.cellsNum.var       = [];

runParam.meshSizeX.nom      = 0.05;
runParam.meshSizeX.unit     = 'cm';
runParam.meshSizeX.var      = [];

runParam.xCathode.nom       = 0.0;
runParam.xCathode.unit      = 'cm';
runParam.xCathode.var       = [];

runParam.l1.nom             = 3.2;
runParam.l1.unit            = 'cm';
runParam.l1.var             = [];

runParam.hHalf.nom          = 9.7250; % ORIGINAL (Mafia) 9.7345;
runParam.hHalf.unit         = 'cm';
runParam.hHalf.var          = []; %-0.0060:0.0005:-0.0040;

runParam.rHalf.nom          = 1.8000;
runParam.rHalf.unit         = 'cm';
runParam.rHalf.var          = [];

runParam.l2.nom             = 4.6;
runParam.l2.unit            = 'cm';
runParam.l2.var             = [];

runParam.hSecond.nom        = 4.4775;
runParam.hSecond.unit       = 'cm';
runParam.hSecond.var        = []; %-0.0030:0.0005:-0.0020;

runParam.rSecond.nom        = 0.9;
runParam.rSecond.unit       = 'cm';
runParam.rSecond.var        = [];

runParam.l3.nom             = 4.7;
runParam.l3.unit            = 'cm';
runParam.l3.var             = [];

runParam.hThird.nom         = 4.4710;
runParam.hThird.unit        = 'cm';
runParam.hThird.var         = []; %0.0045:0.0005:0.0055;

runParam.rThird.nom         = 0.8;
runParam.rThird.unit        = 'cm';
runParam.rThird.var         = [];

runParam.xEnd.nom           = 17.5;
runParam.xEnd.unit          = 'cm';
runParam.xEnd.var           = [];


% Step size [cm] for the output fieldmap
runParam.sf7.stepSize = 0.1;


% No optimizer implemented yet
runParam.optimize.start = false;


lanl_run();
