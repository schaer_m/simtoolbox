%%%%% FUNCTION read_run_logfile.m %%%%%
% Mattia Schaer
% January 2013
%
% Read the run logfile and recover the parameters structure and the
% simulation list
%
% CALL [runId, param, simulationList] = read_run_logfile(logfilePath)
%           logfilePath:    string
%%%%%%%%%%%%


function [runId, param, simulationList] = read_run_logfile(logfilePath)

    % Open file
    inFile = fopen(logfilePath, 'r');
    
    % values are after
    separator = ':';
    
    % Run ID
    tmpLine = fgetl(inFile);
    index = strfind(tmpLine, separator);
    runId = tmpLine(index+2:end);
    % eat blank line
    tmpLine = fgetl(inFile);
    
    % Parameters
    paramIndex = 1;
    while true
        % name
        tmpLine = fgetl(inFile);
        % check if we got to the end of the parametrs definition
        if ~isempty(strfind(tmpLine, 'Iteration #'))
            break;
        end
        index = strfind(tmpLine, separator);
        param(paramIndex).name = tmpLine(index+2:end);
        % active
        tmpLine = fgetl(inFile);
        index = strfind(tmpLine, separator);
        if ~isempty(strfind(tmpLine(index+2:end), 'dynamic'))
            param(paramIndex).active = true;
        else
            param(paramIndex).active = false;
        end
        % parameter values
        tmpLine = fgetl(inFile);
        index = strfind(tmpLine, separator);
        param(paramIndex).values = sscanf(char(tmpLine(index+2:end)), '%f');
        % unit
        tmpLine = fgetl(inFile);
        index1 = strfind(tmpLine, '[');
        index2 = strfind(tmpLine, ']');
        param(paramIndex).unit = tmpLine(index1+1:index2-1);
        % eat blank line
        tmpLine = fgetl(inFile);
        % increment parameter index
        paramIndex = paramIndex + 1;
    end
    
    % Simulation list
    % determine dynamic parameters
    activeParam = find([param(:).active]);
    % number of possible combinations (i.e. number of simulations for this run)
    combinationNo = 1;
    for ii = activeParam
        combinationNo = combinationNo * length(param(ii).values);
    end
    simulationList = fscanf(inFile, '%f', [length(param)+1 combinationNo]);
    simulationList = simulationList(2:end, :)';
end
