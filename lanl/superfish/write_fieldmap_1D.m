%%%%% FUNCTION write_fieldmap_1D.m %%%%%
% Mattia Schaer
% March 2014
%
% From the output data of SF7 export 1D on-axis fieldmaps to use with ASTRA
% and OPAL
%
% CALL write_fieldmap_1D(sf7Input, sf7Type, outOptions, [freq])
%           sf7Input                string: sf7Filepath OR
%                                   struct: sf7Data
%           sf7Type                 string
%               'superfish' OR
%               'poisson'
%           outOptions              struct
%               .zStep                  float
%               .filepath               string
%               [.cutThreshold]         float           default: 1E-4
%               [.shift]                bool            default: false
%           [freq]                  float
%               --> if sf7Type='superfish'
%
%%%%%



function write_fieldmap_1D(sf7Input, sf7Type, outOptions, varargin)


switch sf7Type
    case 'superfish'
        fieldComponent = 'EzNorm';
        freq = varargin{1};
        headerOPAL = 'AstraDynamic 40';
        % default cutting threshold
        if ~isfield(outOptions, 'cutThreshold')
            outOptions.cutThreshold = 0.0;
        end
    case 'poisson'
        fieldComponent = 'BzNorm';
        headerOPAL = 'AstraMagnetostatic 40';
        % defaults
        if ~isfield(outOptions, 'cutThreshold')
            outOptions.cutThreshold = 1E-4;
        end
        if ~isfield(outOptions, 'shift')
            outOptions.shift = false;
        end
end

% Open SF7 file if only path is specified
if ischar(sf7Input)
    sf7Data = read_sf7_outputfile(sf7Input, sf7Type);
elseif isstruct(sf7Input)
    sf7Data = sf7Input;
end

% Define steps of the fieldmap
z = sf7Data.z(1)/100:outOptions.zStep:sf7Data.z(end)/100;   % [m]
% Compute corresponding field values
fieldNorm = spline(sf7Data.z/100, sf7Data.(fieldComponent), z);

% Cut fieldmap to relevant range
switch sf7Type
    case 'superfish'
        cutStartInd = 1;
        cutEndInd   = find(abs(fieldNorm) > outOptions.cutThreshold, 1, 'last');
    case 'poisson'
        cutStartInd = find(abs(fieldNorm) > outOptions.cutThreshold, 1, 'first');
        cutEndInd   = find(abs(fieldNorm) > outOptions.cutThreshold, 1, 'last');
        % Shift fieldmap
        if outOptions.shift
            ind = find(fieldNorm(1:end-1).*fieldNorm(2:end) < 0, 1, 'first');
            zZeroField = interp1(fieldNorm(ind:ind+1), z(ind:ind+1), 0.0);
            z = z - zZeroField;
            fprintf('Fieldmap shifted by deltaZ = %f m to have vanishing field at Z = 0\n', -zZeroField);
        end
end

% Create ASTRA fieldmap
foutASTRA = fopen([outOptions.filepath '.astra'], 'w');
fprintf(foutASTRA, '%20.10e %20.10e\n', [z(cutStartInd:cutEndInd) ; fieldNorm(cutStartInd:cutEndInd)]);
fclose(foutASTRA);

% Create OPAL fieldmap
foutOPAL = fopen([outOptions.filepath '.opal'], 'w');
fprintf(foutOPAL, [headerOPAL '\n']);
if strcmp(sf7Type, 'superfish')
    fprintf(foutOPAL, '%.3f\n', freq);
end
fprintf(foutOPAL, '%20.10e %20.10e\n', [z(cutStartInd:cutEndInd) ; fieldNorm(cutStartInd:cutEndInd)]);
fclose(foutOPAL);

end
