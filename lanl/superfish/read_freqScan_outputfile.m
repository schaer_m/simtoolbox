%%% FUNCTION read_freqScan_outputfile.m %%%
% Mattia Schaer
% July 2013
%
% Import data from the frequency scan output file of SuperFish
%
% CALL scanData = read_freqScan_outputfile(filepath)
%
%           fishData:   struct
%           filepath:   string
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function scanData = read_freqScan_outputfile(filepath)

    % Open file a first time to find the initial and final line of the data matrix
    inFile = fopen(filepath);
    currentLine = 0;
    matrixFound = false;
    while ~matrixFound
        tmpLine = fgetl(inFile);
        currentLine = currentLine + 1;
        % Look for the string determining the beginning of the matrix
        if strfind(tmpLine, 'Data') == 1
            firstLine = currentLine + 1;
        end
        % Look for the string determining the end of the matrix
        if strfind(tmpLine, 'EndData')
            lastLine = currentLine - 1;
            matrixFound = true;
        end
    end
    fclose(inFile);

    % Open file a second time to read in the data
    inFile = fopen(filepath);
    % eat the header lines
    for i = 1:firstLine-1
        tmpLine = fgetl(inFile);
    end
    % import the values matrix
    bufferData = fscanf(inFile, '%f %f %f %f', [4 lastLine-firstLine+1]);
    fclose(inFile);
        
    % Reored data into a sf7Data matrix
    scanData.freq       = bufferData(1, :);
    scanData.k2         = bufferData(2, :);
    scanData.Dk2        = bufferData(3, :);
    scanData.dH1        = bufferData(4, :);
    
end
