%%%%% FUNCTION eqom_Zmotion.m %%%%%
% Mattia Schaer
% April 2013
%
% Function for the longitudinal equations of motion of electrons in an RF
% field
%
% CALL [superfishFilebase, axisLength] = geometry_psiGun1(folderPrefix, runNo, simIndex, dimensions)
%           folderPrefix:   string
%           runNo:          int
%           simIndex:       int
%           dimensions:     float(Nparameters)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function dy = eqom_Efield_1d(t, y)

global constVars valForEqom;


% dy must be a column vector
dy = zeros(2, 1);


%% Prepare field value

% Total phase
phase = 2*pi*valForEqom.freq*t + valForEqom.phase0*pi/180;

switch valForEqom.fieldType
    case 'real'
        if y(2) < valForEqom.z(1)
            % to avoid numerical problems with interp1()
            Ez = valForEqom.Ez(1) * valForEqom.Emax;   % [V/m]
        else
            Ez = interp1(valForEqom.z, valForEqom.Ez, y(2)) * valForEqom.Emax;   % [V/m]
        end
        Ez = Ez * cos(phase);
    case 'complex'
        if y(2) < valForEqom.z(1)
            % to avoid numerical problems with interp1()
            Ez = valForEqom.EzRe(1) * cos(phase) - valForEqom.EzIm(1) * sin(phase);   % [V/m]
        else
            Ez = interp1(valForEqom.z, valForEqom.EzRe, y(2)) * cos(phase) - ...
                 interp1(valForEqom.z, valForEqom.EzIm, y(2)) * sin(phase);
        end
        Ez = Ez / valForEqom.EzRe(1) * valForEqom.Emax;
end


%% Equations of motion

% dp/dt
dy(1) = valForEqom.q * Ez;
% dz/dt
dy(2) = constVars.c.si * sqrt( y(1)^2 / ( y(1)^2 + valForEqom.m^2*constVars.c.si^2 ) );

    
end
