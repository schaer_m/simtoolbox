%%%%% SCRIPT run_integrate_eqom.m %%%%%
% Mattia Schaer
% July 2013
%
% Run integrate_eqom_Efield_1d() with a given fieldmap
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clear all;

global constVars;
setup_constants();


%% Some input
saveMovie = false;
% movieFilepath = '~/particleInField-5p6cells-design6-coupler4.avi';
movieFilepath = '~/particleInField-PSIGun1.avi';


%% Fieldmap list

% CONVERSION BETWEEN PHASE OF ASTRA AND THIS CODE
% gunPhase = phiAstra - 180 - 90

%Efield.fieldType = 'real';
%fmPath = 'X:\project\newgun\PSIGun1_3andHalfCells_CBandRescaled_cell1opt.astra';
%fmPath = '/afs/psi.ch/project/newgun/PSIGun1_3andHalfCells_CBandRescaled_cell1opt.astra';
% phase of maximum energy after manual tuning
%phaseMaxE           = -31;                          % [deg]
%gunPhase            = -13.186;                       % [deg]
%gunPhase            = 0.0;
%Efield.Emax         = 134.79E6;                     % [V/m]
%Efield.freq         = 5712.0E6;                     % [fmPath]

% Efield.fieldType = 'real';
% fmPath = '/afs/psi.ch/project/newgun/fieldmaps/fieldmap_1D_3p6cells-135maxE_36_1.astra';
% % phase of maximum energy after manual tuning
% phaseMaxE           = -51.328;                          % [deg]
% gunPhase            = -1.26;                       % [deg]
% gunPhase            = 0.0;
% Efield.Emax         = 135.0E6;                      % [V/m]
% Efield.freq         = 5712.0E6;                     % [fmPath]

%Efield.fieldType = 'real';
%fmPath = 'X:\project\newgun\rf\CTF2Gun5\superfish\matchBeadpullMeasurements\fieldmap_1D_ctf2gun5.dat';
%fmPath = '/afs/psi.ch/project/newgun/rf/CTF2Gun5/superfish/matchBeadpullMeasurements/fieldmap_1D_ctf2gun5.dat';
%phaseMaxE           = -51.25;                        % [deg]
%gunPhase            = -3.3;                         % [deg]
%gunPhase            = 0.0;                          % [deg]
%Efield.Emax         = 100.0E6;                      % [V/m]
%Efield.freq         = 2997.912E6;                   % [Hz]

% Efield.fieldType = 'real';
% fmPath = '/afs/psi.ch/project/newgun/fieldmaps/fieldmap_1D_PSIGun1_ORIGINAL.astra';
% % phaseMaxE           = -50.0;                        % [deg]
% % gunPhase            = -0.0;                         % [deg]
% Efield.Emax         = 100.0E6;                      % [V/m]
% Efield.freq         = 2998E6;                   % [Hz]
% phiAstra            = 228.3;

% Efield.fieldType = 'real';
% fmPath = '/afs/psi.ch/project/newgun/rf/PSICBandGun/5andHalfCells/design6_23_withCoupling_IPAC2014/fieldmap_1D_5p6cells-design6-coupler4_eigenmode.astra';
% % phaseMaxE           = -50;                        % [deg]
% % gunPhase            = -10;                         % [deg]
% phiAstra            = 224.05;
% Efield.Emax         = 135.0E6;                      % [V/m]
% Efield.freq         = 5712.0E6;                   % [Hz]

% Efield.fieldType = 'complex';
% fmPath{1} = '/afs/psi.ch/project/newgun/fieldmaps/hyb2_hybridGun2-firstTrial_3_5_re.astra';
% fmPath{2} = '/afs/psi.ch/project/newgun/fieldmaps/hyb2_hybridGun2-firstTrial_3_5_im.astra';
% phiAstra            = 211.4 + 0;                         % [deg]
% Efield.Emax         = 135.0E6;                      % [V/m]
% Efield.freq         = 5712.0E6;                   % [Hz]

% Efield.fieldType = 'complex';
% fmPath{1} = '/afs/psi.ch/project/newgun/rf/twGun/fullGun_1/Ez_All5pt_re.txt';
% fmPath{2} = '/afs/psi.ch/project/newgun/rf/twGun/fullGun_1/Ez_All5pt_im.txt';
% phiAstra            = 212.16;                         % [deg]
% Efield.Emax         = 135.0E6;                      % [V/m]
% Efield.freq         = 5712.0E6;                   % [Hz]

% Efield.fieldType = 'real';
% fmPath = '/afs/psi.ch/project/newgun/fieldmaps/fieldmap_1D_2p6cell-first0p6-normal1p0-iris1p2mm_10_3.astra';
% % phase of maximum energy after manual tuning
% phaseMaxE           = -243.55;                          % [deg]
% gunPhase            = -20.0;
% % phiAstra            = 26.45;                         % [deg]
% Efield.Emax         = 135.0E6;                      % [V/m]
% Efield.freq         = 5712.0E6;                     % [fmPath]

Efield.fieldType = 'complex';
fmPath{1} ='/gpfs/home/schaer_m/astraRuns/twGunSwissfelInjector/fullTwGun120deg1-4ps-tune-gunOnly_11/fieldmapsLocal/tune3_fullTwGun120deg1-4ps-tune-gunOnly_11_1_re.astra';
fmPath{2} ='/gpfs/home/schaer_m/astraRuns/twGunSwissfelInjector/fullTwGun120deg1-4ps-tune-gunOnly_11/fieldmapsLocal/tune3_fullTwGun120deg1-4ps-tune-gunOnly_11_1_im.astra';
% phase of maximum energy after manual tuning
% phaseMaxE           = ;                          % [deg]
% gunPhase            = ;
phiAstra            = 180;                         % [deg]
Efield.Emax         = 135.0E6;                      % [V/m]
Efield.freq         = 5712.0E6;                     % [fmPath]

% Astra uses sin(omega*t) and not cos(omega*t) (--> 90 deg shift) and 
% negative fields (--> 180 deg shift) for electrons
if exist('phiAstra', 'var')
    gunPhase = phiAstra - 180 - 90;
    phaseMaxE = 0.0;
end


%% Read fieldmap
switch Efield.fieldType
    
    case 'real'
        fm = importdata(fmPath);
        Efield.z            = fm(:, 1);                     % [m]
        % Prompt a warning if the fieldmap is not normalized
        EzMax = max(abs(fm(:, 2)));
        if EzMax > 1.0
            fprintf('WARNING: the input profile of the field is not normalized!\n');
            fprintf('\t\t max(Ez) = %f\n', EzMax);
        end
        Efield.Ez           = fm(:, 2)/EzMax;               % [normalized]

    case 'complex'
        fm = importdata(fmPath{1});
        Efield.z            = fm(:, 1);                     % [m]
        Efield.EzRe         = fm(:, 2);
        fm = importdata(fmPath{2});
        % check Efield.z
        Efield.EzIm         = interp1(fm(:,1), fm(:, 2), Efield.z);
        Efield.EzIm(isnan(Efield.EzIm)) = 0.0;
        
end

Efield.phase0       = phaseMaxE + gunPhase;         % [deg]

% Prepare variables required for integration
% initial position (i.e. position of cathode)
particle.x0         = 0.0;                          % [m]
% initial time
particle.t0         = 0;                            % [s]
% kinetic energy of electrons coming out from the cathode
particle.Ekin0      = 0.63;                         % [eV]
% electron mass
particle.m.nat      = constVars.mElectron.nat;      % [eV/c^2]
particle.m.si       = constVars.mElectron.si;       % [kg]
% electron charge
particle.q          = constVars.qElectron.si;       % [C]


%% Integrate
kinematics = integrate_eqom_Efield_1d(Efield, particle);


%% Plot desired quantities
figure(1);
FontSize = 16;
LineWidth = 2;

xLim = kinematics.z([1 end])/100;

% subplot(5, 1, 1);
% plot(xLim, [0 0], 'k:');
% hold on;
% plot(Efield.z*1E2, Efield.Ez/max(Efield.Ez));
% hold off;
% xlabel('z [m]');
% ylabel('Ez [normalized]');
% axis([xLim -1 1]);
subplot(4, 1, 1);
plot(kinematics.z/100, kinematics.t*1E9, 'LineWidth',LineWidth);
yLim = [min(kinematics.t*1E9) max(kinematics.t*1E9)];
% hold on;
% plot([0.0265 0.0265], yLim, 'k:', 'LineWidth',LineWidth);
% plot([0.0765 0.0765], yLim, 'k:', 'LineWidth',LineWidth);
% hold off;
ylabel('t [ns]', 'FontSize', FontSize);
%xlabel('z [m]', 'FontSize', FontSize);
% xlim(xLim);
axis([xLim yLim]);
set(gca, 'FontSize', FontSize);
subplot(4, 1, 2);
plot(kinematics.z/100, kinematics.beta, 'LineWidth',LineWidth);
yLim = [min(kinematics.beta) max(kinematics.beta)];
% hold on;
% plot([0.0265 0.0265], yLim, 'k:', 'LineWidth',LineWidth);
% plot([0.0765 0.0765], yLim, 'k:', 'LineWidth',LineWidth);
% hold off;
%xlabel('z [m]', 'FontSize', FontSize);
ylabel('\beta_z', 'FontSize', FontSize);
% xlim(xLim);
axis([xLim yLim]);
set(gca, 'FontSize', FontSize);

% subplot(5, 1, 3);
% plot(kinematics.z/100, kinematics.gamma, 'LineWidth',LineWidth);
% %xlabel('z [m]', 'FontSize', FontSize);
% ylabel('\gamma', 'FontSize', FontSize);
% % xlim(xLim);
% axis([xLim min(kinematics.gamma) max(kinematics.gamma)]);
% set(gca, 'FontSize', FontSize);

subplot(4, 1, 3);
plot(kinematics.z/100, kinematics.Ekin, 'LineWidth',LineWidth);
yLim = [min(kinematics.Ekin) max(kinematics.Ekin)];
% hold on;
% plot([0.0265 0.0265], yLim, 'k:', 'LineWidth',LineWidth);
% plot([0.0765 0.0765], yLim, 'k:', 'LineWidth',LineWidth);
% hold off;
%xlabel('z [m]', 'FontSize', FontSize);
ylabel('E_{kin} [MeV]', 'FontSize', FontSize);
% xlim(xLim);
axis([xLim yLim]);
set(gca, 'FontSize', FontSize);
subplot(4, 1, 4);
plot(kinematics.z/100, kinematics.expEz, 'LineWidth',LineWidth);
yLim = [min(kinematics.expEz) max(kinematics.expEz)];
% hold on;
% plot(xLim, [0 0], 'k--');
% plot([0.0265 0.0265], yLim, 'k:', 'LineWidth',LineWidth);
% plot([0.0765 0.0765], yLim, 'k:', 'LineWidth',LineWidth);
% hold off;
xlabel('z [m]', 'FontSize', FontSize);
ylabel('Exp. E_z [MV/m]', 'FontSize', FontSize);
% xlim(xLim);
axis([xLim yLim]);
set(gca, 'FontSize', FontSize);

fprintf('Final kinetic energy at phase %5.2f: %f MeV\n', Efield.phase0,kinematics.Ekin(end));


%% Make movie of particle moving through the field 
hf2 = figure(2);
set(hf2, 'Color','w')
FontSize = 18;
LineWidth = 2;

movieInd = 1:200:numel(kinematics.t);
t = kinematics.t(movieInd);
phase       = 2*pi*Efield.freq*t + Efield.phase0*pi/180;
phaseAstra  = 2*pi*Efield.freq*t + phiAstra*pi/180;
phaseOpal   = (180-phiAstra)*pi/180 - 2*pi*Efield.freq*t;
% Record video
if saveMovie
    writerObj = VideoWriter(movieFilepath);
    writerObj.FrameRate = 20;
    open(writerObj);
else
    F(numel(phase)) = struct('cdata',[], 'colormap',[]);
end

for ii = 1:numel(phase)
    if strcmp(Efield.fieldType, 'complex')
        % Mine
        Ez = Efield.EzRe * cos(phase(ii)) - Efield.EzIm * sin(phase(ii));
        % ASTRA
        %Ez = Efield.EzRe * sin(phaseAstra(ii)) + Efield.EzIm * sin(pi/2-phaseAstra(ii));
        % OPAL
        %Ez = Efield.EzRe * sin(phaseOpal(ii)) - Efield.EzIm * sin(pi/2-phaseOpal(ii));
    else
        % Mine
        Ez = Efield.Emax * Efield.Ez * cos(phase(ii));
    end
    plot(Efield.z, Ez/1E6, 'LineWidth',LineWidth);
    ylim([-150 150]);
    hold on;
    plot(repmat(kinematics.z(movieInd(ii))/100,1,2), [interp1(Efield.z,Ez,kinematics.z(movieInd(ii))/100)/1E6 0], 'ro-', 'LineWidth',LineWidth);
    plot(xlim(), [0 0], 'k--');
    % Location of output coupler for 18-cell TW gun
    %plot([0.1745 0.1745], ylim(), 'k--');
    hold off;
    title(['Phase = ' num2str(round(phase(ii)*180/pi), '%3d deg')]);
    xlabel('z [m]');
    ylabel('E_z [MV/m]');
    set(gca, 'FontSize',FontSize);
    % record slice
    if saveMovie
        writeVideo(writerObj, getframe(hf2));
    else
        F(ii) = getframe(gcf);
    end
end

if saveMovie
    close(writerObj);
else
    movie(gcf, F, 1);
end
