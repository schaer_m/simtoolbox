%%% FUNCTION get_modes.m %%%
% Mattia Schaer
% July 2013
%
% Find the frequency of the different modes from the frequency scan data
%
% CALL [modes] = get_modes(freqScanData)
%
%           freqScanData:       struct
%
%           modes:              struct
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function modes = get_modes(freqScanData)

    % Found modes
    modes = [];
    modesNum = 0;
    % initial sign of D(k^2)
    currentSign = sign(freqScanData.Dk2(1));
    for ii = 2:numel(freqScanData.freq)
        % The modes are found qhere D(k^2)=0 and its slope is -1
        nextSign = sign(freqScanData.Dk2(ii));
        if currentSign > 0 && nextSign < 0
            modesNum = modesNum + 1;
            modes.freq(modesNum) = interp1(freqScanData.Dk2([ii-1 ii]), freqScanData.freq([ii-1 ii]), 0.0);
            if modesNum > 1
                modes.separation(modesNum) = modes.freq(modesNum) - modes.freq(modesNum-1);
            else
                modes.separation(modesNum) = NaN;
            end
        end
        currentSign = nextSign;
    end
            
end
