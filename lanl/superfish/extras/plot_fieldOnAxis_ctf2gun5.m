%%%%% FUNCTION plot_fieldOnAxis_ctf2gun5.m %%%%%
% Mattia Schaer
% February 2013
%
% Plot the field on axis of the selected simulation together with the
% corrected beadpull measurements and the old curve used for simulations
% for the CTF2 Gun 5
%
% CALL plot_fieldOnAxis_ctf2gun5(simData)
%           simData:        float(Npoints, 6)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [] = plot_fieldOnAxis_ctf2gun5(simData)

% First plot the measured data
% filepath under Windows
%filepath = 'X:\project\newgun\rf\CTF2Gun5\';
filepath = '/afs/psi.ch/project/newgun/rf/CTF2Gun5/';
rawData = importdata([filepath 'beadpull_extractedValues.txt']);
% Ignored measured points which does not look good
ignored = [1 2 31 32 78 77];
rawData.data(ignored,2) = NaN;

% Insert an horizontal shift to correct for the error in the measured data
% (z=0 of the measured data is not the real z=0)
xShift = 1.5;   % [mm] --> "manually" determined by matching the zero points
if xShift < 0
    xLeft = -xShift;
    xRight = 0;
else
    xLeft = 0;
    xRight = xShift;
end
% step in z-direction for the spline interpolation
zStep = 0.1;   % [mm]
zSpline = rawData.data(1,1):zStep:rawData.data(end,1);
EzSpline = spline(rawData.data(:,1), rawData.data(:,2), zSpline);
% Plot the experimental line (spline interpolated)
plot(zSpline(1+fix(xRight/zStep):end-fix(xLeft/zStep))/10, -EzSpline(1+fix(xLeft/zStep):end-fix(xRight/zStep)), '-.k');
hold on;

% Plot the old curve used for beam dynamics simulations
oldData = importdata([filepath 'CTF3_Ez_ASTRA.dat']);
plot(oldData(:,1)*100, oldData(:,2), 'r');

% Plot the SuperFish solution of the selected simulation
plot(simData.z, simData.EzNorm, 'b');

xlabel('z [cm]');
ylabel('E_z [normalized]');
legend('Beadpull measurement', 'OLD curve used for simulations', 'Selected simulation', 'Location','SouthEast');
axis([0.0 max(simData.z) -1.0 1.0]);
hold off;

end
