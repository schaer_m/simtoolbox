%%% FUNCTION get_cellLengths.m %%%
% Mattia Schaer
% July 2013
%
% Compute electrical cell lengths from the field on-axis
%
% CALL electricalLengths = get_cellLengths(sf7Data, mechanicalLengths)
%
%           sf7Data:            float N x 2 , (Nth entry, :) = [ Z , Ez ]
%           electricalLengths:  float N
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function electricalLengths = get_cellLengths(sf7Data, mechanicalLengths)

    cellsNum = numel(mechanicalLengths);
    % range width around iris where to search for the zero of Ez
    searchFactor = 0.3;
    % for the last cell length computation, look for Ez going under a certain threshold
    lastCellLengthThreshold = 0.01;
    % variables to remember at which z we are in the gun
    irisPosition = 0.0;
    electricalPosition = 0.0;

    electricalLengths = zeros(1, cellsNum);
    for ii = 1:cellsNum
        % z range to consider for cell ii
        irisPosition = irisPosition + mechanicalLengths(ii);
        minCellZ = irisPosition - searchFactor*mechanicalLengths(ii);
        maxCellZ = irisPosition + searchFactor*mechanicalLengths(ii);
        cellInd = sf7Data.z>minCellZ & sf7Data.z<maxCellZ;
        % Search zero of Ez in the selected region
        if ii < cellsNum
            electricalLengths(ii) = interp1(sf7Data.EzNorm(cellInd), sf7Data.z(cellInd), 0.0) - electricalPosition;
        else
            % for the last cell look for Ez going under a certain threshold
            electricalLengths(ii) = interp1(abs(sf7Data.EzNorm(sf7Data.z>minCellZ)), sf7Data.z(sf7Data.z>minCellZ), lastCellLengthThreshold) - electricalPosition;
        end
        electricalPosition = electricalPosition + electricalLengths(ii);
    end
    
end
