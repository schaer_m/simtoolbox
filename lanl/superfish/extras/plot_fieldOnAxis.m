%%%%% FUNCTION plot_fieldOnAxis.m %%%%%
% Mattia Schaer
% April 2013
%
% Plot the field on axis of the selected simulation
%
% CALL plot_fieldOnAxis(simData)
%           simData:        float(Npoints, 6)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [] = plot_fieldOnAxis(simData)

% Plot the SuperFish solution of the selected simulation
plot(simData.z, simData.EzNorm, 'b');
xLims = simData.z([1 end]);
axis([xLims -1.0 1.0]);
hold on;
plot(xLims, [0 0], 'k:');
hold off;
xlabel('z [m]');
ylabel('Ez [normalized]');
legend('Selected simulation', 'Location','SouthEast');

end
