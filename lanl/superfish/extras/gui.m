function varargout = gui(varargin)
% GUI MATLAB code for gui.fig
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI.M with the given input arguments.
%
%      GUI('Property','Value',...) creates a new GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui

% Last Modified by GUIDE v2.5 03-Nov-2014 14:46:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gui is made visible.
function gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui (see VARARGIN)

% Choose default command line output for gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gui wait for user response (see UIRESUME)
% uiwait(handles.mainWindow);

% Load physical constants
setup_constants();


% --- Outputs from this function are returned to the command line.
function varargout = gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Destroy sliders, related labels, compare toggles
arrayFieldsToReset = {'paramSlider', 'paramLabelName', 'paramLabelActualVal', 'paramLabelMinVal', 'paramLabelMaxVal', 'compareToggle'};
% Other single things to destroy
singleFieldsToReset = {'compareButton'};
if isfield(handles, 'rp')
    % arrays
    for ii = 1:length(arrayFieldsToReset)
        for jj = 1:handles.rp.active.no
            if handles.(arrayFieldsToReset{ii})(jj) ~= 0
                delete(handles.(arrayFieldsToReset{ii})(jj));
                %fprintf('deleting %s %d\n', fieldsToReset{ii}, jj);
            end
        end
        if isfield(handles, arrayFieldsToReset{ii})
            handles = rmfield(handles, arrayFieldsToReset{ii});
        end
    end
    % singles
    for ii = 1:length(singleFieldsToReset)
        if handles.(singleFieldsToReset{ii}) ~= 0
            delete(handles.(singleFieldsToReset{ii}));
        end
        if isfield(handles, singleFieldsToReset{ii})
            handles = rmfield(handles, singleFieldsToReset{ii});
        end
    end
    % run data
    handles = rmfield(handles, 'rp');
end

% Destroy other variables
clearvars activeInd;
handles.cellLengths = [];

% Select new file to open
[filename, handles.pathname, ~] = uigetfile([handles.runsDir 'runParameters_*.mat']);
% If the user selected cancel, return
if filename == 0
    return;
end
% Read file
load([handles.pathname filename], '-mat');
% Store runParam under handles
handles.rp = runParam;
set(handles.loadedRun, 'String',handles.rp.runNo);

% Create sliders
% first slider position
sliderPos = [1440 780 260 20];
% parameter name label position
labelNamePos = [1300 780 120 20];
% parameter actual value label position
labelActualValPos = [1520 800 80 15];
% parameter min value label position
labelMinValPos = [1440 800 80 15];
% parameter max value label position
labelMaxValPos = [1600 800 80 15];
% position of toggle button for compare plot
toggleComparePos = [1720 780 40 15];
% vertical distance
separation = 40;

for jj = 1:handles.rp.active.no
    
    % Default slider index
    defSliderInd = fix(handles.rp.active.length(jj)/2);

    % Slider
    handles.paramSlider(jj) = uicontrol(gcf, 'Style','slider', 'Position',sliderPos, 'BackgroundColor',[.9 .9 .9]);
    % Create callback for sliders and set default position
    set(handles.paramSlider(jj), ...
        'Callback',@paramSlider_Callback, ...
        'Min',1, 'Max',handles.rp.active.length(jj), 'Value', defSliderInd, ...
        'SliderStep',repmat(1/(handles.rp.active.length(jj)-1), 1, 2));

    % Move to next position (for next loop)
    sliderPos(2) = sliderPos(2) - separation;

    % Labels
    % parameter name text label
    handles.paramLabelName(jj) = uicontrol('Style','text', 'Position',labelNamePos, ...
        'String',handles.rp.active.name{jj}, 'HorizontalAlignment','left');
    labelNamePos(2) = labelNamePos(2) - separation;
    % parameter actual value text label
    actualVal = handles.rp.(handles.rp.active.name{jj}).nom + handles.rp.(handles.rp.active.name{jj}).var(defSliderInd);
    handles.paramLabelActualVal(jj) = uicontrol('Style','text', 'Position',labelActualValPos, ...
        'String',sprintf('%9.6f', actualVal), 'HorizontalAlignment','center');
    labelActualValPos(2) = labelActualValPos(2) - separation;
    % parameter min value text label
    minVal = handles.rp.(handles.rp.active.name{jj}).nom + min(handles.rp.(handles.rp.active.name{jj}).var);
    handles.paramLabelMinVal(jj) = uicontrol('Style','text', 'Position',labelMinValPos, ...
        'String',sprintf('%9.6f', minVal), 'HorizontalAlignment','left');
    labelMinValPos(2) = labelMinValPos(2) - separation;
    % parameter max value text label
    maxVal = handles.rp.(handles.rp.active.name{jj}).nom + max(handles.rp.(handles.rp.active.name{jj}).var);
    handles.paramLabelMaxVal(jj) = uicontrol('Style','text', 'Position',labelMaxValPos, ...
        'String',sprintf('%9.6f', maxVal), 'HorizontalAlignment','right');
    labelMaxValPos(2) = labelMaxValPos(2) - separation;
    
    % Toggle buttons for compare plots
    handles.compareToggle(jj) = uicontrol(gcf, 'Style','checkbox', 'Position',toggleComparePos, 'String','');
    toggleComparePos(2) = toggleComparePos(2) - separation;
    % Create callback for toggle buttons
    set(handles.compareToggle(jj), 'Callback',@compareToggle_Callback);

    % store new created slider as last active slider
    activeInd = jj;
    % store value index of new created slider
    handles.actualIndices(jj) = defSliderInd;
end

% Create the button to compare fieldmaps
compareButtonPos = [1720 800 60 20];
compareButtonPos(2) = toggleComparePos(2);
handles.compareButton = uicontrol('Style','pushbutton', 'Position',compareButtonPos, 'String','Compare', 'Enable','off');
% Create callback for this button
set(handles.compareButton, 'Callback',@compareButton_Callback);

% Update handles structure
guidata(hObject, handles);

% Call the callback to have the first plot of the fieldmap
% simulate a call from an active slider if it exists
% otherwise pass 0 as handle
if ~exist('activeInd', 'var')
    handleForCallback = 0;
else
    handleForCallback = handles.paramSlider(activeInd);
end
paramSlider_Callback(handleForCallback, eventdata);


% --- Executes on sliders movement (SAME CALLBACK FOR ALL SLIDERS)
function paramSlider_Callback(hObject, eventdata)
% Get the guidata
% not automatically passed as function parameter since this callback was
% not automatically created buy guide
handles = guidata(gcf);

if hObject ~= 0
% if there are active indices (more than 1 simulation in run)

    % Determine which slider is calling the function
    paramIndex = find(hObject == handles.paramSlider);

    % Set the slider on the nearest index (only discrete positions are allowed)
    actualValue = get(hObject, 'Value');
    actualIndex = round(actualValue);
    set(hObject, 'Value',actualIndex);
    % Store the new index
    handles.actualIndices(paramIndex) = actualIndex;

    % Set the actual value label to the correct value
    actualVal = handles.rp.(handles.rp.active.name{paramIndex}).nom + handles.rp.(handles.rp.active.name{paramIndex}).var(actualIndex);
    set(handles.paramLabelActualVal(paramIndex), 'String',sprintf('%9.6f', actualVal));

    % Find simulation index corresponding to the selected parameters
    simIndexOld = findSimIndex(handles, handles.actualIndices);
    if handles.rp.active.no > 1
        tmpCell = num2cell(handles.actualIndices);
        tmpCell = {handles.rp.active.length, tmpCell{:}};
        simIndex = sub2ind(tmpCell{:});
    else
        simIndex = handles.actualIndices;
    end
    if simIndex ~= simIndexOld
        fprintf('INDEX PROBLEM!!!\n');
    end

else
% if there are no active indices (only 1 simulation in run)

    simIndex = 1;
    
end

handles.simIndex = simIndex;
% Update handles structure
guidata(gcf, handles);

% Open sf7 file with the field data
sf7Filename = [handles.rp.list(simIndex).simFilebase '.OUT7'];
[handles.sf7Data, fileOk] = read_data_file(handles.pathname, sf7Filename, 'sf7');
if ~fileOk
    warndlg(['File ' sf7Filename ' not found or does not contain valid data!']);
    % Clear plot
    cla(handles.fieldOnAxisAxes);
    % Do not allow to export the fieldmap
    set(handles.exportFieldmapButton, 'Enable','off');
    return;
else
    % Plot field on axis
    axes(handles.fieldOnAxisAxes);
    %plot_fieldOnAxis_ctf2gun5(handles.sf7Data);
    plot_fieldOnAxis(handles.sf7Data);
end

% Open fish file with the frequency data
fishFilename = [handles.rp.list(simIndex).simFilebase '.FISH'];
[handles.fishData, fileOk] = read_data_file(handles.pathname, fishFilename, 'fish');
if ~fileOk
    warndlg(['File ' fishFilename ' not found or does not contain valid data!']);
    set(handles.freqLabel, 'String', 'not found');
    return;
else
    set(handles.freqLabel, 'String', num2str(handles.fishData.freq));
end

% Open sfo file with quality factor and shunt impedance data
sfoFilename = [upper(handles.rp.list(simIndex).simFilebase) '.SFO'];
[handles.sfoData, fileOk] = read_data_file(handles.pathname, sfoFilename, 'sfo');
if ~fileOk
    warndlg(['File ' sfoFilename ' not found or does not contain valid data!']);
    set(handles.qLabel, 'String', 'not found');
    set(handles.shuntImpLabel, 'String', 'not found');
    return;
else
    set(handles.qLabel, 'String', num2str(handles.sfoData.q));
    set(handles.shuntImpLabel, 'String', num2str(handles.sfoData.shuntImpedance));
    set(handles.EmaxSurfLabel, 'String', num2str(handles.sfoData.EmaxSurface/(max(abs(handles.sf7Data.Ez))*1E6)));
    set(handles.HmaxSurfLabel, 'String', num2str(handles.sfoData.HmaxSurface));
end

% Compute cell lengths
% mechanical length (iris position)
for ii = 1:handles.rp.list(simIndex).cellsNum
    cellLengthFieldName = ['l' num2str(ii)];
    if ~strcmp(handles.rp.geometryFunction, 'CBandOvermoded')
        handles.cellLengths.mechanic(ii) = handles.rp.list(simIndex).(cellLengthFieldName);   % [cm]
    else
        if ii == 1
            handles.cellLengths.mechanic(ii) = 1/4 * handles.c.si/handles.fishData.freq /1E4;   % [cm]
        else
            handles.cellLengths.mechanic(ii) = 1/2 * handles.c.si/handles.fishData.freq /1E4;   % [cm]
        end
    end
end
% electrical length (position of zero Ez)
handles.cellLengths.electric = get_cellLengths(handles.sf7Data, handles.cellLengths.mechanic);   % [cm]
% convert from length units [cm] to "velocity units" [1/c]
tableValues = zeros(handles.rp.cellsNum.nom, 6);
tableValues(:, 1) = 2*handles.cellLengths.mechanic'*1E-2 * handles.fishData.freq*1E6 / handles.c.si;
tableValues(:, 2) = 2*handles.cellLengths.electric'*1E-2 * handles.fishData.freq*1E6 / handles.c.si;
% "transit time" of EM wave over cells: half-period since we have a standing wave
tableValues(:, 5) = 180 / (360*handles.fishData.freq*1E6);
% for the first half cell, the phase offset must be taken into consideration!
% this is implemented in kinematicsEdit_Callback

% Set values in gui
set(handles.cellLengthsTable, 'Data', tableValues);

% Update again (2) handles structure
guidata(gcf, handles);

% Compute kinematics with default gradient and phase
kinematicsEdit_Callback(handles.gunGradientEdit, eventdata);

% Allow to edit parameters for kinematics calculations
set(handles.initialEnergyEdit, 'Enable','on');
set(handles.gunGradientEdit, 'Enable','on');
set(handles.gunPhaseEdit, 'Enable','on');
set(handles.pulseLengthEdit, 'Enable','on');
% Allow to export the fieldmap
set(handles.exportFieldmapEdit, 'String',[handles.pathname 'fieldmap_1D_' handles.rp.list(simIndex).simFilebase]);
set(handles.exportFieldmapButton, 'Enable','on');

% Call the callback of the toggle for the radial behavior of the electric
% field if it is activated
if get(handles.fieldAtBackplateToggle, 'Value')
    fieldAtBackplateToggle_Callback(handles.fieldAtBackplateToggle, eventdata, handles)
end

set(handles.loadedSim, 'String',simIndex);


% --- Executes on button press (SAME CALLBACK FOR ALL TOGGLES)
function compareToggle_Callback(hObject, eventdata)
% Get the guidata
% not automatically passed as function parameter since this callback was
% not automatically created buy guide
handles = guidata(gcf);

compareToggleCell = get(handles.compareToggle(:), 'Value');
if handles.rp.active.no == 1
    compareToggled = compareToggleCell;
else
    compareToggleArray = [compareToggleCell{:}];
    compareToggled = sum(compareToggleArray);
end
if compareToggled > 0
    set(handles.compareButton, 'Enable','on');
else
    set(handles.compareButton, 'Enable','off');
end

% Update handles structure
guidata(gcf, handles);


% --- Executes on button press compareButton below the sliders
function compareButton_Callback(hObject, eventdata)
% Get the guidata
% not automatically passed as function parameter
% since this callback was not automatically created by guide
handles = guidata(gcf);

% Create new figure for comparison plot
figure('Name','Fieldmaps comparison');
% Loop over all selected indices and plot fieldmap
curvesCounter = 0;
matlabColors = {[0 0 1], [1 0 0], [0 1 0], [1 0 1], [0 1 1], [1 1 0]};
tmpIndices = num2cell(handles.actualIndices);
for jj = 1:handles.rp.active.no
    if get(handles.compareToggle(jj), 'Value')
        for ii = 1:handles.rp.active.length(jj)
            tmpIndices{jj} = ii;
            %simIndex = findSimIndex(handles, tempIndices);
                %tmpCell = num2cell(handles.actualIndices);
                %tmpCell = {handles.rp.active.length, tmpCell{:}};
                %simIndex = sub2ind(tmpCell{:});
            if handles.rp.active.no > 1
                tmpCell = [handles.rp.active.length, {tmpIndices}];
                simIndex = sub2ind(tmpCell{:});
            else
                simIndex = tmpIndices{jj};
            end
            [sf7Data, fileOk] = read_data_file(handles.pathname, [handles.rp.list(simIndex).simFilebase '.OUT7'], 'sf7');
            if ~fileOk
                continue;
            end
            curvesCounter = curvesCounter + 1;
            tempValue = handles.rp.(handles.rp.active.name{jj}).nom + handles.rp.(handles.rp.active.name{jj}).var(ii);
            legendStrings{curvesCounter} = [handles.rp.active.name{jj} ' = ' num2str(tempValue) ' ' handles.rp.(handles.rp.active.name{jj}).unit]; %#ok<AGROW>
            % generate intensity scale of same color for the current parameter
            lineStyle = '-';
            if ii == 1
                lineStyle = '--';
            elseif ii == handles.rp.active.length(jj)
                lineStyle = ':';
            end
            tempColor = matlabColors{jj} * (0.6 + 0.4*(ii-1)/(handles.rp.active.length(jj)-1));
            %plot(sf7Data.z, sf7Data.EzNorm, lineStyle, 'Color',tempColor);
            plot(sf7Data.z, sf7Data.Ez/sf7Data.Ez(1), lineStyle, 'Color',tempColor);
            hold on;
            xLims = sf7Data.z([1 end]);
        end
    end
end
axis([xLims -1.1 1.1]);
plot(xLims, [0 0], 'k:');
xlabel('z [m]');
ylabel('Ez [normalized]');
legend(legendStrings, 'Location','SouthEast');
hold off;

% Update handles structure
guidata(gcf, handles);


% --- Executes on button press "compare" of single values
function compareButtonSingle_Callback(hObject, eventdata, handles)
% Get the guidata
% not automatically passed as function parameter
% since this callback was not automatically created by guide
%handles = guidata(gcf);

% Find out which button was pressed and define corresponding settings
switch get(hObject, 'Tag')
    case 'compareButton_freq'
        valueName   = 'freq';
        originFile      = 'fish';
        originFileExt   = 'FISH';
    case 'compareButton_Q'
        valueName       = 'q';
        originFile      = 'sfo';
        originFileExt   = 'SFO';
    case 'compareButton_shuntImp'
        valueName   = 'shuntImpedance';
        originFile      = 'sfo';
        originFileExt   = 'SFO';
    case 'compareButton_EmaxSurf'
        valueName   = 'EmaxSurface';
        originFile      = 'sfo';
        originFileExt   = 'SFO';
    case 'compareButton_HmaxSurf'
        valueName   = 'HmaxSurface';
        originFile      = 'sfo';
        originFileExt   = 'SFO';
end

% Create new figure for comparison plot
figure('Name',['Compare: ' valueName]);
% Loop over all selected indices and plot parameter value
curvesCounter = 0;
matlabColors = {[0 0 1], [1 0 0], [0 1 0], [1 0 1], [0 1 1], [1 1 0]};
tmpIndices = num2cell(handles.actualIndices);
for jj = 1:handles.rp.active.no
    if get(handles.compareToggle(jj), 'Value')
        curvesCounter = curvesCounter + 1;
        for ii = 1:handles.rp.active.length(jj)
            tmpIndices{jj} = ii;
            %simIndex = findSimIndex(handles, tempIndices);
            if numel(tmpIndices) > 1
                tmpCell = {handles.rp.active.length, tmpIndices{:}};
                simIndex = sub2ind(tmpCell{:});
            else
                simIndex = tmpIndices{jj};
            end
            [data, fileOk] = read_data_file(handles.pathname, [handles.rp.list(simIndex).simFilebase '.' originFileExt], originFile);
            if ~fileOk
                continue;
            end
            tempParam(ii)       = handles.rp.(handles.rp.active.name{jj}).nom + handles.rp.(handles.rp.active.name{jj}).var(ii); %#ok<AGROW>
            tmpValue(ii)        = data.(valueName); %#ok<AGROW>
        end
        tempColor = matlabColors{jj};
        plot(tempParam, tmpValue, 's-', 'Color',tempColor);
        hold on;
        legendStrings{curvesCounter} = ['Active parameter: ' handles.rp.active.name{jj} ' [' handles.rp.(handles.rp.active.name{jj}).unit ']']; %#ok<AGROW>
    end
end
xlabel('Variable parameter');
ylabel(valueName);
legend(legendStrings)
hold off;


% --- Executes on button press in exportFieldmapButton.
function exportFieldmapButton_Callback(hObject, eventdata, handles)
% hObject    handle to exportFieldmapButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

outOptions.filepath = get(handles.exportFieldmapEdit, 'String');
outOptions.zStep = 1E-5;   % [m]
write_fieldmap_1D(handles.sf7Data, 'superfish', outOptions, handles.fishData.freq);



function exportFieldmapEdit_Callback(hObject, eventdata, handles)
% hObject    handle to exportFieldmapEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of exportFieldmapEdit as text
%        str2double(get(hObject,'String')) returns contents of exportFieldmapEdit as a double


% --- Executes during object creation, after setting all properties.
function exportFieldmapEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to exportFieldmapEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function gunGradientEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gunGradientEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject, 'Callback',@kinematicsEdit_Callback);


% --- Executes during object creation, after setting all properties.
function gunPhaseEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gunPhaseEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject, 'Callback',@kinematicsEdit_Callback);


function kinematicsEdit_Callback(hObject, eventdata)
% hObject    handle to gunGradientEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of gunGradientEdit as text
%        str2double(get(hObject,'String')) returns contents of gunGradientEdit as a double

% Get the guidata
% not automatically passed as function parameter since this callback was
% not automatically created buy guide
handles = guidata(gcf);

%Get initial energy
handles.Ekin0 = str2double(get(handles.initialEnergyEdit, 'String'));
% Get gradient
handles.Emax = str2double(get(handles.gunGradientEdit, 'String'));
% Get phase
handles.gunPhase = str2double(get(handles.gunPhaseEdit, 'String'));
% Get laser pulse length
handles.pulseLength = str2double(get(handles.pulseLengthEdit, 'String'));
% Get table data
tableValues = get(handles.cellLengthsTable, 'Data');
% Compute exit velocities with the new input
handles.kinematics = get_exitVelocity(handles);
tableValues(:, 3) = handles.kinematics(1).exitVelocity';
tableValues(:, 4) = handles.kinematics(1).exitEnergy';
tableValues(:, 6) = handles.kinematics(1).transitTime';
% Consider "transit time" correction in the first cell due to gun phase
tableValues(1, 5) = (90 - handles.gunPhase) / (360 * handles.fishData.freq*1E6);
% Update table values
set(handles.cellLengthsTable, 'Data', tableValues);

% Update handles structure
guidata(hObject, handles);


% --- Executes on mouse press over axes background.
function freqScanAxes_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to freqScanAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Select run containing a frequency scan
[filename, pathname, ~] = uigetfile([handles.runsDir 'runParameters_*.mat']);
% If the user selected cancel, return
if filename == 0
    disp('here')
    return;
end

% Read file
load([pathname filename], '-mat');
% Open the file with the freqLabel scan data
scanFilepath = [pathname runParam.list(1).simFilebase '.TBL'];
foundfile = dir(scanFilepath);
% if the file is found
if isempty(foundfile)
    set(handles.freqScanLabel, 'String','not found');
    return
end
handles.fishData.freqScan = read_freqScan_outputfile([pathname foundfile.name]);
% Plot
axes(handles.freqScanAxes);
plot(handles.fishData.freqScan.freq, handles.fishData.freqScan.Dk2, 'x-');
hold on;
plot(handles.fishData.freqScan.freq([1 end]), [0.0 0.0], 'k', 'LineWidth',2);
xlim(handles.fishData.freqScan.freq([1 end]));
xlabel('Frequency [MHz]');
ylabel('D(k^2)');
%Display loaded run
set(handles.freqScanLabel, 'String',runParam.list(1).simFilebase);

% Find the modes
handles.modes = get_modes(handles.fishData.freqScan);
if ~isempty(handles.modes)
    modesTableData(:, 1) = handles.modes.freq';
    modesTableData(:, 2) = handles.modes.separation';
    set(handles.modesTable, 'Data', modesTableData);
    plot(handles.modes.freq, zeros(1, numel(handles.modes.freq)), 'ro');
end
hold off;
% to be able to change displayed frequency scan
set(handles.freqScanAxes, 'NextPlot','replacechildren');

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function mainWindow_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mainWindow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Define some general variables
%handles.runsDir = 'C:\Documents and Settings\schaer_m\superfishRuns\';
handles.runsDir = '/scratch/lanlRuns/';

% Define some constants
handles.mElectron.nat   = 5.10998928E5;     % [eV/c^2]
handles.mElectron.si    = 9.10938291E-31;   % [kg]
handles.c.si            = 2.99792458E8;     % [m/s]
handles.e.si            = 1.60217657E-19;   % [C]
handles.Ekin0.nat       = 0.63;             % [eV]

% Update handles structure
guidata(hObject, handles);



function pulseLengthEdit_Callback(hObject, eventdata, handles)
% hObject    handle to pulseLengthEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pulseLengthEdit as text
%        str2double(get(hObject,'String')) returns contents of pulseLengthEdit as a double

set(hObject, 'Callback',@kinematicsEdit_Callback);


% --- Executes during object creation, after setting all properties.
function pulseLengthEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pulseLengthEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in screenshotButton.
function screenshotButton_Callback(hObject, eventdata, handles)
% hObject    handle to screenshotButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

filepath = fullfile(handles.rp.workingPath, handles.rp.runFolderName, ['screenshot_' handles.rp.list(handles.simIndex).simFilebase '.png']);
% Options for gui printing
%saveas(gcf, filepath, 'png');
print(gcf, '-dpng', '-r0', filepath);


function simIndex = findSimIndex(handles, sliderIndices)

simIndex = 0;
for ii = 1:handles.rp.active.combinationsNo
    %fprintf('next iteration\n')
    for jj = 1:handles.rp.active.no
        desiredValue = handles.rp.(handles.rp.active.name{jj}).nom + handles.rp.(handles.rp.active.name{jj}).var(sliderIndices(jj));
        searchValue = handles.rp.list(ii).(handles.rp.active.name{jj});
        if abs(searchValue - desiredValue) > 1E-12
            break;
        elseif jj == handles.rp.active.no
            simIndex = ii;
        end
    end
    if simIndex > 0
        break;
    end
end


% --- Executes on button press in fieldAtBackplateToggle.
function fieldAtBackplateToggle_Callback(hObject, eventdata, handles)
% hObject    handle to fieldAtBackplateToggle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of fieldAtBackplateToggle

if get(hObject, 'Value')
    % Open sf7 file with the field data
    sf7Filename = [handles.rp.list(handles.simIndex).simFilebase '_radial.OUT7'];
    [handles.sf7DataRadial, fileOk] = read_data_file(handles.pathname, sf7Filename, 'sf7');
    if ~fileOk
        warndlg(['File ' sf7Filename ' not found or does not contain valid data!']);
        % Clear plot
        cla(handles.fieldAtBackplateAxes);
    else
        % Plot maximal electric field at the cathode along the radial direction
        axes(handles.fieldAtBackplateAxes);
        plot_fieldAlongRadius(handles.sf7DataRadial);
    end
else
    % Clear plot
    cla(handles.fieldAtBackplateAxes);
end
    
    
function [] = plot_fieldAlongRadius(simData)

% Plot the total electric field at the cathode along the radius
plot(simData.r, simData.Etot/simData.Etot(1), 'b');
xlabel('r [cm]');
ylabel('|E| [normalized to the on-axis field]');
legend('Selected simulation', 'Location','SouthWest');
% Show the region of the cathode plug only
axis([0.0 1.0 0.0 2.0]);


% --- Executes on button press in compareButton_freq.
function compareButton_freq_Callback(hObject, eventdata, handles)
% hObject    handle to compareButton_freq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in compareButton_Q.
function compareButton_Q_Callback(hObject, eventdata, handles)
% hObject    handle to compareButton_Q (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in compareButton_shuntImp.
function compareButton_shuntImp_Callback(hObject, eventdata, handles)
% hObject    handle to compareButton_shuntImp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in compareButton_EmaxSurf.
function compareButton_EmaxSurf_Callback(hObject, eventdata, handles)
% hObject    handle to compareButton_EmaxSurf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in compareButton_HmaxSurf.
function compareButton_HmaxSurf_Callback(hObject, eventdata, handles)
% hObject    handle to compareButton_HmaxSurf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton13.
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function initialEnergyEdit_Callback(hObject, eventdata, handles)
% hObject    handle to initialEnergyEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of initialEnergyEdit as text
%        str2double(get(hObject,'String')) returns contents of initialEnergyEdit as a double
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject, 'Callback',@kinematicsEdit_Callback);


% --- Executes during object creation, after setting all properties.
function initialEnergyEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to initialEnergyEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
