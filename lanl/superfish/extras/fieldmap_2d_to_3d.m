%%%%% SCRIPT fieldmap_2d_to_3d.m %%%%%
% Mattia Schaer
% November 2012
%
% This script converts 2D fieldmaps of SuperFish to 3D, rotational symmetric fieldmaps for ASTRA 


clear all;


%----- USER INPUT -----

% Grid resolutions
% longitudinal resolution of the SuperFish file [mm]
% will define the longitudinal resolution for the 3D fieldmaps [mm]
longRes = 1.0;
% define transversal resolution for the 3D fieldmaps [mm]
transvRes = 1.0;
% define the transversal dimension of the map [mm]
transvMax = 50.0;

% SuperFish file
superfishFilepath = '/afs/psi.ch/project/newgun/rf/PSIGun1/superfish/OUTSF7_longRes1.0mm_transvRes0.1mm.TXT';
% number of grid points in radial direction
rValues = 429;
% number of grid points in longitudinal direction
zValues = 176;
% invert field signs (to always have positive Ez field at the cathode
invertFieldSigns = true;
% normalization constants for the 3D fieldmaps
% grid points normalization to get [m]
% 1:    SuperFish output in [m]
% 100: SuperFish output in [cm]
% 1000: SuperFish output in [mm]
normL = 1000;

% superfishFilepath = '/afs/psi.ch/project/newgun/rf/CTF2Gun5/superfish/beadpullMatched/beadpullMatch_2_1_2D.OUT7';
% rValues = 98;
% zValues = 196;
% invertFieldSigns = false;
% normL = 100;

% Output file
generateAstraFile = true;
astraFilebase = '3D_fieldmap_gapEffect_15_2_new';
%astraFilebase = '3D_fieldmap_beadpullMatch_2_1_new';

%----- USER INPUT -----

astraFileExt = {'ex' ; 'ey' ; 'ez' ; 'bx' ; 'by' ; 'bz'};

% import SuperFish file
inData = read_sf7_outputfile(superfishFilepath, 'superfish');

% read 2D points at which fields are known
z = inData.z(1:zValues)' / normL;   % [m]
r = inData.r(1:zValues:rValues*zValues)' / normL;   % [m]
% extend the radius of the map
rStep = r(2)-r(1);   % [m]
rExtend = r(end)+rStep:rStep:transvMax/1000;   % [m]
r = [r ; rExtend'];

% define grid points at which we want to compute the fields
xMin = -r(end);   % [m]
xMax = r(end);   % [m]
xStep = transvRes/1000;   % [mm]
x = xMin:xStep:xMax;
y = x;

% Parameters to plot 2D distribution
yIndex = ceil(numel(y)/2);   % index to fix the y-coordinate
ind = 1;

if generateAstraFile
    [filepath, ~, ~] = fileparts(superfishFilepath);
    for i = 1:length(astraFileExt)
        outFile(i) = fopen(fullfile(filepath, [astraFilebase '.' astraFileExt{i}]), 'w'); %#ok<SAGROW>

        % write the grid coordinates in the Astra input file
        fprintf(outFile(i), '%d', length(x));
        fprintf(outFile(i), ' %.5e', x');
        fprintf(outFile(i), '\n');
        fprintf(outFile(i), '%d', length(y));
        fprintf(outFile(i), ' %.5e', y');
        fprintf(outFile(i), '\n');
        fprintf(outFile(i), '%d', length(z));
        fprintf(outFile(i), ' %.5e', z');
        fprintf(outFile(i), '\n');
    end
end

% initialize the field vectors and
% extend them according to the extension of r (no field outside the cavity)
Er  = zeros(size(r));
Ez  = zeros(size(r));
H   = zeros(size(r));

% scale E with maximum electric field max(E)
% normE = max(inData.Etot);   % [MV/m]
% scale E with maximum electric field on-axis Ez
normE = max(abs(inData.Ez(1:zValues)));   % [MV/m]
% scale H field with the same factor, unit of E/H must be [V/m/T]
mu0 = 4*pi * 10^(-7);   % vacuum permittivity [Tm/A]
normH = 10^6*normE / mu0;

fieldSign = 1; %#ok<NASGU>
% invert field signs
if invertFieldSigns
    fieldSign = -1;
end

for k = 1:length(z)
    for j = 1:length(y)
        % read known fields at the given z
        Er(1:rValues) = fieldSign * inData.Er(k:zValues:rValues*zValues)' / normE;
        Ez(1:rValues) = fieldSign * inData.Ez(k:zValues:rValues*zValues)' / normE;
        H(1:rValues)  = fieldSign * inData.H(k:zValues:rValues*zValues)' / normH;
        
        % find the cylindrical coordinates corresponding to the desired points on the grid
        yRep = repmat(y(j), size(x));
        [theta rho] = cart2pol(x, yRep);
        
        % Electric field [MV/m]
        % interpolate to get the radial field component at the desired points
        Erho = interp1(r, Er, rho');
        % set NaN values to 0.0
        Erho(isnan(Erho)) = 0.0;
        % radial electric field components at the desired grid points
        Ex = Erho .* cos(theta');
        Ey = Erho .* sin(theta');
        % extend longitudinal field component to negative values
        rExt = [-r(end:-1:2) ; r];
        EzExt = [Ez(end:-1:2) ; Ez];
        % interpolate to get the longitudinal field component at the desired points
        %Ezet = interp1(rExt, EzExt, sqrt(x.^2+yRep.^2)');
        Ezet = interp1(r, Ez, sqrt(x.^2+yRep.^2)');
        % set NaN values to 0.0
        Ezet(isnan(Ezet)) = 0.0;
        
        % Magnetic field [A/m]
        % interpolate to get the radial field component at the desired points
        Hrho = interp1(r, H, rho');
        % set NaN values to 0.0
        Hrho(isnan(Hrho)) = 0.0;
        % radial magnetic field components at the desired grid points
        Hx = Hrho .* cos(theta'+pi/2);
        Hy = Hrho .* sin(theta'+pi/2);
        % no longitudinal magnetic field component
        Hz = zeros(length(x),1);

        if generateAstraFile
            % write fields to files (NORMALIZED!)
            fprintf(outFile(1), '%12.5E ', Ex);
            fprintf(outFile(1), '\n');
            fprintf(outFile(2), '%12.5E ', Ey);
            fprintf(outFile(2), '\n');
            fprintf(outFile(3), '%12.5E ', Ezet);
            fprintf(outFile(3), '\n');
            fprintf(outFile(4), '%12.5E ', Hx);
            fprintf(outFile(4), '\n');
            fprintf(outFile(5), '%12.5E ', Hy);
            fprintf(outFile(5), '\n');
            % workaround
            tmpString = sprintf('%12.5E ', Hz);
            fprintf(outFile(6), '%s\n', tmpString);
        end
        
        % save data for check plot
        if j == yIndex
            Eplot1(:,ind) = Ezet;
            Eplot2(:,ind) = Ex;
            Eplot3(:,ind) = Hy;
            ind = ind+1;
        end
    end
end

% generate check plot
figure(1);
imagesc(Eplot1);
set(gca, 'DataAspectRatio',[transvRes longRes 1]);
colorbar;
title('Ez [norm.]');
xlabel('z [a.u.]');
ylabel('x [a.u.]');
figure(2);
imagesc(Eplot2);
set(gca, 'DataAspectRatio',[transvRes longRes 1]);
colorbar;
title('Ex [norm.]');
xlabel('z [a.u.]');
ylabel('x [a.u.]');
figure(3);
imagesc(Eplot3);
set(gca, 'DataAspectRatio',[transvRes longRes 1]);
colorbar;
title('Hy [norm.]');
xlabel('z [a.u.]');
ylabel('x [a.u.]');

if generateAstraFile
    for i = 1:numel(astraFileExt)
        fclose(outFile(i));
    end
end
