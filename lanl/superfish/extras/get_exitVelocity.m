%%% FUNCTION get_exitVelocity.m %%%
% Mattia Schaer
% July 2013
%
% Compute exit velocity (i.e. energy) at each cell
%
% CALL [exitVelocities, energy] = get_exitVelocity(sf7Data, Emax, electricalLengths)
%
%           sf7Data:            float N x 2 , (Nth entry, :) = [ Z , Ez ]
%           Emax:               float
%           electricalLengths   float Ncells
%           exitVelocities:     float Ncells
%           energy:             float N x 2 , (Nth entry, :) = [ Z , Energy]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function kinematics = get_exitVelocity(handles)
 
    % Define start times of particles to track
    % head particle
    startTimes(1) = 0.0;
    % central particle
    startTimes(2) = handles.pulseLength * 1E-12 / 2;
    % tail particle
    startTimes(3) = handles.pulseLength * 1E-12;
    % colors for plotting
    pltColor = {'g', 'b', 'r'};
    
    % Prepare variables required for integration
    % initial position
    particle.x0 = handles.rp.list(handles.simIndex).xCathode / 100;         % [m]
    % kinetic energy of electrons coming out from the cathode
    particle.Ekin0      = handles.Ekin0;                                    % [eV]
    % electron mass
    particle.m.nat      = handles.mElectron.nat;                            % [eV/c^2]
    particle.m.si       = handles.mElectron.si;                             % [kg]
    % electron charge
    particle.q          = handles.e.si;                                     % [C]
    % Field
    Efield.fieldType    = 'real';
    Efield.z            = handles.sf7Data.z * 1E-2;                         % [m]
    Efield.Ez           = handles.sf7Data.EzNorm;                           % [normalized]
    Efield.Emax         = handles.Emax * 1E6;                               % [V/m]
    Efield.freq         = handles.fishData.freq * 1E6;                      % [Hz]
    Efield.phase0       = handles.gunPhase;                                 % [deg]
    
    for ii = 1:numel(startTimes)
        % initial time for current particle
        particle.t0 = startTimes(ii);                                   % [s]
        % Integrate
        kinematics(ii) = integrate_eqom_Efield_1d(Efield, particle);

        % Plot results
        axes(handles.betaAxes);
        plot(kinematics(ii).z,kinematics(ii).beta, pltColor{ii});
        hold on;
        axes(handles.EkinAxes);
        plot(kinematics(ii).z,kinematics(ii).Ekin, pltColor{ii});
        hold on;
        axes(handles.expFieldAxes);
        plot(kinematics(ii).z, kinematics(ii).expEz, pltColor{ii});
        hold on;
    end
    
    xLim = kinematics(1).z([1 end]);
    axes(handles.betaAxes);
    xlabel('z [cm]');
    ylabel('beta [1/c]');
    legend('head', 'center', 'tail', 'Location','SouthEast');
    xlim(xLim);
    hold off;
    axes(handles.EkinAxes);
    xlabel('z [cm]');
    ylabel('Ekin [MeV]');
    legend('head', 'center', 'tail', 'Location','NorthWest');
    xlim(xLim);
    hold off;
    axes(handles.expFieldAxes);
    plot(xlim, [0 0], 'k:');
    xlabel('z [cm]');
    ylabel('Experienced Ez [MV/m]');
    legend('head', 'center', 'tail');
    xlim(xLim);
    hold off;
    
    % Extract desired data from the integration results
    electricPosition = 0.0;
    cellEnterTime = kinematics(1).t(1);
    for ii = 1:handles.rp.cellsNum.nom
        % cell exit velocity
        electricPosition = electricPosition + handles.cellLengths.electric(ii);
        kinematics(1).exitVelocity(ii) = interp1(kinematics(1).z, kinematics(1).beta, electricPosition);
        % cell exit energy
        kinematics(1).exitEnergy(ii)   = interp1(kinematics(1).z, kinematics(1).Ekin, electricPosition);
        % cell transit time
        kinematics(1).transitTime(ii)  = interp1(kinematics(1).z, kinematics(1).t, electricPosition) - cellEnterTime;
        cellEnterTime = interp1(kinematics(1).z, kinematics(1).t, electricPosition);
    end
    
end
