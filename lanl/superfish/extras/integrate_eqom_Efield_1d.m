function kinematic = integrate_eqom_Efield_1d(Efield, particle)

global constVars valForEqom;


% Other required values
valForEqom.z            = Efield.z;
valForEqom.freq         = Efield.freq;
valForEqom.phase0       = Efield.phase0;
valForEqom.m            = particle.m.si;
valForEqom.q            = particle.q;
valForEqom.fieldType    = Efield.fieldType;
switch Efield.fieldType
    case 'real'
        valForEqom.Ez       = Efield.Ez;
        valForEqom.Emax     = Efield.Emax;
        % Prompt a warning if the fieldmap is not normalized
        if max(valForEqom.Ez) > 1.0
            fprintf('WARNING: the profile of the field provided to integrate_eqom_Efield_1d() is not normalized!\n');
            fprintf('\t\t max(Efield.Ez) = %f\n', max(valForEqom.Ez));
        end
    case 'complex'
        valForEqom.EzRe     = Efield.EzRe;
        valForEqom.EzIm     = Efield.EzIm;
        valForEqom.Emax     = Efield.Emax;
end

% Time discretization
% time at which to stop the integration
tFinal = 0.8E-9;   % [s]
% integration step
tStep = 0.01E-12;   % [s]
t = particle.t0:tStep:tFinal;
% Vector of initial conditions
[~, ~, p0] = Ekin_to_p(particle.Ekin0, particle.m.nat);
p0 = p0 * constVars.qElectron.si/constVars.c.si;   % [kg*m/s]
y = [p0 particle.x0];
[T, Y] = ode45(@eqom_Efield_1d, t, y);

% Store computed data
% indices going up to the end of provided Ez fieldmap
ind = Y(:, 2)' < Efield.z(end);
% if the integration does not extend to the end of the fieldmap: warning message
if Y(end, 2) < Efield.z(end);
    fprintf('The integration of the eqom extended only up to %f cm which is less than the fieldmap extension %f cm\n', Y(end, 2), Efield.z(end));
end
kinematic.t = T(ind);                       % [s]
kinematic.z = Y(ind, 2)' *1E2;              % [cm]
kinematic.p = Y(ind, 1)' *constVars.c.si/constVars.qElectron.si;              % [eV/c]
[kinematic.beta, kinematic.gamma, kinematic.Ekin] = p_to_Ekin(kinematic.p, particle.m.nat);
kinematic.Ekin = kinematic.Ekin / 1E6;      % [MeV]
% Compute experienced electric field
phase = 2*pi*Efield.freq*T(ind) + Efield.phase0*pi/180;
switch Efield.fieldType
    case 'real'
        kinematic.expEz = interp1(Efield.z, Efield.Ez, Y(ind, 2)) .* cos(phase) * Efield.Emax/1E6;   % [MeV]
    case 'complex'
        kinematic.expEz = (interp1(Efield.z, Efield.EzRe, Y(ind, 2)) .* cos(phase) - ...
                           interp1(Efield.z, Efield.EzIm, Y(ind, 2)) .* sin(phase)) ...
                          / Efield.EzRe(1) * Efield.Emax / 1E6;   % [MeV]
end

end
