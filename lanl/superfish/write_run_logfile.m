%%%%% FUNCTION write_run_logfile.m %%%%%
% Mattia Schaer
% January 2013
%
% Generate a log file in the run folder summarizing the parameters used in
% each simulation
%
% CALL write_run_logfile(folderPrefix, runNo, param, simulationList)
%           folderPrefix:   string
%           runNo:          int
%           param:          struct(Nparameters)
%           simulationList: float(rangeParameter1*...*rangeParameterN, Nparameters)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [] = write_run_logfile(folderPrefix, runNo, param, simulationList)

    % Create file
    logFilename = ['log_' folderPrefix '_' num2str(runNo) '.txt'];
    fout = fopen(logFilename, 'w');
    
    % Print header
    % run ID
    fprintf(fout, 'Run ID: %s_%d\n\n', folderPrefix, runNo);
    % parameters definitions
    for jj = 1:length(param)
        % name
        fprintf(fout, 'Parameter %d: %s\n', jj, param(jj).name);
        % active true / active false
        fprintf(fout, 'State: ');
        if param(jj).active
            fprintf(fout, 'dynamic\n');
        else
            fprintf(fout, 'static\n');
        end
        % values
        fprintf(fout, 'Values: ');
        fprintf(fout, '%f ', param(jj).values);
        fprintf(fout, '\n');
        % unit
        fprintf(fout, 'Unit: [%s]\n\n', param(jj).unit);
    end
    
    % Print table of values
    % table header
    fprintf(fout, '         Iteration #');
    fprintf(fout, '       Parameter %3d', 1:length(param));
    fprintf(fout, '\n');
    % prepare fprintf string for values
    valuesString = '%20d';
    for jj = 1:length(param)
        valuesString = [valuesString '%20.6f'];
    end
    % print values
    fprintf(fout, [valuesString '\n'], [1:size(simulationList,1) ; simulationList']);
    
    % Close file
    fclose(fout);
    
end
