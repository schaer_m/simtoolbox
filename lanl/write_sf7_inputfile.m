%%%%% FUNCTION write_sf7_inputfile.m %%%%%
% Mattia Schaer
% January 2013
%
% Generate the input file for the SuperFish utility SF7
%
% CALL write_sf7_inputfile(superfishFilebase, type, ranges, steps)
%           superfishFilebase:      string
%           type:                   string      {'Line'}
%           ranges:                 float
%                   with 'Line':    [xMin yMin xMax yMax]
%           steps
%                   with 'Line':    [xSteps]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [] = write_sf7_inputfile(superfishFilebase, type, ranges, steps)

    outFile = fopen([superfishFilebase '.IN7'], 'w');
    fprintf(outFile, '%s\n', type);
    fprintf(outFile, '%f ', ranges');
    fprintf(outFile, '\n%d\n', steps);
    fprintf(outFile, 'End');
    fclose(outFile);
    
end
