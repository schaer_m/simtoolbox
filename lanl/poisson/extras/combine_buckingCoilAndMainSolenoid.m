%%%%% FUNCTION combine_buckingCoilAndMainSolenoid.m %%%%%
% Mattia Schaer
% January 2014
%
% Combine the field distributions of bucking coil and main solenoid in
% order to have no field at the cathode and avoiding collisions
%
% CALL 
%
%%%%%



function combine_buckingCoilAndMainSolenoid(sf7DataBucking, sf7DataMain, outOptions, varargin)

    % Output options
    activateDisplay = true;

    % Redout input values
    if nargin > 3
        % 4th and 5th input arguments are to specify the mechanical length
        % and position of the magnets, in the coordinate system of the
        % original simulation (before any shift)
        mechBackZbucking    = varargin{1}(1);
        mechFrontZbucking   = varargin{1}(2);
        mechBackZmain       = varargin{2}(1);
        mechFrontZmain      = varargin{2}(2);
    end
    if nargin > 5
        % 6th input argument defines the position of the front mechanical
        % edge of the bucking solenoid
        zShiftBucking = varargin{3} - mechFrontZbucking;
    else
        % Find the longitudinal position at which Bz is maximum
        [~, maxBzBuckingInd] = max(sf7DataBucking.Bz);
        % Shift bucking coil field so that the maximum is at the cathode (z=0)
        zShiftBucking = -sf7DataBucking.z(maxBzBuckingInd);
    end
    
    % Shift bucking coil field by the determined value
    fprintf('ASTRA position of bucking coil: %f\n', zShiftBucking);
    sf7DataBucking.z = sf7DataBucking.z + zShiftBucking;
    % make it negative
    sf7DataBucking.Bz       = -sf7DataBucking.Bz;
    
    % Find th longitudinal position at which the Bz of the main solenoid is
    % equal (and opposite) to the Bz of the bucking coil at the cathode
    cathodeBzBuckingCoil = interp1(sf7DataBucking.z, sf7DataBucking.Bz, 0.0);
    endSearchRange = fix(numel(sf7DataMain.z)/2) - 1;
    zShiftMain = -interp1(sf7DataMain.Bz(1:endSearchRange), sf7DataMain.z(1:endSearchRange), -cathodeBzBuckingCoil);
    % Shift the main solenoid so that the two fields perfectly compensate
    fprintf('ASTRA position of main solenoid: %f\n', zShiftMain);
    sf7DataMain.z = sf7DataMain.z + zShiftMain;
    
    % Shift the mechanical edges by the determined values
    mechBackZbucking    = mechBackZbucking + zShiftBucking;
    mechFrontZbucking   = mechFrontZbucking + zShiftBucking;
    mechBackZmain       = mechBackZmain + zShiftMain;
    mechFrontZmain      = mechFrontZmain + zShiftMain;
    
    % Combine the two fieldmaps
    % extend with zeros the two fieldmaps
    if sf7DataMain.z(end) > sf7DataBucking.z(end)
        zStepBucking            = sf7DataBucking.z(2) - sf7DataBucking.z(1);
        nPointsToAddBucking     = ceil((sf7DataMain.z(end)-sf7DataBucking.z(end)) / zStepBucking);
        sf7DataBucking.z(end+1:end+nPointsToAddBucking)         = sf7DataBucking.z(end)+zStepBucking:zStepBucking:sf7DataBucking.z(end)+nPointsToAddBucking*zStepBucking;
        sf7DataBucking.Bz(end+1:end+nPointsToAddBucking)        = 0.0;
    end
    if sf7DataBucking.z(1) < sf7DataMain.z(1);
        zStepMain               = sf7DataMain.z(2) - sf7DataMain.z(1);
        nPointsToAddMain        = ceil((sf7DataMain.z(1)-sf7DataBucking.z(1)) / zStepMain);
        sf7DataMain.z               = [sf7DataMain.z(1)-nPointsToAddMain*zStepMain:zStepMain:sf7DataMain.z(1)-zStepMain sf7DataMain.z];
        sf7DataMain.Bz              = [zeros(1,nPointsToAddMain) sf7DataMain.Bz];
    end
    outputData.z        = min(sf7DataBucking.z) : outOptions.zStep*1E2 : max(sf7DataMain.z);   % [cm]
    outputData.Bz       = interp1(sf7DataBucking.z, sf7DataBucking.Bz, outputData.z);
    outputData.Bz       = outputData.Bz + interp1(sf7DataMain.z, sf7DataMain.Bz, outputData.z);
    outputData.BzNorm   = outputData.Bz / max(abs(outputData.Bz));
    
    % Sanity check: if Bz at the cathode is not vanishing, printout warning
    zeroBzTolerance = 1E-3;   % [G] = [1E-4 T]
    cathodeBzCombined = interp1(outputData.z, outputData.Bz, 0.0);
    if abs(cathodeBzCombined - 0.0) > zeroBzTolerance
        fprintf('WARNING: Bz at the cathode is not vanishing (%f G)!!!\n', cathodeBzCombined);
    end
    
    % Generate fieldmap of the combined fields to use for tracking
    if outOptions.writeFm
        write_fieldmap_1D(outputData, 'poisson', outOptions, 'solenoid');
    end
    
    if activateDisplay
        hFig = findobj('Type','figure', 'Name',mfilename());
        if isempty(hFig)
            hFig = figure('Name',mfilename(), 'NumberTitle','off');
        end
        figure(hFig);
        subplot(2,1,1);
        % Cathode
        yLim = get(gca, 'YLim');
        hLegend(1) = plot([0.0 0.0], yLim, 'k-', 'LineWidth',2);
        hold on;
        % Bucking coil field
        hLegend(2) = plot(sf7DataBucking.z, sf7DataBucking.Bz, 'g-');
        % Main solenoid field
        hLegend(3) = plot(sf7DataMain.z, sf7DataMain.Bz, 'b-');
        % Combination
        % absolute values
        hLegend(4) = plot(outputData.z, outputData.Bz, 'r-', 'LineWidth',2);
        xlabel('z [cm]');
        ylabel('Bz [G]');
        legendStr = {'Cathode mech. position', 'Bucking coil Bz', 'Main solenoid Bz', 'Combined Bz'};
        legend(legendStr);
        hold off;
        % normalized values
        subplot(2,1,2);
        plot(outputData.z, outputData.BzNorm, 'r:');
        xlabel('z [cm]');
        ylabel('Bz [norm.]');
    end

    if nargin > 3
        % Printout mechanical distance between the two magnets
        fprintf('Mechanical magnet distance = %f cm\n\tBucking coil front edge at\t%f cm\tMain solenoid back edge at\t%f cm\n', mechBackZmain-mechFrontZbucking, mechFrontZbucking, mechBackZmain);
        %Check for collision
        if mechFrontZbucking > mechBackZmain
            fprintf('WARNING: Collision!!!\n');
        end
        if activateDisplay
            subplot(2,1,1)
            hold on;
            hLegend(5) = plot(repmat(mechBackZbucking,1,2), yLim, '--g', 'LineWidth',2);
            plot(repmat(mechFrontZbucking,1,2), yLim, '--g', 'LineWidth',2);
            hLegend(6) = plot(repmat(mechBackZmain,1,2), yLim, '--b', 'LineWidth',2);
            plot(repmat(mechFrontZmain,1,2), yLim, '--b', 'LineWidth',2);
            legendStr =[legendStr, {'Bucking coil mech. edges', 'Main solenoid mech. edges'}];
            legend(hLegend, legendStr);
            hold off;
        end
    end
    
end
