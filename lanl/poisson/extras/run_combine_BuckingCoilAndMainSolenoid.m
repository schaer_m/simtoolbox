%%%%% SCRIPT run_combine_buckingCoilAndMainSolenoid.m %%%%%
% Mattia Schaer
% February 2014
%
% Run the function which combines the bucking coil and main solenoid
% fields.
%
%%%%%



clear all;


%% My Poisson Simulations

buckingCoilFilepath     = 'C:\Documents and Settings\schaer_m\lanlRuns\gunBuckingCoil\CBand-noEdgeCut_3\CBand-noEdgeCut_3_1.OUT7';
buckingCoilMechEdges    = [-4.5 0.0];   % [cm]

% Short solenoid
mainSolenoidFilepath    = 'C:\Documents and Settings\schaer_m\lanlRuns\gunSolenoid\CBand-main-length_11\CBand-main-length_11_1.OUT7';
mainSolenoidMechEdges   = [-6.08 6.08];   % [cm]

% Long solenoid
% mainSolenoidFilepath    = 'C:\Documents and Settings\schaer_m\lanlRuns\gunSolenoid\CBand-main-length_4\CBand-main-length_4_1.OUT7';
% mainSolenoidMechEdges   = [-14.83 14.83];   % [cm]

setBuckingCoilFrontEdge = -1.0;   % [cm]

sf7DataBucking  = read_sf7_outputfile(buckingCoilFilepath, 'poisson');
sf7DataMain     = read_sf7_outputfile(mainSolenoidFilepath, 'poisson');

outOptions.zStep    = 1E-3;   % [m]
outOptions.writeFm  = true;
outOptions.filepath = 'X:\project\newgun\fieldmaps\combinedGunSol_CBand_1';

combine_buckingCoilAndMainSolenoid(sf7DataBucking, sf7DataMain, outOptions, buckingCoilMechEdges, mainSolenoidMechEdges);
% combine_buckingCoilAndMainSolenoid(sf7DataBucking, sf7DataMain, outOptions, buckingCoilMechEdges, mainSolenoidMechEdges, setBuckingCoilFrontEdge);


%% Han Gun

rawBuckingCoil  = load('/afs/psi.ch/project/newgun/fieldmaps/ORIGINAL_HanGun/buck_sol17.dat');
buckingCoil.z   = rawBuckingCoil(:,1)' * 100;   % [cm]
buckingCoil.Bz  = rawBuckingCoil(:,2)';   % [G]
buckingCoilMechEdges = [-11.7 -3.0];   % [cm]
rawMainSolenoid = load('/afs/psi.ch/project/newgun/fieldmaps/ORIGINAL_HanGun/main_sol12.dat');
mainSolenoid.z  = rawMainSolenoid(:,1)' * 100;   % [cm]
mainSolenoid.Bz = rawMainSolenoid(:,2)';   % [G]
mainSolenoidMechEdges   = [-6.75 6.75];   % [cm]

setBuckingCoilFrontEdge = -3.0;   % [cm]

outOptions.zStep    = 1E-3;   % [m]
outOptions.writeFm  = true;
outOptions.filepath = '/afs/psi.ch/project/newgun/fieldmaps/ORIGINAL_HanGun/combinedGunSol_Han';

%combine_buckingCoilAndMainSolenoid(buckingCoil, mainSolenoid, outOptions, buckingCoilMechEdges, mainSolenoidMechEdges);
combine_buckingCoilAndMainSolenoid(buckingCoil, mainSolenoid, outOptions, buckingCoilMechEdges, mainSolenoidMechEdges, setBuckingCoilFrontEdge);
