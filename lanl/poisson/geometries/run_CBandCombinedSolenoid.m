%%%%% SCRIPT run_CBandCombinedSolenoid.m %%%%%
% Mattia Schaer
% February 2014
%
% Run multiple Poisson simulations of a combined solenoid for a C-band gun
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clearvars -global runParam;
global runParam;


% Force user to check parameters before starting simulations
runParam.forceParamCheck = true;

% Working on which machine?
runParam.machineName = 'localWin';

% Prefix for this run
runParam.folderPrefix = 'CBand';

% Geometry function
runParam.geometryFunction = 'combinedSolenoid';

% Solver
runParam.solver = 'poisson';


% Geometry related parameters

runParam.meshSizeX.nom              = 0.1;
runParam.meshSizeX.unit             = 'cm';
runParam.meshSizeX.var              = [];

runParam.Rin.nom                    = 6.5;
runParam.Rin.unit                   = 'cm';
runParam.Rin.var                    = [];

runParam.buckingCoilL.nom           = 5.0;
runParam.buckingCoilL.unit          = 'cm';
runParam.buckingCoilL.var           = []; %0.0:1.0:6.0;

runParam.mainCoilL.nom              = 20.0;
runParam.mainCoilL.unit             = 'cm';
runParam.mainCoilL.var              = []; %0.0:1.0:6.0;

runParam.coilSep.nom                = 1.0;
runParam.coilSep.unit               = 'cm';
runParam.coilSep.var                = []; %0.0:1.0:6.0;

runParam.coilR.nom                  = 7.5;
runParam.coilR.unit                 = 'cm';
runParam.coilR.var                  = [];

runParam.buckingCoilCurrent.nom     = -15000;
runParam.buckingCoilCurrent.unit    = 'A';
runParam.buckingCoilCurrent.var     = [];

runParam.mainCoilCurrent.nom        = 75000;
runParam.mainCoilCurrent.unit       = 'A';
runParam.mainCoilCurrent.var        = [];

runParam.sepLong.nom                = 0.33;
runParam.sepLong.unit               = 'cm';
runParam.sepLong.var                = [];

runParam.sepRad.nom                 = 0.33;
runParam.sepRad.unit                = 'cm';
runParam.sepRad.var                 = [];

runParam.ferThickLong.nom           = 2.0;
runParam.ferThickLong.unit          = 'cm';
runParam.ferThickLong.var           = [];

runParam.ferThickRad.nom            = 2.0;
runParam.ferThickRad.unit           = 'cm';
runParam.ferThickRad.var            = [];

runParam.ferOffsetFront.nom         = 0.3;
runParam.ferOffsetFront.unit        = 'cm';
runParam.ferOffsetFront.var         = [];

runParam.ferOffsetBack.nom          = 0.3;
runParam.ferOffsetBack.unit         = 'cm';
runParam.ferOffsetBack.var          = [];


% Step size [cm] for the output fieldmap
runParam.sf7.stepSize = 0.1;


% No optimizer implemented yet
runParam.optimize.start = false;


lanl_run();
 