%%%%% SCRIPT run_SBandGunSolenoid.m %%%%%
% Mattia Schaer
% August 2013
%
% Run multiple Poisson simulations of the S-band solenoid
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clearvars -global runParam;
global runParam;


% Force user to check parameters before starting simulations
runParam.forceParamCheck = true;

% Working on which machine?
runParam.machineName = 'localWin';

% Simulation type
runParam.simType = 'lanl';
% Specific procedure
runParam.procedure = 'poisson';

% Geometry function
runParam.geometryFunction = 'gunSolenoid';

% Prefix for this run
runParam.folderPrefix = 'SBand-lengths';


% Geometry related parameters

runParam.meshSizeX.nom      = 0.5;
runParam.meshSizeX.unit     = 'cm';
runParam.meshSizeX.var      = [];

runParam.coilL.nom          = 20.0;   %6.24;   % ORIGINAL
runParam.coilL.unit         = 'cm';
runParam.coilL.var          = []; %0.0:1.0:6.0;

runParam.Rin.nom        = 4;
runParam.Rin.unit       = 'cm';
runParam.Rin.var        = [];

runParam.coilR.nom       = 11.75;
runParam.coilR.unit      = 'cm';
runParam.coilR.var       = [];

runParam.coilCurrent.nom    = 110000;
runParam.coilCurrent.unit   = 'mA';
runParam.coilCurrent.var    = [];

runParam.sepLong.nom        = 0.5;
runParam.sepLong.unit       = 'cm';
runParam.sepLong.var        = [];

runParam.sepRad.nom         = 0.5;
runParam.sepRad.unit        = 'cm';
runParam.sepRad.var         = [];

runParam.ferThickLong.nom   = 3;
runParam.ferThickLong.unit  = 'cm';
runParam.ferThickLong.var   = [];

runParam.ferThickRad.nom    = 3.0;
runParam.ferThickRad.unit   = 'cm';
runParam.ferThickRad.var    = [];

runParam.ferRinOff.nom      = 0;
runParam.ferRinOff.unit     = 'cm';
runParam.ferRinOff.var      = [];

runParam.ferOffsetFront.nom   = 0;
%runParam.ferOffsetFront.nom      = 0.25;
runParam.ferOffsetFront.unit     = 'cm';
runParam.ferOffsetFront.var      = [];

runParam.ferOffsetBack.nom      = 0;
%runParam.ferOffsetBack.nom      = 0.25;
runParam.ferOffsetBack.unit     = 'cm';
runParam.ferOffsetBack.var      = [];


% Step size [cm] for the output fieldmap
runParam.sf7.stepSize = 0.1;


%% POSTPROCESSING
runParam.postprocess.start = true;
runParam.postprocess.fomIndices = [1];


%% OPTIMIZATION
runParam.optimize.start                 = false;

%runParam.optimize.algorithm = 'NLOPT_LN_NELDERMEAD';
runParam.optimize.algorithm = 'NLOPT_LN_SBPLX';
%runParam.optimize.algorithm = 'NLOPT_GN_DIRECT_L';

% Figure-of-merit to use for the optimization
runParam.optimize.fomToOpt  = 1;

% max number of simulations
runParam.optimize.MaxFunEval = 20;


%% Figure-of-merit definitions

runParam.fomDef{1}.name                     = 'plotBz';


%%
run_start(runParam);
