%%%%% SCRIPT run_CBandCombinedSolenoid_2.m %%%%%
% Mattia Schaer
% March 2014
%
% Run multiple Poisson simulations of a combined solenoid for a C-band gun
%
%%%%%



clearvars -global runParam;
global runParam;


% Force user to check parameters before starting simulations
runParam.forceParamCheck = true;

% Working on which machine?
runParam.machineName = 'localWin';

% Simulation type
runParam.simType = 'lanl';
% Specific procedure
runParam.procedure = 'poisson';

% Geometry function
runParam.geometryFunction = 'combinedSolenoid_2';

% Prefix for this run
%runParam.folderPrefix = 'forFrontCoupler';
%runParam.folderPrefix = 'forBackCoupler';
runParam.folderPrefix = 'forTwGun';


%% Poisson input parameters

runParam.meshSizeX.nom              = 0.1;
runParam.meshSizeX.unit             = 'cm';
runParam.meshSizeX.var              = [];

runParam.Rin.nom                    = 6.0;
runParam.Rin.unit                   = 'cm';
runParam.Rin.var                    = [];

runParam.buckingCoilL.nom           = 2.0;
runParam.buckingCoilL.unit          = 'cm';
runParam.buckingCoilL.var           = []; %0.0:1.0:6.0;

runParam.mainCoilL.nom              = 10.0;
runParam.mainCoilL.unit             = 'cm';
runParam.mainCoilL.var              = []; %0.0:1.0:6.0;

runParam.mainCoilR.nom              = 12.0;
runParam.mainCoilR.unit             = 'cm';
runParam.mainCoilR.var              = [];

runParam.buckingCoilR.nom           = 12.0;
runParam.buckingCoilR.unit          = 'cm';
runParam.buckingCoilR.var           = [];

% If true, set buckingCoilR equal to mainCoilR
runParam.bypassCoilR.nom            = false;
runParam.bypassCoilR.unit           = 'bool';
runParam.bypassCoilR.var            = [];

runParam.buckingCoilCurrent.nom     = -12000;
runParam.buckingCoilCurrent.unit    = 'A';
runParam.buckingCoilCurrent.var     = 0:500:5000;

runParam.mainCoilCurrent.nom        = 55250;
runParam.mainCoilCurrent.unit       = 'A';
runParam.mainCoilCurrent.var        = [];%-1000:500:1000;

% If true, set sepRad equal to sepLong
runParam.bypassSep.nom              = true;
runParam.bypassSep.unit             = 'bool';
runParam.bypassSep.var              = [];

runParam.sepLong.nom                = 0.25;
runParam.sepLong.unit               = 'cm';
runParam.sepLong.var                = [];

runParam.sepRad.nom                 = 0.25;
runParam.sepRad.unit                = 'cm';
runParam.sepRad.var                 = [];

runParam.ferThickFront.nom          = 3.0;
runParam.ferThickFront.unit         = 'cm';
runParam.ferThickFront.var          = [];

runParam.ferThickMiddle.nom         = 3.5;
runParam.ferThickMiddle.unit        = 'cm';
runParam.ferThickMiddle.var         = [];

runParam.ferThickBack.nom           = 2.0;
runParam.ferThickBack.unit          = 'cm';
runParam.ferThickBack.var           = [];

runParam.ferThickRad.nom            = 4.0;
runParam.ferThickRad.unit           = 'cm';
runParam.ferThickRad.var            = [];

runParam.ferOffsetFront.nom         = 0.25;
runParam.ferOffsetFront.unit        = 'cm';
runParam.ferOffsetFront.var         = [];

runParam.ferOffsetMiddle.nom        = 0.25;
runParam.ferOffsetMiddle.unit       = 'cm';
runParam.ferOffsetMiddle.var        = []; %0.0:0.5:2.0;

runParam.ferOffsetBack.nom          = 0.25;
runParam.ferOffsetBack.unit         = 'cm';
runParam.ferOffsetBack.var          = [];


% Step size [cm] for the output fieldmap
runParam.sf7.stepSize = 0.1;


%% POSTPROCESSING
runParam.postprocess.start = true;
runParam.postprocess.fomIndices = [1];


%% OPTIMIZATION
runParam.optimize.start                 = false;

%runParam.optimize.algorithm = 'NLOPT_LN_NELDERMEAD';
runParam.optimize.algorithm = 'NLOPT_LN_SBPLX';
%runParam.optimize.algorithm = 'NLOPT_GN_DIRECT_L';

% Figure-of-merit to use for the optimization
runParam.optimize.fomToOpt  = 1;

% max number of simulations
runParam.optimize.MaxFunEval = 20;


%% Figure-of-merit definitions

runParam.fomDef{1}.name                     = 'plotBz';


%%
run_start(runParam);
 