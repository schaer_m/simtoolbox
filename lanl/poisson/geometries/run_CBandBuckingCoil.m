%%%%% SCRIPT run_CBandBuckingCoil.m %%%%%
% Mattia Schaer
% January 2014
%
% Run multiple Poisson simulations of a bucking coil for a C-band gun
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clearvars -global runParam;
global runParam;


% Force user to check parameters before starting simulations
runParam.forceParamCheck = true;

% Working on which machine?
runParam.machineName = 'localWin';

% Prefix for this run
runParam.folderPrefix = 'CBand-noEdgeCut';

% Geometry function
runParam.geometryFunction = 'gunBuckingCoil';

% Solver
runParam.solver = 'poisson';


% Geometry related parameters

runParam.meshSizeX.nom      = 0.1;
runParam.meshSizeX.unit     = 'cm';
runParam.meshSizeX.var      = [];

runParam.Lfront.nom         = 0.0;
runParam.Lfront.unit        = 'cm';
runParam.Lfront.var         = [];

runParam.Rin.nom            = 4.5;
runParam.Rin.unit           = 'cm';
runParam.Rin.var            = [];

runParam.coilL.nom          = 2.0;
runParam.coilL.unit         = 'cm';
runParam.coilL.var          = [];

runParam.coilR.nom          = 5.0;
runParam.coilR.unit         = 'cm';
runParam.coilR.var          = [];

runParam.coilCurrent.nom    = 5000;
runParam.coilCurrent.unit   = 'A';
runParam.coilCurrent.var    = [];

runParam.sepLongFront.nom    = 0.25;
runParam.sepLongFront.unit   = 'cm';
runParam.sepLongFront.var    = [];

runParam.sepLongBack.nom     = 0.25;
runParam.sepLongBack.unit    = 'cm';
runParam.sepLongBack.var     = [];

runParam.sepRadIn.nom       = 0.25;
runParam.sepRadIn.unit      = 'cm';
runParam.sepRadIn.var       = [];

runParam.sepRadOut.nom      = 0.25;
runParam.sepRadOut.unit     = 'cm';
runParam.sepRadOut.var      = [];

runParam.ferThickLongFront.nom   = 1.0;
runParam.ferThickLongFront.unit  = 'cm';
runParam.ferThickLongFront.var   = [];

runParam.ferThickLongBack.nom   = 1.0;
runParam.ferThickLongBack.unit  = 'cm';
runParam.ferThickLongBack.var   = [];

runParam.ferThickRadIn.nom    = 1.0;
runParam.ferThickRadIn.unit   = 'cm';
runParam.ferThickRadIn.var    = [];

runParam.ferThickRadOut.nom    = 1.0;
runParam.ferThickRadOut.unit   = 'cm';
runParam.ferThickRadOut.var    = [];

runParam.cutInR.nom             = 0.5;
runParam.cutInR.unit            = 'cm';
runParam.cutInR.var             = [];

runParam.cutInL.nom             = 1.0;
runParam.cutInL.unit            = 'cm';
runParam.cutInL.var             = [];

runParam.cutFrontR.nom          = 1.0;
runParam.cutFrontR.unit         = 'cm';
runParam.cutFrontR.var          = [];

runParam.cutFrontL.nom          = 0.5;
runParam.cutFrontL.unit         = 'cm';
runParam.cutFrontL.var          = [];

runParam.cutCoilR.nom           = 0.5;
runParam.cutCoilR.unit          = '%';
runParam.cutCoilR.var           = [];

runParam.cutCoilL.nom           = 0.75;
runParam.cutCoilL.unit          = '%';
runParam.cutCoilL.var           = [];


% Step size [cm] for the output fieldmap
runParam.sf7.stepSize = 0.1;


% No optimizer implemented yet
runParam.optimize.start = false;


lanl_run();
 