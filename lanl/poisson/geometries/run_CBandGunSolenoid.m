%%%%% SCRIPT run_CBandGunSolenoid.m %%%%%
% Mattia Schaer
% January 2014
%
% Run multiple Poisson simulations of a solenoid for a C-band gun
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clearvars -global runParam;
global runParam;


% Force user to check parameters before starting simulations
runParam.forceParamCheck = true;

% Working on which machine?
runParam.machineName = 'localWin';

% Prefix for this run
runParam.folderPrefix = 'CBand-main-length';

% Geometry function
runParam.geometryFunction = 'gunSolenoid';

% Solver
runParam.solver = 'poisson';


% Geometry related parameters

runParam.meshSizeX.nom      = 0.1;
runParam.meshSizeX.unit     = 'cm';
runParam.meshSizeX.var      = [];

runParam.Rin.nom        = 4.5;
runParam.Rin.unit       = 'cm';
runParam.Rin.var        = [];

%runParam.coilL.nom          = 25.0;
runParam.coilL.nom          = 7.5;
runParam.coilL.unit         = 'cm';
runParam.coilL.var          = []; %0.0:1.0:6.0;

%runParam.coilR.nom        = 5.0;
runParam.coilR.nom        = 7.5;
runParam.coilR.unit       = 'cm';
runParam.coilR.var        = [];

%runParam.coilCurrent.nom    = 62500;
runParam.coilCurrent.nom    = 37500;
runParam.coilCurrent.unit   = 'A';
runParam.coilCurrent.var    = [];

runParam.sepLong.nom        = 0.33;
%runParam.sepLong.nom        = 0.25;
runParam.sepLong.unit       = 'cm';
runParam.sepLong.var        = [];

runParam.sepRad.nom         = 0.33;
%runParam.sepRad.nom         = 0.25;
runParam.sepRad.unit        = 'cm';
runParam.sepRad.var         = [];

runParam.ferThickLong.nom   = 2.0;
%runParam.ferThickLong.nom   = 1.0;
runParam.ferThickLong.unit  = 'cm';
runParam.ferThickLong.var   = [];

runParam.ferThickRad.nom    = 2.0;
%runParam.ferThickRad.nom    = 1.0;
runParam.ferThickRad.unit   = 'cm';
runParam.ferThickRad.var    = [];

runParam.ferOffsetFront.nom   = 0.3;
%runParam.ferOffsetFront.nom      = 0.25;
runParam.ferOffsetFront.unit     = 'cm';
runParam.ferOffsetFront.var      = [];

runParam.ferOffsetBack.nom      = 0.3;
%runParam.ferOffsetBack.nom      = 0.25;
runParam.ferOffsetBack.unit     = 'cm';
runParam.ferOffsetBack.var      = [];


% Step size [cm] for the output fieldmap
runParam.sf7.stepSize = 0.1;


% No optimizer implemented yet
runParam.optimize.start = false;


lanl_run();
 