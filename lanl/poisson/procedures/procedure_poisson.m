%%%%% FUNCTION procedure_poisson.m %%%%%
% Mattia Schaer
% March 2014
%
% Run the single Poisson simulation
%
% CALL [modifiedSimulationParameters] = procedure_poisson([actualSimulationParameters])
%           actualSimulationParameters              struct
%
%           modifiedSimulationParameters            struct
%
%%%%%



function varargout = procedure_poisson(varargin)

global runParam;


% Manage input
switch nargin
    case 0
        actualPars = runParam.list(runParam.active.simInd);
    case 1
        actualPars = varargin{1};
    otherwise
        error('Unknown sequence of input parameters');
end

% Run Automesh
fprintf('... Automesh\n');
% call geometry function to generate Poisson input file
% WARNING: with bypasses, the geometry function might change the actual parameters, therefore return them
eval(['actualPars = geometry_' runParam.geometryFunction '(actualPars);']);
exeString = [runParam.lanlExepath 'AUTOMESH.EXE ' actualPars.simFilebase '.am'];
system(exeString);
% Rename files
exeString = [runParam.moveCommand ' OUTAUT.TXT ' actualPars.simFilebase '.MESH'];
[~,~] = system(exeString);

% Run Poisson
exeString = [runParam.lanlExepath 'POISSON.EXE ' upper(actualPars.simFilebase) '.T35'];
system(exeString);
% Rename files
exeString = [runParam.moveCommand ' OUTPOI.TXT ' actualPars.simFilebase '.POI'];
[~,~] = system(exeString);
exeString = [runParam.moveCommand ' OUTPOI.TBL ' actualPars.simFilebase '.TBL'];
[~,~] = system(exeString);

% Run SF7 to get fieldmap (on-axis)
fprintf('... SF7\n');
stepNo = round((actualPars.domainLmax-actualPars.domainLmin) / runParam.sf7.stepSize);
write_sf7_inputfile(actualPars.simFilebase, 'Line', ...
                    [0.0 actualPars.domainLmin 0.0 actualPars.domainLmax], stepNo);
exeString = [runParam.lanlExepath 'SF7.EXE ' actualPars.simFilebase '.IN7 ' upper(actualPars.simFilebase) '.T35'];
system(exeString);
exeString = [runParam.moveCommand ' OUTSF7.TXT ' actualPars.simFilebase '.OUT7'];
[~,~] = system(exeString);

% Manage output
switch nargout
    case 0
        runParam.list(runParam.active.simInd) = actualPars;
    case 1
        varargout{1} = actualPars;
    otherwise
        error('Unknown sequence of output parameters');
end
    
end
