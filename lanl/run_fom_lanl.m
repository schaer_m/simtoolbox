%%%%% FUNCTION run_fom_lanl.m %%%%%
% Mattia Schaer
% March 2014
%
% Compute figure-of-merits for LANL simulations.
%
% CALL fom = run_fom_lanl(fomDefinition, actualSimulationParameters)
%           fomDefinition                       struct
%           actualSimulationParameters          struct
%
%           fom                                 float
%
%%%%%



function fom = run_fom_lanl(fomDef, actualPars)

global runParam;


% Default fom
fom = NaN;

path = [runParam.workingPath runParam.runFolderName];

switch fomDef.name
    

    % SuperFish
    case 'EmaxSurface'
        filename    = [upper(actualPars.simFilebase) '.SFO'];
        [sfoData, fileOK(1)] = read_data_file(path, filename, 'sfo');
        filename    = [actualPars.simFilebase '.OUT7'];
        [sf7Data, fileOK(2)] = read_data_file(path, filename, 'sf7');
        if sum(fileOK) == 2
            fom = sfoData.EmaxSurface*1E-6 / max(abs(sf7Data.Ez));
        else
            fom = NaN;
        end
        
    case 'radiusMargin'
        fom = actualPars.r1 - actualPars.rEdge - actualPars.b1 - actualPars.i1;
        
    case 'cathodeFieldRadial'
        % Run SF7 to get total electric field along the gun radius
        fprintf('... SF7\n');
        stepNo = round(actualPars.xEnd / runParam.sf7.stepSize);
        write_sf7_inputfile([actualPars.simFilebase '_radial'], 'Line', ...
            [actualPars.xCathode 0.0 actualPars.xCathode actualPars.r1], stepNo);
        exeString = [runParam.lanlExepath 'SF7.EXE ' actualPars.simFilebase '_radial.IN7 ' upper(actualPars.simFilebase) '.T35'];
        system(exeString);
        exeString = [runParam.moveCommand ' OUTSF7.TXT ' actualPars.simFilebase '_radial.OUT7'];
        [~,~] = system(exeString);
        
    case 'fieldBalance'
        filename    = [actualPars.simFilebase '.OUT7'];
        [sf7Data, fileOK] = read_data_file(path, filename, 'sf7');
        if fileOK
            fieldAtCathode = sf7Data.EzNorm(1);
            peakFields = get_extrema({sf7Data}, 'EzNorm', 'max', [], true, false);
            peakFields = [peakFields get_extrema({sf7Data}, 'EzNorm', 'min', [], true, true)];
            if numel(peakFields) == actualPars.cellsNum-1
                fom = abs(fieldAtCathode);
                for ii = 1:numel(peakFields)
                    fom = fom + abs(peakFields(ii).EzNorm);
                end
                fom = 1 - fom/actualPars.cellsNum;
            else
                fom = actualPars.cellsNum;
            end
        else
            fom = NaN;
        end
        
        
    % Poisson
    
    case 'plotBz'
        filename    = [actualPars.simFilebase '.OUT7'];
        [sf7Data, fileOK] = read_data_file(path, filename, 'sf7', 'poisson');
        if fileOK
            plot(sf7Data.z, sf7Data.Bz);
            xlabel('z [cm]');
            ylabel('Bz [G]');
            hold on;
        end
        
    otherwise
        error('Unknown fomDef.name');
        
end

end
