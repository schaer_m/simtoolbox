%%%%% FUNCTION read_sf7_outputfile.m %%%%%
% Mattia Schaer
% March 2014
%
% Import data from the output file of the SuperFish program SF7
%
% CALL sf7Data = read_sf7_outputfile(filepath, solverName)
%           filepath                string
%           solverName              string
%               'superfish' OR
%               'poisson'
%
%           sf7Data                 struct(N)
%
%%%%%



function sf7Data = read_sf7_outputfile(filepath, solver)


% First and last line identifiers for the data matrix
switch solver
    case 'superfish'
        tableStartIdentifier = '|E|';
        tableEndIdentifier = 'Stored-energy and magnetic-flux integrals';
    case 'poisson'
        tableStartIdentifier = '|B|';
        tableEndIdentifier = 'Integral H dl =';
end

% Open file a first time to find the initial and final line of the data matrix
inFile = fopen(filepath);
currentLine = 0;
% find first line or error line
firstFound = false;
while ~firstFound
    tmpLine = fgetl(inFile);
    currentLine = currentLine + 1;
    % String '|E|' is present in the header of the data table
    if strfind(tmpLine, tableStartIdentifier)
        firstFound = true;
        % If SF7 outputfile exists but does not contain any data
    elseif ~isempty([strfind(tmpLine, 'does not contain a solution array') strfind(tmpLine, 'Error')])
        fclose(inFile);
        fprintf(['File ' strrep(filepath, '\', '\\') ' does not contain valid data!\n']);
        sf7Data = [];
        return;
    end
end
firstLine = currentLine + 2;
% find last line
endReached = false;
while ~endReached
    tmpLine = fgetl(inFile);
    if feof(inFile)
        lastLine = currentLine + 1;
        endReached = true;
    elseif strfind(tmpLine, tableEndIdentifier)
        lastLine = currentLine - 1;
        endReached = true;
    end
    currentLine = currentLine + 1;
end
fclose(inFile);

% Open file a second time to read in the data
inFile = fopen(filepath);
% eat the header lines
for i = 1:firstLine-1
    tmpLine = fgetl(inFile);
end

% Import the values matrix
switch solver
    case 'superfish'
        bufferData = fscanf(inFile, '%f %f %f %f %f %f', [6 lastLine-firstLine+1]);
        % Reored raw data into a sf7Data matrix
        sf7Data.z       = bufferData(1, :);
        sf7Data.r       = bufferData(2, :);
        sf7Data.Ez      = bufferData(3, :);
        sf7Data.Er      = bufferData(4, :);
        sf7Data.Etot    = bufferData(5, :);
        sf7Data.H       = bufferData(6, :);
        % Normalize Ez and make it positive at the cathode (this is the most used quantity
        sf7Data.EzNorm = sf7Data.Ez / max(abs(sf7Data.Ez));
        if sf7Data.EzNorm(1) < 0
            sf7Data.EzNorm = -1 * sf7Data.EzNorm;
        end
    case 'poisson'
        bufferData = fscanf(inFile, '%f %f %f %f %f %f %f %f %f', [9 lastLine-firstLine+1]);
        % Reored raw data into a sf7Data matrix
        sf7Data.r       = bufferData(1, :);
        sf7Data.z       = bufferData(2, :);
        sf7Data.Br      = bufferData(3, :);
        sf7Data.Bz      = bufferData(4, :);
        sf7Data.Btot    = bufferData(5, :);
        sf7Data.A       = bufferData(6, :);
        sf7Data.dBzdR   = bufferData(7, :);
        sf7Data.dBrdz   = bufferData(8, :);
        sf7Data.n       = bufferData(9, :);
        % Normalize Bz and make it positive
        sf7Data.BzNorm = sf7Data.Bz / max(abs(sf7Data.Bz));
end

fclose(inFile);

end
