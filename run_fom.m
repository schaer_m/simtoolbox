%%%%% FUNCTION run_fom.m %%%%%
% Mattia Schaer
% March 2014
%
% Compute figure-of-merits. The aim of this function is just to separate
% the fom computations of ASTRA and LANL to make the code more readable
%
% CALL fom = run_fom(fomDefinition, [simulationParameters])
%           fomDefinition                       struct
%           simulationParameters                struct
%
%           fom                                 float
%
%%%%%



function fom = run_fom(fomDef, varargin)

global runParam;


% Manage input
switch nargin
    case 1
        actualPars = runParam.list(runParam.active.simInd);
    case 2
        actualPars = varargin{1};
    otherwise
        error('Unknown input parameter sequence');
end

if isstruct(fomDef)
    switch runParam.simType
        case 'astra'
            fom = run_fom_astra(fomDef, actualPars);
        case 'lanl'
            fom = run_fom_lanl(fomDef, actualPars);
        otherwise
            error('Unknown simType');
    end
elseif ischar(fomDef)
    fom = actualPars.(fomDef);
else
    error('Unknown fomDef type');
end

end
