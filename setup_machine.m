%%%%% FUNCTION setup_machine.m %%%%%
% Mattia Schaer
% March 2014
%
% Define paths of the simulation codes depending on the machine we are working on
%
% CALL setup_machine()
%
%%%%%



function setup_machine()

global runParam;


% General settings
switch runParam.machineName
    
    case 'merlin'
        runParam.moveCommand            = 'mv';
        runParam.smtpServer             = 'mail.psi.ch';
    
    case 'localLinux'
        runParam.moveCommand            = 'mv';
    
    case 'laptopLinux'
        runParam.moveCommand            = 'mv';
        
    case 'localWin'
        runParam.moveCommand            = 'move';
        
end

% Simulation type dependent settings
switch runParam.simType
    case 'astra'
        setup_machine_astra();
    case 'lanl'
        setup_machine_lanl();
    otherwise
        error('Unknown simType');
end

end
