# SimToolbox
A toolbox to run parameter scans and optimizations of beam dynamics (with ASTRA) and EM fields (with POISSON SUPERFISH) simulations. Simulations can be sent to the local CPUs as well as to an SGE queue on a cluster (at least on the PSI cluster Merlin).
## Code Organization
* This toolbox represents an interface to easily run and analyse many simulations and optimizations either with ASTRA or with POISSON SUPERFISH, two completely independent codes which however share the same principle:
  - Multiple ASCII input files must be written
  - Multiple executables must be run with the prepared input files
  - Multiple ASCII output files are generated
* A common base code is used for both ASTRA and POISSON SUPERFISH. These are the files found in the [root folder](https://github.com/tiaschaer/SimToolbox). To complete installation after the download of the repo, the correct paths in the [setup_machine_astra.m](https://github.com/tiaschaer/SimToolbox/blob/master/astra/setup_machine_astra.m) and [setup_machine_lanl.m](https://github.com/tiaschaer/SimToolbox/blob/master/lanl/setup_machine_lanl.m) files must be set correctly. The other `setup_` files usually do not need to be modified.
* The parts of the code which are specific for ASTRA are in the [astra folder](https://github.com/tiaschaer/SimToolbox/tree/master/astra) and those specific to POISSON SUPERFISH are in the [lanl folder](https://github.com/tiaschaer/SimToolbox/tree/master/lanl)
* The [NLopt folder](https://github.com/tiaschaer/SimToolbox/tree/master/NLopt) contains the source code of the [NLopt optimization library](http://ab-initio.mit.edu/wiki/index.php/NLopt), which can be installed in order to add more optimization capabilities

Let's take the example of ASTRA simulations (the discussion is completely analogous for POISSON SUPERFISH simulations).
* The code must be started from one of the files in the [geometries folder](https://github.com/tiaschaer/SimToolbox/blob/master/astra/geometries) starting with `run_`, like for example [run_CBandTwGunSwissfelInjector_noMan_120deg_4ps.m](https://github.com/tiaschaer/SimToolbox/blob/master/astra/geometries/run_CBandTwGunSwissfelInjector_noMan_120deg_4ps.m)
* In this file, all of the parameters defining the run, which can represent a single simulation, a (multiple) parameter scan or an optimization, are defined. At the bottom, the [function run_start()](https://github.com/tiaschaer/SimToolbox/blob/master/run_start.m) is called and the routine runs automatically till all simulations/optimizations are executed and analyzed.
* From the [function run_start()](https://github.com/tiaschaer/SimToolbox/blob/master/run_start.m) the code should be more or less self-explaining. It should be quite easy to surf it from there in order to understand its structure.
* The `geometry_` files in the [geometries folder](https://github.com/tiaschaer/SimToolbox/tree/master/astra/geometries) is where the layout of the accelerator is defined, which represents the core function for the generation of the ASTRA input file.
* Depending on the type of simulation/analysis required, a different `procedure_` among those within the [procedures folder](https://github.com/tiaschaer/SimToolbox/tree/master/astra/procedures) is selected in the initial `run_` file.
* Independent codes (not integrated in the automatic procedure) for special analysis of the results are found within the [extras folder](https://github.com/tiaschaer/SimToolbox/tree/master/astra/extras).
