%%%%% FUNCTION check_jobs_merlin.m %%%%%
% Mattia Schaer
% March 2014
%
% Make sure we are not submitting too many jobs to the cluster. Wait before
% going on.
% Also check the recently finished simulations ready for postprocessing
%
% CALL postproInd = check_jobs_merlin()
%           postProInd                      int(N)
%
%%%%%



function postproInd = check_jobs_merlin()

global runParam;


%% Check number of jobs sent to the cluster
exeString = 'qstat';
waitingTime = 0.0;   % [s]
% enter loop
jobsNum = runParam.maxJobsNum;
while jobsNum >= runParam.maxJobsNum
    % count number of submitted jobs
    [~, qstatOutput] = system(exeString);
    jobsNum = numel(strfind(qstatOutput, char(10))) - 2;
    pause(waitingTime);
end


%% Determine finished simulations
if runParam.postprocess.start
    notFinishedInd = find(~runParam.simProgress.finished(1:runParam.active.simInd));
    % store the last simulation which should be restarted
    if ~isempty(notFinishedInd)
        runParam.active.restartInd = notFinishedInd(1);
    end
    searchFileList = cell(1, numel(notFinishedInd));
    for ii = 1:numel(notFinishedInd)
        searchFileList{ii} = runParam.list(notFinishedInd(ii)).finalDistrFilename;
    end
    if runParam.active.simInd < runParam.active.combinationsNo
        newFinishedInd = find_files([runParam.workingPath runParam.runFolderName], searchFileList);
    else
        newFinishedInd = find_files([runParam.workingPath runParam.runFolderName], searchFileList, runParam.maxExecutionTime*runParam.maxJobsNum/10);
    end
    runParam.simProgress.finished(notFinishedInd(newFinishedInd)) = true;
    postproInd = notFinishedInd(newFinishedInd);
else
    postproInd = [];
end


end
