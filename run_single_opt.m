%%%%% FUNCTION run_single_opt.m %%%%%
% Mattia Schaer
% March 2014
%
% Run iteration for the optimizer.
%
% CALL fom = run_single_opt(newPars)
%               newPars                 float(N)
%
%               fom                     float
%
%%%%%



function fom = run_single_opt(newPars)


global runParam;


fprintf('\nIteration %d...\n', runParam.optimize.optInd);

% Extract parameters for this iteration from the list
actualPars = runParam.list(runParam.active.simInd);

% Append simFilebase according to iteration index
actualPars.simFilebase = [actualPars.simFilebase '_opt' sprintf('%d',runParam.optimize.optInd)];

% Use the new values passed by the optimizing function
for ii = 1:runParam.optimize.no
    actualPars.(runParam.optimize.name{ii}) = newPars(ii);
    % output current parameters
    fprintf('%35s = %10f %4s\n', ...
            runParam.optimize.name{ii}, ...
            newPars(ii), ...
            runParam.(runParam.optimize.name{ii}).unit ...
    );
end

%     % transform values from real dimensions to normalized dimensions in [0,1]
%     normRange = [0.0 1.0];
%     for ii = 1:runParam.optimize.no
%         realRange = [runParam.(runParam.optimize.name{ii}).opt(2) runParam.(runParam.optimize.name{ii}).opt(3)];
%         actualPars.(runParam.optimize.name{ii}) = interp1(normRange, realRange, newPars(ii));
%     end


%% Start simulation
eval(['actualPars = procedure_' runParam.procedure '(actualPars);']);


%% Analyze results

switch runParam.machineName
    case 'merlin'
    % In systems where jobs are submitted to a queue (e.g. on thecluster)
    % check for the simulation to be finished
        simulationOK = find_files([runParam.workingPath runParam.runFolderName], ...
                                  {actualPars.finalDistrFilename}, ...
                                  runParam.maxExecutionTime*20 );
    otherwise
    % In systems without queue
    % this point should be reached after the simulation has been completed
        simulationOK = true;
end

% Check if we are not loosing charge
chargeOK = false;
if simulationOK
    chargeOK = check_charge([actualPars.simFilebase '.Cathode.' sprintf('%03d',actualPars.astraId)], ...
                            actualPars.bunchCharge);
end

% Compute fom
if chargeOK && simulationOK
    fom = run_fom(runParam.fomDef{runParam.optimize.fomToOpt}, actualPars);
else
    % Set the highest fom up to now, to "simulate" a bad result
    fom = max(runParam.optimize.fom(runParam.active.simInd, :));
    %fom = NaN
    warning('Assigning maximum fom=%.3E to simulation.', fom);
end

% Save iteration parameters
runParam.optimize.list(runParam.active.simInd, runParam.optimize.optInd) = actualPars;
% fom to optimize is at the first place in the list
runParam.optimize.fom(runParam.active.simInd, runParam.optimize.optInd) = fom;
% current best fom
[runParam.optimize.bestFom(runParam.active.simInd), runParam.optimize.bestInd(runParam.active.simInd)] = ...
    min(runParam.optimize.fom(runParam.active.simInd, :));
% current best pars
for ii = 1:runParam.optimize.no
    runParam.optimize.bestPars(runParam.active.simInd, ii) = ...
        runParam.optimize.list(runParam.active.simInd, runParam.optimize.bestInd(runParam.active.simInd)).(runParam.optimize.name{ii});
end
% output result
fprintf('%35s = %10.3e   (best = %10.3e at iteration %d)\n', ...
        ['Fitness: ' runParam.fomDef{runParam.optimize.fomToOpt}.name], ...
        fom, ...
        runParam.optimize.bestFom(runParam.active.simInd), runParam.optimize.bestInd(runParam.active.simInd) ...
);

% Update iteration output file
write_iterations_outputfile();


%% Postprocess simulation
if simulationOK && runParam.postprocess.start
    % Calculate all figure-of-merits
    for jj = 1:numel(runParam.postprocess.fomIndices)
        if runParam.postprocess.fomIndices(jj) ~= runParam.optimize.fomToOpt
            runParam.fom(jj, runParam.active.simInd, runParam.optimize.optInd) = ...
                run_fom(runParam.fomDef{runParam.postprocess.fomIndices(jj)}, actualPars);
        else
            % that was already computed
            runParam.fom(jj, runParam.active.simInd, runParam.optimize.optInd) = ...
                fom;
        end
        % output result
        [minVal minInd] = min(runParam.fom(jj, runParam.active.simInd, :));
        fprintf('%35s = %10.3e   (best = %10.3e at iteration %d)\n', ...
                runParam.fomDef{runParam.postprocess.fomIndices(jj)}.name, ...
                runParam.fom(jj, runParam.active.simInd, runParam.optimize.optInd), ...
                minVal, minInd ...
        );
    end
end


%% Move files to archive folder
exeString = [runParam.moveCommand ' *' actualPars.simFilebase '.* archive_' num2str(runParam.active.simInd)];
system(exeString);
exeString = [runParam.moveCommand ' *' actualPars.simFilebase '_* archive_' num2str(runParam.active.simInd)];
system(exeString);
exeString = [runParam.moveCommand ' r' num2str(runParam.runNo) '_' num2str(runParam.active.simInd) '.* archive_' num2str(runParam.active.simInd)];
system(exeString);


%% Optimization diagnostic
optimization_diagnostic();


%% Save run parameters to MATLAB file and continue
save(fullfile(runParam.workingPath, runParam.runFolderName, runParam.logFilename), 'runParam');
runParam.optimize.optInd = runParam.optimize.optInd + 1;


end
