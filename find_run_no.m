%%%%% FUNCTION find_run_no.m %%%%%
% Mattia Schaer
% March 2014
%
% Look into the working folder and figure out the last run ID, create a new
% folder for the current run with ID+1 and jump in it
%
% CALL find_run_no()
%
%%%%%



function find_run_no()

global runParam;


% (Create if missing) and jump into working path
if isempty(dir(runParam.workingPath))
    mkdir(runParam.workingPath);
end
cd(runParam.workingPath);
% List elements in the working directory which may be a run folder
wholeList = dir([runParam.folderPrefix '_*']);
dirList = wholeList([wholeList(:).isdir]);
% Find larger run ID
if isempty(dirList)
    runNo = 1;
else
    runIds = zeros(length(dirList),1);
    for ii = 1:length(dirList)
        [~, secondPart] = strtok(dirList(ii).name, '_');
        runIds(ii) = str2double(secondPart(2:end));
    end
    runNo = max(runIds)+1;
end

% Store parameters
runParam.runNo          = runNo;
runParam.runFolderName  = [runParam.folderPrefix '_' num2str(runNo)];
fprintf('Run prefix: %s\n', runParam.folderPrefix);
fprintf('Run number: %d\n', runParam.runNo);
fprintf('Working in folder: %s\n', [runParam.workingPath runParam.runFolderName]);

% Log filename
runParam.logFilename = ['runParameters_' runParam.runFolderName '.mat'];

end
