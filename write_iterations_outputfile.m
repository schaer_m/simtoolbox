%%%%% FUNCTION write_iterations_outputfile.m %%%%%
% Mattia Schaer
% March 2014
%
% Create or update (if already existing) the iterations log file
%
% CALL write_iterations_outputfile()
%
%%%%%



function write_iterations_outputfile()

global runParam;

% Check if file exists (if not it is the first iteration)
wholeList = dir(runParam.iterFile{runParam.active.simInd});
writeHeader = false;
if isempty(wholeList)
    writeHeader = true;
end

% Create or update file
outFile = fopen(runParam.iterFile{runParam.active.simInd}, 'a');

if writeHeader
    % Write header
    fprintf(outFile, '%18s', 'Iteration');
    for ii = 1:runParam.optimize.no
        fprintf(outFile, '%18s', runParam.optimize.name{ii});
    end
    fprintf(outFile, '%18s\n', 'Figure of merit');
end

% Update file with last iteration
fprintf(outFile, '%18d', runParam.optimize.optInd);
for ii = 1:runParam.optimize.no
    actualOptPars(ii) = runParam.optimize.list(runParam.active.simInd, ...
                                               runParam.optimize.optInd).(runParam.optimize.name{ii} ...
                                              ); %#ok<AGROW>
end
fprintf(outFile, '%18f', actualOptPars);
fprintf(outFile, '%18.5e\n', runParam.optimize.fom( ...
                                          runParam.active.simInd, ...
                                          runParam.optimize.optInd ...
                                         ));

fclose(outFile);

end
