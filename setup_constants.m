%%%%% FUNCTION setup_constants.m %%%%%
% Mattia Schaer
% March 2014
%
% Set physical constants as global variables
%
% CALL setup_constants()
%
%%%%%



function setup_constants()

global constVars;


% Speed of light
constVars.c.si      = 2.99792458E8;     % [m/s]

% Electron mass
constVars.mElectron.nat     = 0.510998928E6;            % [eV/c^2]
constVars.mElectron.si      = 9.10938291E-31;           % [kg]
% Electron charge
constVars.qElectron.si      = 1.60217657E-19;           % [C]

% Vacuum permittivity
constVars.epsilon0.si       = 8.854187817620E-12;       % [F/m]

end
