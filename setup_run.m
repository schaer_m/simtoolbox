%%%%% FUNCTION setup_run.m %%%%%
% Mattia Schaer
% March 2014
%
% Find run number
% Determine active parameters (to loop over), combinations number, combinations order
% Determine parameters to optimize
% Ask the user to check the input before starting the computation
%
% CALL startRun = setup_run()
%           startRun                    bool
%
%%%%%



function startRun = setup_run()

global runParam;


% If this function is called, a new run is started, from index 1
runParam.active.restartInd = 1;


%% Find run number
find_run_no();


%% Count active and static parameters
activeNo = 0;
staticNo = 0;
activeLength = [];
% Compute combinations
combinationsNo = 1;
% Count parameters to be optimized
optNo = 0;

% Loop over fields of runParam structure
runParamFields = fieldnames(runParam);
for ii = 1:numel(runParamFields)
    % scan parameters have field .var
    if isfield(runParam.(runParamFields{ii}), 'var')
        %fprintf('runParam.%s has field ''var''!\n', runParamFields{ii});
        if ~isempty(runParam.(runParamFields{ii}).var)
            %fprintf('\t Value: %f\n', runParam.(runParamFields{ii}).var);
            activeNo = activeNo + 1;
            activeLength(activeNo) = numel(runParam.(runParamFields{ii}).var); %#ok<AGROW>
            activeName{activeNo} = runParamFields{ii}; %#ok<AGROW>
            combinationsNo = combinationsNo * activeLength(activeNo);
        else
            staticNo = staticNo + 1;
            staticName{staticNo} = runParamFields{ii}; %#ok<AGROW>
        end
    end
    % optimize parameters have field .opt
    if isfield(runParam.(runParamFields{ii}), 'opt')
        if ~isempty(runParam.(runParamFields{ii}).opt)
            %fprintf('\t Opt: %f\n', runParam.(runParamFields{ii}).opt);
            % check that the order of the initial, lower and upper value is
            % correct
            if runParam.(runParamFields{ii}).opt(2) < runParam.(runParamFields{ii}).opt(3) && ...
               runParam.(runParamFields{ii}).opt(1) > runParam.(runParamFields{ii}).opt(2) && ...
               runParam.(runParamFields{ii}).opt(1) < runParam.(runParamFields{ii}).opt(3) && ...
               runParam.(runParamFields{ii}).opt(4) < runParam.(runParamFields{ii}).opt(3)-runParam.(runParamFields{ii}).opt(2)
                optNo = optNo + 1;
                optName{optNo} = runParamFields{ii}; %#ok<AGROW>
            else
                error(['Wrong ' runParamFields{ii} '.opt order. It must be: [startValue lowerBound upperBound absoluteTolerance]']);
            end
        end
    end
end


%% Assemble list of all parameter combinations
[indList{1:activeNo}] = ind2sub(activeLength, 1:combinationsNo);
for ii = 1:combinationsNo
    for jj = 1:activeNo
        runParam.list(ii).(activeName{jj}) = runParam.(activeName{jj}).nom + runParam.(activeName{jj}).var(indList{jj}(ii));
    end
    for jj = 1:staticNo
        runParam.list(ii).(staticName{jj}) = runParam.(staticName{jj}).nom;
    end
    runParam.list(ii).simFilebase = [runParam.runFolderName '_' num2str(ii)];
end

% List to monitor the simulation progress and allow safe run restarts
runParam.simProgress.started    = false(1, combinationsNo);
runParam.simProgress.finished   = false(1, combinationsNo);

% TODO: Maybe remove this iter files?
% Text files where we can see the progress of the optimizations
if runParam.optimize.start
    for ii = 1:combinationsNo
        runParam.iterFile{ii} = [runParam.list(ii).simFilebase '_iter.txt'];
    end
else
    runParam.iterFile = [runParam.runFolderName '_iter.txt'];
end


%% Output Summary

% Active parameters
runParam.active.combinationsNo  = combinationsNo;
runParam.active.no              = activeNo;
if activeNo > 0
    runParam.active.name        = activeName;
    runParam.active.length      = activeLength;
else
    runParam.active.name        = [];
    runParam.active.length      = [];
end
fprintf('----------\nACTIVE parameters: %d\n----------\n', runParam.active.no);
for ii = 1:runParam.active.no
    fprintf('%15s: from %10f %-5s to %10f %-5s in %3d steps\n', runParam.active.name{ii}, ...
        runParam.(activeName{ii}).nom+min(runParam.(activeName{ii}).var), ...
        runParam.(activeName{ii}).unit, ...
        runParam.(activeName{ii}).nom+max(runParam.(activeName{ii}).var), ...
        runParam.(activeName{ii}).unit, ...
        runParam.active.length(ii) ...
        );
end
fprintf('Total number of combinations: %d\n\n', combinationsNo);

% Parameters to optimize
runParam.optimize.no            = optNo;
if optNo > 0
    runParam.optimize.name      = optName;
end
if runParam.optimize.start
    fprintf('----------\nOPTIMIZATION enabled\n----------\n');
    fprintf('Algorithm: %s\n', runParam.optimize.algorithm);
    fprintf('Figure of merit: %s\n', runParam.fomDef{runParam.optimize.fomToOpt}.name);
    fprintf('Maximum number of optimizing simulations: %d\n', runParam.optimize.MaxFunEval);
    fprintf('Free parameters: %d\n', runParam.optimize.no);
    for ii = 1:runParam.optimize.no
        fprintf('%15s: starting at %10f %-5s and staying in range [%10f %-5s , %10f %-5s] with absolute tolerance %10f %-5s\n', ...
            runParam.optimize.name{ii}, ...
            runParam.(optName{ii}).opt(1), runParam.(optName{ii}).unit, ...
            runParam.(optName{ii}).opt(2), runParam.(optName{ii}).unit, ...
            runParam.(optName{ii}).opt(3), runParam.(optName{ii}).unit, ...
            runParam.(optName{ii}).opt(4), runParam.(optName{ii}).unit ...
            );
    end
end


%% Ask for confirmation before going on (if not disabled)
if runParam.forceParamCheck
    % If phase space is large: pop up a warning
    totalSimNo = combinationsNo;
    if runParam.optimize.start
        totalSimNo = totalSimNo * runParam.optimize.MaxFunEval;
    end
    noOptThreshold  = 100;
    optThreshold    = 1000;
    if (~runParam.optimize.start && totalSimNo>noOptThreshold) || (runParam.optimize.start && totalSimNo>optThreshold)
        warning('The number of simulations that you want to perform is very large: %d\n', totalSimNo);
    end
    answer = input('\nStart run? N/Y [N]: ', 's');
    if ~strcmp(answer,'Y')
        runParam.list = [];
        startRun = false;
        fprintf('\nEXITING\n********************\n\n');
    else
        startRun = true;
    end
else
    startRun = true;
end


end
