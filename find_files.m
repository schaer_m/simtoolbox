%%%%% FUNCTION find_files.m %%%%%
% Mattia Schaer
% March 2014
%
% Look for the presence of files and, if required, wait till all of them
% are present.
%
% CALL foundFiles = find_files(pathWhereToSearch, fileListToSearch, [maxWaitingTime])
%           path                            string
%           fileListToSearch                cell{N} (strings)
%           [maxWaitingTime]                float [s]
%
%           foundFiles                      bool(N)
%
%%%%%



function foundFiles = find_files(path, fileList, varargin)

waitingTime = 0.5;   % [s]
% measure waiting time
startTime = tic();

% Waiting loop
while true
    foundFiles = false(1, numel(fileList));
    for ii = find(~foundFiles)
        foundList = dir(fullfile(path, fileList{ii}));
        if ~isempty(foundList)
            foundFiles(ii) = true;
        end
    end
    
    % Just check once
    if nargin == 2
        break;
        % Continue checking till all files are found or maxWaitingTime has passed
    elseif nargin == 3
        maxWaitingTime = varargin{1};   % [min]
        % not all files were found
        if sum(foundFiles) < numel(foundFiles)
            if toc(startTime) > maxWaitingTime*60
                % stop waiting
                warning('Simulation is taking too much time: skipping it and continuing.\n');
                break;
            else
                % wait some time and check again
                pause(waitingTime);
            end
            % all files were found
        else
            % wait some additional time to be sure that the file has been completely written
            % then exit
            pause(waitingTime);
            break;
        end
    end
end

end