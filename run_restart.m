%%%%% SCRIPT run_restart.m %%%%%
% Mattia Schaer
% March 2014
%
% Restart an interrupted run
%
%%%%%



clearvars -global runParam;

    
% runFilepath = '/gpfs/home/schaer_m/astraRuns/swissfelInjector/normalCell1p0_10/runParameters_normalCell1p0_10.mat';
% restartFromSimInd = 3306;
% run_start(runFilepath, restartFromSimInd);

runFilepath = '/gpfs/home/schaer_m/astraRuns/swissfelInjector/coupler4-eigenmode-largeScan_2/runParameters_coupler4-eigenmode-largeScan_2.mat';
run_start(runFilepath);
